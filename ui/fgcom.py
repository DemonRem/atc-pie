# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'fgcom.ui'
#
# Created: Mon Jul 24 22:55:56 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_fgcomPane(object):
    def setupUi(self, fgcomPane):
        fgcomPane.setObjectName("fgcomPane")
        fgcomPane.resize(256, 234)
        self.verticalLayout = QtWidgets.QVBoxLayout(fgcomPane)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.recordATIS_button = QtWidgets.QToolButton(fgcomPane)
        self.recordATIS_button.setEnabled(True)
        self.recordATIS_button.setObjectName("recordATIS_button")
        self.horizontalLayout_2.addWidget(self.recordATIS_button)
        self.ATISfreq_combo = FrequencyPickCombo(fgcomPane)
        self.ATISfreq_combo.setEditable(True)
        self.ATISfreq_combo.setObjectName("ATISfreq_combo")
        self.horizontalLayout_2.addWidget(self.ATISfreq_combo)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.radios_table = QtWidgets.QTableWidget(fgcomPane)
        self.radios_table.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.radios_table.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.radios_table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.radios_table.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.radios_table.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.radios_table.setColumnCount(1)
        self.radios_table.setObjectName("radios_table")
        self.radios_table.setRowCount(0)
        self.radios_table.horizontalHeader().setVisible(False)
        self.radios_table.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.radios_table)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.addBox_button = QtWidgets.QToolButton(fgcomPane)
        self.addBox_button.setPopupMode(QtWidgets.QToolButton.MenuButtonPopup)
        self.addBox_button.setObjectName("addBox_button")
        self.horizontalLayout.addWidget(self.addBox_button)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.muteAllRadios_tickBox = QtWidgets.QCheckBox(fgcomPane)
        self.muteAllRadios_tickBox.setObjectName("muteAllRadios_tickBox")
        self.horizontalLayout.addWidget(self.muteAllRadios_tickBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.actionCreate_blank_strip = QtWidgets.QAction(fgcomPane)
        self.actionCreate_blank_strip.setObjectName("actionCreate_blank_strip")

        self.retranslateUi(fgcomPane)
        QtCore.QMetaObject.connectSlotsByName(fgcomPane)
        fgcomPane.setTabOrder(self.radios_table, self.ATISfreq_combo)
        fgcomPane.setTabOrder(self.ATISfreq_combo, self.addBox_button)
        fgcomPane.setTabOrder(self.addBox_button, self.muteAllRadios_tickBox)

    def retranslateUi(self, fgcomPane):
        _translate = QtCore.QCoreApplication.translate
        self.recordATIS_button.setText(_translate("fgcomPane", "Record ATIS:"))
        self.addBox_button.setToolTip(_translate("fgcomPane", "Click: add box using internal FGCom process\n"
"Hold: use an externally running FGCom client"))
        self.addBox_button.setText(_translate("fgcomPane", "+ radio box"))
        self.muteAllRadios_tickBox.setText(_translate("fgcomPane", "Mute radios"))
        self.actionCreate_blank_strip.setText(_translate("fgcomPane", "Create blank strip"))

from gui.widgets.basicWidgets import FrequencyPickCombo
