# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'radiobox.ui'
#
# Created: Mon Jul 24 22:55:57 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_radioBox(object):
    def setupUi(self, radioBox):
        radioBox.setObjectName("radioBox")
        radioBox.setEnabled(True)
        radioBox.resize(289, 91)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(radioBox)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.box_frame = QtWidgets.QFrame(radioBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.box_frame.sizePolicy().hasHeightForWidth())
        self.box_frame.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.box_frame.setFont(font)
        self.box_frame.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.box_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.box_frame.setObjectName("box_frame")
        self.gridLayout = QtWidgets.QGridLayout(self.box_frame)
        self.gridLayout.setObjectName("gridLayout")
        self.kbdPTT_checkBox = QtWidgets.QCheckBox(self.box_frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.kbdPTT_checkBox.sizePolicy().hasHeightForWidth())
        self.kbdPTT_checkBox.setSizePolicy(sizePolicy)
        self.kbdPTT_checkBox.setChecked(False)
        self.kbdPTT_checkBox.setObjectName("kbdPTT_checkBox")
        self.gridLayout.addWidget(self.kbdPTT_checkBox, 0, 1, 1, 1)
        self.softVolume_tickBox = QtWidgets.QCheckBox(self.box_frame)
        self.softVolume_tickBox.setObjectName("softVolume_tickBox")
        self.gridLayout.addWidget(self.softVolume_tickBox, 0, 2, 1, 1)
        self.PTT_button = QtWidgets.QToolButton(self.box_frame)
        self.PTT_button.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(38)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.PTT_button.sizePolicy().hasHeightForWidth())
        self.PTT_button.setSizePolicy(sizePolicy)
        self.PTT_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.PTT_button.setCheckable(True)
        self.PTT_button.setObjectName("PTT_button")
        self.gridLayout.addWidget(self.PTT_button, 0, 0, 2, 1)
        self.frequency_combo = FrequencyPickCombo(self.box_frame)
        self.frequency_combo.setEditable(True)
        self.frequency_combo.setObjectName("frequency_combo")
        self.gridLayout.addWidget(self.frequency_combo, 1, 1, 1, 3)
        self.onOff_button = QtWidgets.QToolButton(self.box_frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.onOff_button.sizePolicy().hasHeightForWidth())
        self.onOff_button.setSizePolicy(sizePolicy)
        self.onOff_button.setCheckable(True)
        self.onOff_button.setObjectName("onOff_button")
        self.gridLayout.addWidget(self.onOff_button, 0, 3, 1, 1)
        self.horizontalLayout_4.addWidget(self.box_frame)
        self.removeBox_button = QtWidgets.QToolButton(radioBox)
        self.removeBox_button.setObjectName("removeBox_button")
        self.horizontalLayout_4.addWidget(self.removeBox_button)

        self.retranslateUi(radioBox)
        QtCore.QMetaObject.connectSlotsByName(radioBox)
        radioBox.setTabOrder(self.onOff_button, self.frequency_combo)
        radioBox.setTabOrder(self.frequency_combo, self.kbdPTT_checkBox)
        radioBox.setTabOrder(self.kbdPTT_checkBox, self.softVolume_tickBox)
        radioBox.setTabOrder(self.softVolume_tickBox, self.PTT_button)

    def retranslateUi(self, radioBox):
        _translate = QtCore.QCoreApplication.translate
        self.kbdPTT_checkBox.setToolTip(_translate("radioBox", "Key in radio with left-Ctrl"))
        self.kbdPTT_checkBox.setText(_translate("radioBox", "Kbd PTT"))
        self.softVolume_tickBox.setToolTip(_translate("radioBox", "Reduce volume"))
        self.softVolume_tickBox.setText(_translate("radioBox", "Soft"))
        self.PTT_button.setText(_translate("radioBox", "PTT"))
        self.frequency_combo.setToolTip(_translate("radioBox", "Type+ENTER to tune manually"))
        self.onOff_button.setText(_translate("radioBox", "On/off"))
        self.removeBox_button.setToolTip(_translate("radioBox", "Remove radio box"))
        self.removeBox_button.setText(_translate("radioBox", "X"))

from gui.widgets.basicWidgets import FrequencyPickCombo
