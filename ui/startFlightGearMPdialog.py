# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startFlightGearMPdialog.ui'
#
# Created: Sat Aug  5 10:26:31 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_startFlightGearMPdialog(object):
    def setupUi(self, startFlightGearMPdialog):
        startFlightGearMPdialog.setObjectName("startFlightGearMPdialog")
        startFlightGearMPdialog.resize(234, 175)
        self.verticalLayout = QtWidgets.QVBoxLayout(startFlightGearMPdialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label_4 = QtWidgets.QLabel(startFlightGearMPdialog)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.callsign_edit = QtWidgets.QLineEdit(startFlightGearMPdialog)
        self.callsign_edit.setMaxLength(7)
        self.callsign_edit.setObjectName("callsign_edit")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.callsign_edit)
        self.label_2 = QtWidgets.QLabel(startFlightGearMPdialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.nickname_edit = QtWidgets.QLineEdit(startFlightGearMPdialog)
        self.nickname_edit.setObjectName("nickname_edit")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.nickname_edit)
        self.label_3 = QtWidgets.QLabel(startFlightGearMPdialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.clientPort_edit = QtWidgets.QSpinBox(startFlightGearMPdialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.clientPort_edit.sizePolicy().hasHeightForWidth())
        self.clientPort_edit.setSizePolicy(sizePolicy)
        self.clientPort_edit.setMaximum(99999)
        self.clientPort_edit.setObjectName("clientPort_edit")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.clientPort_edit)
        self.label = QtWidgets.QLabel(startFlightGearMPdialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.FGMSserver_info = QtWidgets.QLabel(startFlightGearMPdialog)
        self.FGMSserver_info.setObjectName("FGMSserver_info")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.FGMSserver_info)
        self.line = QtWidgets.QFrame(startFlightGearMPdialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.SpanningRole, self.line)
        self.verticalLayout.addLayout(self.formLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cancel_button = QtWidgets.QPushButton(startFlightGearMPdialog)
        self.cancel_button.setObjectName("cancel_button")
        self.horizontalLayout.addWidget(self.cancel_button)
        self.OK_button = QtWidgets.QPushButton(startFlightGearMPdialog)
        self.OK_button.setDefault(True)
        self.OK_button.setObjectName("OK_button")
        self.horizontalLayout.addWidget(self.OK_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label_4.setBuddy(self.callsign_edit)
        self.label_2.setBuddy(self.nickname_edit)
        self.label_3.setBuddy(self.clientPort_edit)

        self.retranslateUi(startFlightGearMPdialog)
        self.cancel_button.clicked.connect(startFlightGearMPdialog.reject)
        self.OK_button.clicked.connect(startFlightGearMPdialog.accept)
        QtCore.QMetaObject.connectSlotsByName(startFlightGearMPdialog)
        startFlightGearMPdialog.setTabOrder(self.callsign_edit, self.nickname_edit)
        startFlightGearMPdialog.setTabOrder(self.nickname_edit, self.clientPort_edit)
        startFlightGearMPdialog.setTabOrder(self.clientPort_edit, self.OK_button)
        startFlightGearMPdialog.setTabOrder(self.OK_button, self.cancel_button)

    def retranslateUi(self, startFlightGearMPdialog):
        _translate = QtCore.QCoreApplication.translate
        startFlightGearMPdialog.setWindowTitle(_translate("startFlightGearMPdialog", "Connect to FlightGear MP server"))
        self.label_4.setText(_translate("startFlightGearMPdialog", "Callsign:"))
        self.callsign_edit.setToolTip(_translate("startFlightGearMPdialog", "Typically starts with 4-letter ICAO code"))
        self.label_2.setText(_translate("startFlightGearMPdialog", "Nickname:"))
        self.nickname_edit.setToolTip(_translate("startFlightGearMPdialog", "Choose one to be recognised by fellow ATCs nearby"))
        self.label_3.setText(_translate("startFlightGearMPdialog", "Client port:"))
        self.label.setText(_translate("startFlightGearMPdialog", "Connecting to:"))
        self.FGMSserver_info.setText(_translate("startFlightGearMPdialog", "###"))
        self.cancel_button.setText(_translate("startFlightGearMPdialog", "Cancel"))
        self.OK_button.setText(_translate("startFlightGearMPdialog", "OK"))

