# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'textChat.ui'
#
# Created: Mon Jul 24 22:55:55 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_textChatFrame(object):
    def setupUi(self, textChatFrame):
        textChatFrame.setObjectName("textChatFrame")
        textChatFrame.resize(608, 157)
        self.verticalLayout = QtWidgets.QVBoxLayout(textChatFrame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.chatHistory_view = QtWidgets.QTableView(textChatFrame)
        self.chatHistory_view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.chatHistory_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.chatHistory_view.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.chatHistory_view.setShowGrid(False)
        self.chatHistory_view.setObjectName("chatHistory_view")
        self.chatHistory_view.horizontalHeader().setVisible(False)
        self.chatHistory_view.horizontalHeader().setStretchLastSection(True)
        self.chatHistory_view.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.chatHistory_view)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.resetDest_button = QtWidgets.QToolButton(textChatFrame)
        self.resetDest_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.resetDest_button.setObjectName("resetDest_button")
        self.horizontalLayout.addWidget(self.resetDest_button)
        self.dest_combo = QtWidgets.QComboBox(textChatFrame)
        self.dest_combo.setEditable(True)
        self.dest_combo.setInsertPolicy(QtWidgets.QComboBox.NoInsert)
        self.dest_combo.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)
        self.dest_combo.setMinimumContentsLength(10)
        self.dest_combo.setObjectName("dest_combo")
        self.horizontalLayout.addWidget(self.dest_combo)
        self.resetMessage_button = QtWidgets.QToolButton(textChatFrame)
        self.resetMessage_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.resetMessage_button.setObjectName("resetMessage_button")
        self.horizontalLayout.addWidget(self.resetMessage_button)
        self.chatLine_input = QtWidgets.QComboBox(textChatFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.chatLine_input.sizePolicy().hasHeightForWidth())
        self.chatLine_input.setSizePolicy(sizePolicy)
        self.chatLine_input.setEditable(True)
        self.chatLine_input.setInsertPolicy(QtWidgets.QComboBox.NoInsert)
        self.chatLine_input.setObjectName("chatLine_input")
        self.horizontalLayout.addWidget(self.chatLine_input)
        self.send_button = QtWidgets.QToolButton(textChatFrame)
        self.send_button.setEnabled(True)
        self.send_button.setObjectName("send_button")
        self.horizontalLayout.addWidget(self.send_button)
        self.menu_button = QtWidgets.QToolButton(textChatFrame)
        self.menu_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.menu_button.setObjectName("menu_button")
        self.horizontalLayout.addWidget(self.menu_button)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(textChatFrame)
        QtCore.QMetaObject.connectSlotsByName(textChatFrame)
        textChatFrame.setTabOrder(self.chatHistory_view, self.resetDest_button)
        textChatFrame.setTabOrder(self.resetDest_button, self.dest_combo)
        textChatFrame.setTabOrder(self.dest_combo, self.resetMessage_button)
        textChatFrame.setTabOrder(self.resetMessage_button, self.chatLine_input)
        textChatFrame.setTabOrder(self.chatLine_input, self.send_button)

    def retranslateUi(self, textChatFrame):
        _translate = QtCore.QCoreApplication.translate
        self.resetDest_button.setToolTip(_translate("textChatFrame", "Clear and focus recipient field"))
        self.resetDest_button.setText(_translate("textChatFrame", "To"))
        self.resetMessage_button.setToolTip(_translate("textChatFrame", "Clear and focus message line"))
        self.resetMessage_button.setText(_translate("textChatFrame", "Msg"))
        self.send_button.setText(_translate("textChatFrame", "Send"))
        self.menu_button.setText(_translate("textChatFrame", "Opts"))

