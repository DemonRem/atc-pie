# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'routeEditWidget.ui'
#
# Created: Thu May  5 08:13:10 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_routeEditWidget(object):
    def setupUi(self, routeEditWidget):
        routeEditWidget.setObjectName("routeEditWidget")
        routeEditWidget.resize(321, 148)
        self.horizontalLayout = QtWidgets.QHBoxLayout(routeEditWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.text_edit = QtWidgets.QPlainTextEdit(routeEditWidget)
        self.text_edit.setTabChangesFocus(True)
        self.text_edit.setObjectName("text_edit")
        self.horizontalLayout.addWidget(self.text_edit)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.view_button = QtWidgets.QToolButton(routeEditWidget)
        self.view_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.view_button.setObjectName("view_button")
        self.verticalLayout.addWidget(self.view_button)
        self.suggestions_button = QtWidgets.QToolButton(routeEditWidget)
        self.suggestions_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.suggestions_button.setObjectName("suggestions_button")
        self.verticalLayout.addWidget(self.suggestions_button)
        self.saveAsPreset_button = QtWidgets.QToolButton(routeEditWidget)
        self.saveAsPreset_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.saveAsPreset_button.setObjectName("saveAsPreset_button")
        self.verticalLayout.addWidget(self.saveAsPreset_button)
        self.horizontalLayout.addLayout(self.verticalLayout)

        self.retranslateUi(routeEditWidget)
        QtCore.QMetaObject.connectSlotsByName(routeEditWidget)
        routeEditWidget.setTabOrder(self.text_edit, self.suggestions_button)
        routeEditWidget.setTabOrder(self.suggestions_button, self.saveAsPreset_button)

    def retranslateUi(self, routeEditWidget):
        _translate = QtCore.QCoreApplication.translate
        self.view_button.setToolTip(_translate("routeEditWidget", "Route details"))
        self.view_button.setText(_translate("routeEditWidget", "V"))
        self.suggestions_button.setToolTip(_translate("routeEditWidget", "Route presets"))
        self.suggestions_button.setText(_translate("routeEditWidget", "R"))
        self.saveAsPreset_button.setToolTip(_translate("routeEditWidget", "Save current entry as route preset"))
        self.saveAsPreset_button.setText(_translate("routeEditWidget", "S"))

