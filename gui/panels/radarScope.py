
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from math import log, exp

from PyQt5.QtWidgets import QWidget, QGraphicsView, QMenu, QAction, QActionGroup, QMessageBox, QInputDialog
from PyQt5.QtCore import pyqtSignal
from ui.radarScope import Ui_radarScopeFrame

from game.config import settings
from game.env import env

from data.nav import Navpoint, NavpointError
from data.strip import parsed_route_detail
from data.instruction import Instruction

from ext.xplane import get_airport_data

from gui.misc import signals, selection
from gui.graphics.miscGraphics import BgPixmapItem
from gui.graphics.radarScene import Layer, RadarScene
from gui.dialog.miscDialogs import RouteSpecsLostDialog
from gui.dialog.bgImg import PositionBgImgDialog


# ---------- Constants ----------

max_zoom_factor = 1000
max_zoom_range = .1 # NM
auto_bgimg_spec_file_prefix = 'auto-'

# -------------------------------



class ScopeFrame(QWidget, Ui_radarScopeFrame):
	def __init__(self, parent=None, secondary=False):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.mouse_info.clear()
		self.scene = RadarScene(self)
		self.scopeView.setScene(self.scene)
		self.courseLineFlyTime_edit.setValue(self.scene.speedMarkCount())
		self.last_airfield_clicked = None
		self.LDG_disp_actions = {}
		# BG IMAGES menu
		self.rebuildImgToggleMenu()
		# Nav menu
		nav_menu = QMenu()
		self._addMenuLayerToggle(nav_menu, 'Airfields', False, Layer.NAV_AIRFIELDS)
		self._addMenuLayerToggle(nav_menu, 'Navaids', True, Layer.NAV_AIDS)
		self._addMenuLayerToggle(nav_menu, 'Fixes', False, Layer.NAV_FIXES)
		self._addMenuLayerToggle(nav_menu, 'RNAV points', False, Layer.RNAV_POINTS)
		self.nav_menuButton.setMenu(nav_menu)
		# AD menu
		AD_menu = QMenu()
		self._addMenuToggleAction(AD_menu, 'Ground routes', False, self.scene.showGroundNetworks)
		self._addMenuToggleAction(AD_menu, 'Taxiway names', False, self.scene.showTaxiwayNames)
		self._addMenuToggleAction(AD_menu, 'Highlight taxiways under mouse', True, self.scene.highlightEdgesOnMouseover)
		self._addMenuToggleAction(AD_menu, 'RWY names always visible', False, self.scene.setRunwayNamesAlwaysVisible)
		AD_menu.addSeparator()
		self._addMenuLayerToggle(AD_menu, 'Lines on tarmac', False, Layer.AIRPORT_LINES)
		self._addMenuLayerToggle(AD_menu, 'Parking positions', True, Layer.PARKING_POSITIONS)
		self._addMenuLayerToggle(AD_menu, 'Other objects', True, Layer.AIRPORT_OBJECTS)
		AD_menu.addSeparator()
		drawAirport_action = QAction('Draw additional airport...', self)
		drawAirport_action.triggered.connect(self.drawAdditionalAirport)
		resetAirports_action = QAction('Reset drawn airports', self)
		resetAirports_action.triggered.connect(self.scene.resetAirportItems)
		AD_menu.addAction(drawAirport_action)
		AD_menu.addAction(resetAirports_action)
		self.AD_menuButton.setMenu(AD_menu)
		# LDG menu
		if env.airport_data == None:
			self.LDG_menuButton.setEnabled(False)
		else:
			LDG_menu = QMenu()
			self.syncLDG_action = self._addMenuToggleAction(LDG_menu, 'Follow arrival runway selection', True, self.setSyncLDG)
			for rwy in env.airport_data.allRunways(sortByName=True): # all menu options set to False below; normally OK at start
				action = self._addMenuToggleAction(LDG_menu, 'RWY %s' % rwy.name, False, lambda b, r=rwy.name: self.scene.showLandingHelper(r, b))
				action.triggered.connect(lambda b: self.syncLDG_action.setChecked(False)) # cancel sync when one is set manually (triggered)
				self.LDG_disp_actions[rwy.name] = action
			LDG_menu.addSeparator()
			self._addMenuToggleAction(LDG_menu, 'Slope altitudes', True, self.scene.showSlopeAltitudes)
			self._addMenuToggleAction(LDG_menu, 'Interception cones', False, self.scene.showInterceptionCones)
			self.LDG_menuButton.setMenu(LDG_menu)
		# ACFT menu
		ACFT_menu = QMenu()
		self._addMenuToggleAction(ACFT_menu, 'Filter out unlinked GND modes', False, lambda b: self.scene.showGndModes(not b))
		ACFT_menu.addSeparator()
		self._addMenuToggleAction(ACFT_menu, 'Selected ACFT course/assignments', True, self.scene.showSelectionAssignments)
		self._addMenuToggleAction(ACFT_menu, 'All courses/vectors', False, self.scene.showVectors)
		self._addMenuToggleAction(ACFT_menu, 'All routes', False, self.scene.showRoutes)
		ACFT_menu.addSeparator()
		self._addMenuToggleAction(ACFT_menu, 'Info boxes', True, self.scene.showInfoBoxes)
		self._addMenuToggleAction(ACFT_menu, 'Separation rings', False, self.scene.showSeparationRings)
		self.ACFT_menuButton.setMenu(ACFT_menu)
		# OPTIONS menu
		options_menu = QMenu()
		self.autoCentre_action = QAction('Centre on indications', self)
		self.autoCentre_action.setCheckable(True)
		self.autoCentre_action.setChecked(not secondary)
		options_menu.addAction(self.autoCentre_action)
		self._addMenuToggleAction(options_menu, 'Show custom labels', True, self.scene.layers[Layer.CUSTOM_LABELS].setVisible)
		options_menu.addSeparator()
		measuringShowsCoords_action = QAction('Measuring shows coordinates', self)
		measuringShowsCoords_action.setCheckable(True)
		measuringShowsCoords_action.toggled.connect(self.scene.setMouseInfoShowsCoords)
		measuringShowsCoords_action.setChecked(True)
		measuringShowsElev_action = QAction('Measuring shows elevation', self)
		measuringShowsElev_action.setCheckable(True)
		if env.elevation_map == None:
			measuringShowsElev_action.setEnabled(False)
		action_group = QActionGroup(self)
		action_group.addAction(measuringShowsCoords_action)
		action_group.addAction(measuringShowsElev_action)
		options_menu.addActions(action_group.actions())
		options_menu.addSeparator()
		save_points_and_labels_action = QAction('Save points and labels', self)
		save_points_and_labels_action.triggered.connect(self.savePinnedPointsAndLabels)
		options_menu.addAction(save_points_and_labels_action)
		self.options_menuButton.setMenu(options_menu)
		# Other actions and signals
		self.courseLineFlyTime_edit.valueChanged.connect(self.scene.setSpeedMarkCount)
		self.scene.mouseInfo.connect(self.mouse_info.setText)
		self.scene.addRemoveRouteNavpoint.connect(self.addRemoveRouteNavpointToSelection)
		self.scene.imagesRedrawn.connect(self.rebuildImgToggleMenu)
		signals.selectionChanged.connect(self.mouse_info.clear)
		signals.runwayUseChanged.connect(self.updateLdgMenuAndDisplay)
		signals.navpointClick.connect(self.setLastAirfieldClicked)
		signals.indicatePoint.connect(self.indicatePoint)
		signals.mainWindowClosing.connect(self.close)
		self.zoomLevel_slider.valueChanged.connect(self.changeZoomLevel)
		self.scopeView.zoom_signal.connect(self.zoom)
		# Finish
		self.sync_LDG_display = True
		self.updateLdgMenuAndDisplay()
		self.scopeView.moveToShow(env.radarPos())
		self.f_scale = lambda x: max_zoom_factor / exp(x * log(settings.map_range / max_zoom_range)) # x in [0, 1]
		self.changeZoomLevel(self.zoomLevel_slider.value())
	
	def _addMenuLayerToggle(self, menu, text, init_state, layer):
		action = self._addMenuToggleAction(menu, text, init_state, self.scene.layers[layer].setVisible)
		return action
	
	def _addMenuToggleAction(self, menu, text, init_state, toggle_function):
		action = QAction(text, self)
		action.setCheckable(True)
		menu.addAction(action)
		action.setChecked(init_state)
		toggle_function(init_state)
		action.toggled.connect(toggle_function)
		return action
	
	def rebuildImgToggleMenu(self):
		img_list = self.scene.layerItems(Layer.BG_IMAGES)
		img_menu = QMenu()
		for img_item in img_list:
			self._addMenuToggleAction(img_menu, img_item.title, False, img_item.setVisible)
		self.bgImg_menuButton.setMenu(img_menu)
		self.bgImg_menuButton.setEnabled(img_list != [])
	
	def setLastAirfieldClicked(self, navpoint):
		if navpoint.type == Navpoint.AD:
			self.last_airfield_clicked = navpoint.code
	
	def setSyncLDG(self, toggle):
		self.sync_LDG_display = toggle
		if toggle:
			self.updateLdgMenuAndDisplay()
	
	def updateLdgMenuAndDisplay(self):
		if self.sync_LDG_display and env.airport_data != None:
			for rwy, action in self.LDG_disp_actions.items():
				action.setChecked(env.airport_data.runway(rwy).use_for_arrivals)
	
	def drawAdditionalAirport(self):
		sep = '  '
		lst = sorted('%s%s%s' % (ad, sep, ad.long_name) for ad in env.navpoints.findAll(types=[Navpoint.AD]))
		if self.last_airfield_clicked == None:
			index = 0
		else:
			index = next(i for i, item in enumerate(lst) if lst[i].startswith(self.last_airfield_clicked))
		ad_item, ok = QInputDialog.getItem(self, 'Draw additional airport', \
			'NOTE: This may freeze the program for a few seconds.', lst, current=index)
		if ok:
			ad_code = ad_item.split(sep, maxsplit=1)[0].upper()
			try:
				ignore = env.navpoints.findUnique(ad_code, types=[Navpoint.AD])
				ad_data = get_airport_data(ad_code)
			except NavpointError:
				QMessageBox.critical(self, 'Airport code error', 'No such airport code in map range: %s' % ad_code)
			else:
				self.scene.drawAirportData(ad_data)
				signals.indicatePoint.emit(ad_data.navpoint.coordinates)
	
	def savePinnedPointsAndLabels(self):
		settings.saved_pinned_navpoints = ['%s~%s' % (p, p.coordinates.toString()) for p in self.scene.pinnedNavpoints()]
		settings.saved_pinned_parking_positions = self.scene.pinnedParkingPositions()
		settings.saved_custom_labels = [(lbl, pos.toString()) for lbl, pos in self.scene.customLabels()]
		QMessageBox.information(self, 'Points saved', 'Current pinned points and custom labels saved as defaults.')
	
	def changeZoomLevel(self, percent_level):
		'''
		percent_level is a value in [0, 100]
		'''
		self.scopeView.setScaleFactor(self.f_scale(1 - percent_level / 100))
	
	def zoom(self, zoom_in):
		step = self.zoomLevel_slider.pageStep()
		if not zoom_in: # zooming OUT
			step = -step
		self.zoomLevel_slider.setValue(self.zoomLevel_slider.value() + step)
	
	def indicatePoint(self, coords):
		if env.pointOnMap(coords):
			if self.autoCentre_action.isChecked():
				self.scopeView.moveToShow(coords)
			self.scene.point_indicator.indicate(coords)
		else:
			signals.statusBarMsg.emit('Point is off map!')
	
	def positionVisibleBgImages(self):
		imglst_all = self.scene.layerItems(Layer.BG_IMAGES)
		imglst_moving = [item for item in imglst_all if item.isVisible() and isinstance(item, BgPixmapItem)]
		PositionBgImgDialog(imglst_moving, self).exec()
		file_name = settings.outputFileName(auto_bgimg_spec_file_prefix + settings.location_code, ext='lst', sessionID=False)
		with open(file_name, 'w') as f:
			for item in imglst_all:
				f.write(item.specLine() + '\n')
			for src, img, scale, title in settings.loose_strip_bay_backgrounds:
				f.write('%s\tLOOSE %g\t%s\n' % (src, scale, title))
		if imglst_moving != []:
			QMessageBox.information(self, 'Image positioning', 'Your changes have so far only affected the radar screen '
				'in the main window. If you like it the way it is, make sure you modify your .lst file and reload your images.\n\n'
				'File %s was also generated with your new corner coordinates for you to copy.' % file_name)
	
	def addRemoveRouteNavpointToSelection(self, navpoint):
		strip = selection.strip
		if strip == None:
			QMessageBox.critical(self, 'Routing error', 'No strip in current selection.')
			return
		route = strip.lookup(parsed_route_detail)
		if route == None:
			QMessageBox.critical(self, 'Routing error', 'No parsed route on strip! Fill in at least departure and arrival airports.')
		else:
			dialog = None
			if navpoint in route: # Remove navpoint from route
				lost_before, lost_after = strip.removeRouteWaypoint(navpoint)
				if settings.open_lost_leg_specs_dialog and (lost_before != [] or lost_after != []):
					dialog = RouteSpecsLostDialog(self, 'Waypoint %s removed' % navpoint, \
							'%s [%s] %s' % (' '.join(lost_before), navpoint, ' '.join(lost_after)))
			else: # Add navpoint to route
				lost_specs = strip.insertRouteWaypoint(navpoint)
				if settings.open_lost_leg_specs_dialog and lost_specs != []:
					dialog = RouteSpecsLostDialog(self, 'Waypoint %s inserted' % navpoint, ' '.join(lost_specs))
			if dialog != None:
				dialog.exec()
				if dialog.mustOpenStripDetails():
					signals.stripEditRequest.emit(strip)
			signals.stripInfoChanged.emit()
			if settings.instruct_route_change_immediately:
				settings.game_manager.instructAircraft(Instruction(Instruction.FOLLOW_ROUTE, arg=route.dup()))

	def closeEvent(self, event):
		self.scene.disconnectAllSignals()
		signals.selectionChanged.disconnect(self.mouse_info.clear)
		signals.runwayUseChanged.disconnect(self.updateLdgMenuAndDisplay)
		signals.navpointClick.disconnect(self.setLastAirfieldClicked)
		signals.indicatePoint.disconnect(self.indicatePoint)
		signals.mainWindowClosing.disconnect(self.close)
		event.accept() # deletes (not just hides) extra scope windows open because WA_DeleteOnClose attribute is set
		QWidget.closeEvent(self, event)

