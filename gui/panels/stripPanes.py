
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QWidget, QMenu, QAction, QActionGroup, QGraphicsView, QMessageBox
from PyQt5.QtGui import QIcon
from ui.stripPane import Ui_stripPane
from ui.looseStripBay import Ui_looseStripPane

from data.util import some
from game.config import settings
from game.env import env
from models.gameStrips import default_rack_name
from data.strip import rack_detail, recycled_detail, runway_box_detail

from gui.misc import signals, selection, IconFile
from gui.stripRackActions import new_strip_dialog, new_rack_dialog, discard_strip, pull_XPDR_details, pull_FPL_details
from gui.dialog.miscDialogs import yesNo_question
from gui.graphics.flightStrips import LooseStripBayScene


# ---------- Constants ----------

# -------------------------------


class StripRacksPane(QWidget, Ui_stripPane):
	view_types = DIVIDE, SCROLL, TABBED = range(3) # NB: order matters for menu action descriptions, and first is default
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.createBlankStrip_button.setIcon(QIcon(IconFile.action_newStrip))
		self.createNewRack_button.setIcon(QIcon(IconFile.action_newRack))
		self.deleteStrip_button.setIcon(QIcon(IconFile.button_bin))
		self.rwyBoxes_view.setVisible(env.airport_data != None)
		
		# PULL menu
		self.pullXpdrDetails_action = QAction('XPDR details', self)
		self.pullFplDetails_action = QAction('FPL details', self)
		pull_menu = QMenu()
		pull_menu.addAction(self.pullXpdrDetails_action)
		pull_menu.addAction(self.pullFplDetails_action)
		self.pullXpdrDetails_action.triggered.connect(lambda: pull_XPDR_details(self))
		self.pullFplDetails_action.triggered.connect(lambda: pull_FPL_details(self))
		self.pull_menuButton.setMenu(pull_menu)
		
		# VIEW menu
		self.showRwyBoxes_action = QAction('Show RWY boxes', self)
		self.showRwyBoxes_action.setCheckable(True)
		self.showRwyBoxes_action.toggled.connect(self.updateRwyBoxesVisibility)
		self.showRwyBoxes_action.setChecked(env.airport_data != None)
		view_action_group = QActionGroup(self)
		for view_type, txt in zip(StripRacksPane.view_types, ['Divided width', 'Horizontal scroll', 'Tabbed racks']):
			view_action = QAction(txt, self)
			view_action.setCheckable(True)
			view_action.triggered.connect(lambda ignore_bool, t=view_type: self.selectViewType(t))
			view_action_group.addAction(view_action)
			if view_type == 0: # i.e. == StripRacksPane.view_types[0]
				view_action.setChecked(True)
		saveRackConfig_action = QAction('Save rack configuration', self)
		saveRackConfig_action.triggered.connect(self.saveCurrentRackOrder)
		view_menu = QMenu()
		if env.airport_data != None:
			view_menu.addAction(self.showRwyBoxes_action)
			view_menu.addSeparator()
		view_menu.addActions(view_action_group.actions())
		view_menu.addSeparator()
		view_menu.addAction(saveRackConfig_action)
		self.view_menuButton.setMenu(view_menu)
		self.selectViewType(StripRacksPane.view_types[0])
		
		# Other buttons and signals
		self.updateButtons()
		self.createBlankStrip_button.clicked.connect(lambda: new_strip_dialog(self, default_rack_name))
		self.deleteStrip_button.clicked.connect(lambda: discard_strip(self, selection.strip, False))
		self.shelf_button.stripDropped.connect(lambda strip: discard_strip(self, strip, True))
		self.shelf_button.clicked.connect(signals.openShelfRequest.emit)
		self.createNewRack_button.clicked.connect(lambda: new_rack_dialog(self))
		signals.selectionChanged.connect(self.updateButtons)
		signals.selectionChanged.connect(self.updateRwyBoxesVisibility)
		signals.selectionChanged.connect(self.rwyBoxes_view.updateSelection)
		signals.selectionChanged.connect(self.stripRacks_view.updateSelection)
		signals.runwayUseChanged.connect(self.updateRwyBoxesVisibility)
	
	def updateButtons(self):
		self.pull_menuButton.setEnabled(selection.strip != None)
		if self.pull_menuButton.isEnabled():
			self.pullXpdrDetails_action.setEnabled(selection.strip.linkedAircraft() != None)
			self.pullFplDetails_action.setEnabled(selection.strip.linkedFPL() != None)
		self.deleteStrip_button.setEnabled(selection.strip != None \
				and (selection.strip.lookup(rack_detail) != None or selection.strip.lookup(runway_box_detail) != None))
	
	def updateRwyBoxesVisibility(self):
		show = env.strips.count(lambda s: s.lookup(runway_box_detail) != None) != 0 # in any case if a strip is already boxed
		if env.airport_data != None:
			show |= self.showRwyBoxes_action.isChecked() and any(rwy.inUse() for rwy in env.airport_data.allRunways())
		self.rwyBoxes_view.setVisible(show)
		self.stripRacks_tabs.showRunwayBoxTab(show)
	
	def selectViewType(self, view_type):
		if view_type == StripRacksPane.TABBED:
			self.stacked_view_widget.setCurrentWidget(self.tabView_page)
		else:
			self.stacked_view_widget.setCurrentWidget(self.tableView_page)
			self.stripRacks_view.setDivideHorizWidth(view_type == StripRacksPane.DIVIDE)
	
	def saveCurrentRackOrder(self):
		if self.stacked_view_widget.currentWidget() is self.tableView_page:
			hh = self.stripRacks_view.horizontalHeader()
			settings.saved_strip_racks = [env.strips.rackName(hh.logicalIndex(vis)) for vis in range(hh.count())]
		else:
			settings.saved_strip_racks = [w.rackFilter() for i, w in self.stripRacks_tabs.rackTabs()]
		settings.saved_rwy_tab_pos = some(self.stripRacks_tabs.rwyBoxTabIndex(), 0)
		QMessageBox.information(self, 'Racks saved', 'Rack names and order saved successfully.')





#############################

##     LOOSE STRIP BAY     ##

#############################


class LooseStripPane(QWidget, Ui_looseStripPane):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.force_on_close = False
		self.createStrip_button.setIcon(QIcon(IconFile.action_newStrip))
		self.deleteStrip_button.setIcon(QIcon(IconFile.button_bin))
		self.stripView.setDragMode(QGraphicsView.ScrollHandDrag)
		self.scene = LooseStripBayScene(self)
		self.stripView.setScene(self.scene)
		self.clearBg_action = QAction('No background', self)
		self.clearBg_action.setCheckable(True)
		self.clearBg_action.triggered.connect(self.scene.clearBgImg)
		self.rebuildBgMenu()
		self.updateButtons()
		self.compactStrips_tickBox.toggled.connect(self.scene.setCompactStrips)
		self.createStrip_button.clicked.connect(self.createStrip)
		self.deleteStrip_button.clicked.connect(self.deleteStrip)
		self.shelf_button.stripDropped.connect(lambda strip: discard_strip(self, strip, True))
		self.shelf_button.clicked.connect(signals.openShelfRequest.emit)
		signals.backgroundImagesReloaded.connect(self.rebuildBgMenu)
		signals.selectionChanged.connect(self.updateButtons)
		signals.gameStopped.connect(self.scene.deleteAllStripItems)
		signals.mainWindowClosing.connect(self.forceClose)
	
	def rebuildBgMenu(self):
		self.scene.clearBgImg() # clears background
		bg_action_group = QActionGroup(self)
		bg_action_group.addAction(self.clearBg_action)
		for file_spec, pixmap, scale, title in settings.loose_strip_bay_backgrounds:
			action = QAction(title, self)
			action.setCheckable(True)
			action.triggered.connect(lambda b, px=pixmap, sc=scale: self.scene.setBgImg(px, sc))
			bg_action_group.addAction(action)
		bg_menu = QMenu()
		bg_menu.addAction(self.clearBg_action)
		bg_menu.addSeparator()
		bg_menu.addActions(bg_action_group.actions()[1:]) # index 0 is self.clearBg_action
		self.background_menuButton.setMenu(bg_menu)
		self.clearBg_action.setChecked(True)
		self.background_menuButton.setEnabled(settings.loose_strip_bay_backgrounds != [])
	
	def updateButtons(self):
		strip = selection.strip
		self.deleteStrip_button.setEnabled(strip != None and any(s is strip for s in self.scene.stripsInBay()))
	
	def createStrip(self):
		strip = new_strip_dialog(self, None)
		if strip != None:
			self.scene.placeNewStrip(strip)
		self.updateButtons()
	
	def deleteStrip(self):
		strip = discard_strip(self, selection.strip, False)
		if strip != None:
			self.scene.deleteStripItem(strip)
	
	def forceClose(self):
		self.force_on_close = True
		self.close()
	
	def closeEvent(self, event):
		if not self.force_on_close: # Warn if strip bay is not empty
			strips = self.scene.stripsInBay()
			if len(strips) > 0:
				if yesNo_question(self, 'Strip bay not empty', \
						'This strip bay is currently holding %d strip(s).' % len(strips), 'Rack strips and close?'):
					for strip in strips:
						strip.writeDetail(recycled_detail, True)
						env.strips.repositionStrip(strip, default_rack_name)
				else:
					event.ignore()
					return
		self.scene.disconnectAllSignals()
		signals.backgroundImagesReloaded.disconnect(self.rebuildBgMenu)
		signals.selectionChanged.disconnect(self.updateButtons)
		signals.gameStopped.disconnect(self.scene.deleteAllStripItems)
		signals.mainWindowClosing.disconnect(self.forceClose)
		event.accept() # deletes (not just hides) the window because WA_DeleteOnClose attribute is set
		QWidget.closeEvent(self, event)




