
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re

from PyQt5.QtCore import Qt, QAbstractTableModel, QSortFilterProxyModel, QModelIndex, pyqtSignal
from PyQt5.QtWidgets import QWidget
from ui.navigator import Ui_navigator
from data.nav import Navpoint
from data.coords import dist_str
from game.env import env
from gui.misc import signals



# ---------- Constants ----------


# -------------------------------






# =============================================== #

#                     MODELS                      #

# =============================================== #







class NavpointModel(QAbstractTableModel):

	column_headers = ['Type', 'Code/ID', 'Name/info']

	def __init__(self, navdb, parent):
		QAbstractTableModel.__init__(self, parent)
		self.navpoints = navdb.findAll()
		self.pk_pos = [] if env.airport_data == None else env.airport_data.ground_net.parkingPositions()

	def rowCount(self, parent=QModelIndex()):
		return 0 if parent.isValid() else len(self.navpoints) + len(self.pk_pos)

	def columnCount(self, parent=QModelIndex()):
		return 0 if parent.isValid() else len(NavpointModel.column_headers)

	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return NavpointModel.column_headers[section]

	def data(self, index, role):
		row = index.row()
		col = index.column()
		if role == Qt.DisplayRole:
			if row < len(self.navpoints): # row is a navpoint
				navpoint = self.navpoints[row]
				if col == 0:
					return Navpoint.tstr(navpoint.type)
				elif col == 1:
					return navpoint.code
				elif col == 2:
					return navpoint.long_name
			else: # row is a parking position
				pk = self.pk_pos[row - len(self.navpoints)]
				#EarthCoords, Heading, str (gate|hangar|misc|tie-down), str list (heavy|jets|turboprops|props|helos)
				if col == 0:
					return 'Pkg'
				elif col == 1:
					return pk
				elif col == 2:
					pk_info = env.airport_data.ground_net.parkingPosInfo(pk)
					txt = pk_info[2].capitalize()
					if pk_info[3] != []:
						txt += ' for ' + ', '.join(pk_info[3])
					return txt
		
		elif role == Qt.ToolTipRole:
			coords = self.coordsForRow(row)
			distance = env.radarPos().distanceTo(coords)
			return '%s°, %s' % (env.radarPos().headingTo(coords).readTrue(), dist_str(distance))
	
	## data accessors
	
	def navpointOnRow(self, row): # WARNING: fails if row is a parking position
		return self.navpoints[row]
	
	def navTypeForRow(self, row):
		return self.navpoints[row].type if row < len(self.navpoints) else None
	
	def codeForRow(self, row):
		return self.navpoints[row].code if row < len(self.navpoints) else self.pk_pos[row - len(self.navpoints)]
	
	def coordsForRow(self, row):
		if row < len(self.navpoints): # row is a navpoint
			return self.navpoints[row].coordinates
		else: # row is a parking position
			return env.airport_data.ground_net.parkingPosition(self.pk_pos[row - len(self.navpoints)])





class NavpointFilterModel(QSortFilterProxyModel):
	filtersChanged_signal = pyqtSignal()
	
	def __init__(self, base_model, parent=None):
		QSortFilterProxyModel.__init__(self, parent)
		self.setSourceModel(base_model)
		self.text_filter = re.compile('') # accepts all
		self.navtype_filters = { t: t != Navpoint.RNAV for t in Navpoint.types }
		self.include_pkg = True
		self.include_long_names = True
		self.filtersChanged_signal.connect(lambda: self.setFilterFixedString(''))
	
	def filterAcceptsRow(self, sourceRow, sourceParent):
		t = self.sourceModel().navTypeForRow(sourceRow) # None if parking pos
		if self.text_filter == None or t == None and not self.include_pkg or t != None and not self.navtype_filters[t]:
			return False # problem with regexp or row filtered out by type
		elif self.text_filter.search(self.sourceModel().codeForRow(sourceRow)):
			return True
		elif t == None or not self.include_long_names:
			return False
		else: # only thing left: search in long name
			return bool(self.text_filter.search(self.sourceModel().navpointOnRow(sourceRow).long_name))
		
	def toggleAirfieldFilter(self, b):
		self.navtype_filters[Navpoint.AD] = b
		self.filtersChanged_signal.emit()
	
	def toggleAidFilter(self, b):
		for t in [Navpoint.VOR, Navpoint.NDB, Navpoint.ILS]:
			self.navtype_filters[t] = b
		self.filtersChanged_signal.emit()
	
	def toggleFixFilter(self, b):
		self.navtype_filters[Navpoint.FIX] = b
		self.filtersChanged_signal.emit()
	
	def toggleRnavFilter(self, b):
		self.navtype_filters[Navpoint.RNAV] = b
		self.filtersChanged_signal.emit()
	
	def togglePkgFilter(self, b):
		self.include_pkg = b
		self.filtersChanged_signal.emit()
	
	def toggleLongNamesFilter(self, b):
		self.include_long_names = b
		self.filtersChanged_signal.emit()
	
	def setTextFilter(self, string):
		try:
			self.text_filter = re.compile(string, flags=re.IGNORECASE)
		except: # problem compiling regexp
			self.text_filter = None
		self.filtersChanged_signal.emit()






# ================================================ #

#                     WIDGETS                      #

# ================================================ #

class NavigatorFrame(QWidget, Ui_navigator):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.base_model = NavpointModel(env.navpoints, parent=self)
		self.table_model = NavpointFilterModel(self.base_model, parent=self)
		self.table_view.setModel(self.table_model)
		for i in range(self.table_model.columnCount()):
			self.table_view.resizeColumnToContents(i)
		self.filter_edit.setClearButtonEnabled(True)
		self.airfieldFilter_button.toggled.connect(self.table_model.toggleAirfieldFilter)
		self.aidFilter_button.toggled.connect(self.table_model.toggleAidFilter)
		self.fixFilter_button.toggled.connect(self.table_model.toggleFixFilter)
		self.rnavFilter_button.toggled.connect(self.table_model.toggleRnavFilter)
		self.pkgFilter_button.toggled.connect(self.table_model.togglePkgFilter)
		self.includeLongNamesFilter_button.toggled.connect(self.table_model.toggleLongNamesFilter)
		self.filter_edit.textChanged.connect(self.table_model.setTextFilter)
		self.table_view.doubleClicked.connect(self.indicateNavpoint)
		signals.navpointClick.connect(lambda p: self.filter_edit.setText(p.code))
		signals.pkPosClick.connect(self.filter_edit.setText)
	
	def focusInEvent(self, event):
		QWidget.focusInEvent(self, event)
		self.filter_edit.setFocus()
		self.filter_edit.selectAll()
	
	def indicateNavpoint(self, table_index):
		src_row = self.table_model.mapToSource(table_index).row()
		signals.indicatePoint.emit(self.base_model.coordsForRow(src_row))

