
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QIcon
from ui.infoPane import Ui_infoPane

from data.strip import parsed_route_detail
from game.env import env
from data.coords import RadarCoords, dist_str
from data.params import TTF_str
from gui.misc import signals, selection, IconFile
from gui.dialog.routeDialog import RouteDialog


# ---------- Constants ----------

# -------------------------------



class InfoPane(QWidget, Ui_infoPane):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.viewRoute_button.setIcon(QIcon(IconFile.button_view))
		self.airport_box.setEnabled(env.airport_data != None)
		self.radar_contact = None # RadarContact
		self.updateDisplay()
		self.cheatContact_tickBox.toggled.connect(self.setContactCheatMode)
		self.ignoreContact_tickBox.toggled.connect(self.setContactIgnore)
		self.viewRoute_button.clicked.connect(self.viewRoute)
		env.radar.blip.connect(self.updateDisplay)
		signals.selectionChanged.connect(self.updateSelection)
		signals.stripInfoChanged.connect(self.updateDisplay)
	
	def setContactCheatMode(self, b):
		self.radar_contact.setIndividualCheat(b)
		signals.stripInfoChanged.emit()
	
	def setContactIgnore(self, b):
		self.radar_contact.ignored = b
		signals.stripInfoChanged.emit()
	
	def updateSelection(self):
		self.radar_contact = selection.acft
		self.updateDisplay()
	
	def viewRoute(self):
		if self._last_known_route != None:
			spd = acft = None
			if self.radar_contact != None:
				spd = self.radar_contact.groundSpeed()
				acft = self.radar_contact.xpdrAcftType()
			RouteDialog(self._last_known_route, speedHint=spd, acftHint=acft, parent=self).exec()
	
	def updateDisplay(self):
		if self.radar_contact == None:
			self.info_area.setEnabled(False)
			self.cheatContact_tickBox.setEnabled(False)
			self.ignoreContact_tickBox.setEnabled(False)
			return
		
		self.info_area.setEnabled(True)
		self.cheatContact_tickBox.setEnabled(True)
		self.ignoreContact_tickBox.setEnabled(True)
		
		# TICK BOXES
		self.cheatContact_tickBox.setChecked(self.radar_contact.individual_cheat)
		self.ignoreContact_tickBox.setChecked(self.radar_contact.ignored)
		
		# AIRCRAFT BOX
		# Heading
		hdg = self.radar_contact.heading()
		self.aircraftHeading_info.setText('?' if hdg == None else hdg.read() + '°')
		# Alt./FL
		alt = self.radar_contact.xpdrAlt()
		if alt == None:
			self.aircraftAltitude_info.setText('?')
		else:
			qnh = env.QNH(noneSafe=False)
			if qnh == None:
				alt_str = alt.read(1013.25, step=None, unit=True)
				if not alt_str.startswith('FL'):
					alt_str += '  !!QNH?'
			else:
				alt_str = alt.read(qnh, step=None, unit=True)
			self.aircraftAltitude_info.setText(alt_str)
		# Ground speed
		groundSpeed = self.radar_contact.groundSpeed()
		if groundSpeed == None:
			self.aircraftGroundSpeed_info.setText('?')
		else:
			self.aircraftGroundSpeed_info.setText(str(groundSpeed))
		# Indicated airspeed speed
		ias = self.radar_contact.IAS()
		if ias == None:
			self.indicatedAirSpeed_info.setText('?')
		else:
			s = str(ias)
			if self.radar_contact.xpdrIAS() == None:
				s += '  !!estimate'
			self.indicatedAirSpeed_info.setText(s)
		# vertical speed
		vs = self.radar_contact.verticalSpeed()
		if vs == None:
			self.aircraftVerticalSpeed_info.setText('?')
		else:
			self.aircraftVerticalSpeed_info.setText('%+d ft/min' % vs)
		
		# ROUTE BOX
		coords = self.radar_contact.coords()
		strip = env.linkedStrip(self.radar_contact)
		route = None if strip == None else strip.lookup(parsed_route_detail)
		self._last_known_route = route
		if route == None:
			self.route_box.setEnabled(False)
		else:
			self.route_box.setEnabled(True)
			i_leg = route.currentLegIndex(coords)
			wpdist = coords.distanceTo(route.waypoint(i_leg).coordinates)
			self.legCount_info.setText('%d of %d' % (i_leg + 1, route.legCount()))
			self.legSpec_info.setText(route.legStr(i_leg))
			self.waypointAt_info.setText(dist_str(wpdist))
			# TTF
			try:
				if groundSpeed == None:
					raise ValueError('No ground speed info')
				self.waypointTTF_info.setText(TTF_str(wpdist, groundSpeed))
			except ValueError:
				self.waypointTTF_info.setText('?')
		
		# AIRPORT BOX
		if env.airport_data != None:
			acft_XY = coords.toRadarCoords()
			# Bearing
			self.airportBearing_info.setText(acft_XY.headingTo(RadarCoords(0, 0)).read())
			# Distance
			airport_dist = acft_XY.distanceTo(RadarCoords(0, 0))
			self.airportDistance_info.setText(dist_str(airport_dist))
			# TTF
			try:
				if groundSpeed == None:
					raise ValueError('No ground speed info')
				self.airportTTF_info.setText(TTF_str(airport_dist, groundSpeed))
			except ValueError:
				self.airportTTF_info.setText('?')
		
