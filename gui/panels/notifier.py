
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from os import path

from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex, QFileInfo, QUrl
from PyQt5.QtGui import QPixmap, QIcon, QColor
from PyQt5.QtWidgets import QWidget, QMenu, QAction
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from ui.notifier import Ui_notifierFrame

from game.config import settings
from game.env import env
from game.manager import GameType
from data.nav import Navpoint
from data.strip import received_from_detail, assigned_SQ_detail
from data.utc import now, timestr
from data.fpl import FPL
from gui.misc import signals


# ---------- Constants ----------

sounds_directory = 'resources/sounds'
icons_directory = 'resources/pixmap'
nearbyATC_max_dist = 1 # NM

# -------------------------------


class Notification:
	t = int
	types = GUI_INFO, ALARM_TIMEOUT, NEW_CONTACT, NEW_ATC, UNRECOGNISED_VOICE_INSTR, \
			WEATHER_UPDATE, FPL_FILED, TEXT_MSG, SOFT_LINK, LOST_CONTACT, STRIP_RECEIVED, \
			EMG_SQUAWK, RWY_INCURSION, CONFLICT_WARNING, SEPARATION_INCIDENT = range(15)
	
	def __init__(self, ntype, time, msg, action):
		self.t = ntype
		self.time = time
		self.msg = msg
		self.double_click_function = action
	
	def tstr(t):
		return {
			Notification.GUI_INFO: 'GUI message',
			Notification.ALARM_TIMEOUT: 'Alarm clock time-out',
			Notification.NEW_CONTACT: 'New radar contact',
			Notification.NEW_ATC: 'New ATC connection',
			Notification.UNRECOGNISED_VOICE_INSTR: 'Unrecognised voice instruction',
			Notification.WEATHER_UPDATE: 'Weather update',
			Notification.FPL_FILED: 'New flight plan',
			Notification.TEXT_MSG: 'Incoming text message',
			Notification.SOFT_LINK: 'Radar identification',
			Notification.LOST_CONTACT: 'Radar contact lost',
			Notification.STRIP_RECEIVED: 'Strip received',
			Notification.EMG_SQUAWK: 'Emergency squawk',
			Notification.RWY_INCURSION: 'Runway incursion',
			Notification.CONFLICT_WARNING: 'Route conflict',
			Notification.SEPARATION_INCIDENT: 'Separation incident'
		}[t]


icon_files = { # If given, the messages will appear in the notification table.
	Notification.GUI_INFO: 'info.png',
	Notification.ALARM_TIMEOUT: 'stopwatch.png',
	Notification.NEW_CONTACT: 'plane.png',
	Notification.NEW_ATC: 'control-TWR.png',
	Notification.WEATHER_UPDATE: 'weather.png',
	Notification.FPL_FILED: 'FPLicon.png',
	Notification.SOFT_LINK: 'radar.png',
	Notification.LOST_CONTACT: 'contactLost.png',
	Notification.STRIP_RECEIVED: 'envelope.png',
	Notification.EMG_SQUAWK: 'planeEMG.png',
	Notification.RWY_INCURSION: 'runway-incursion.png',
	Notification.CONFLICT_WARNING: 'routeConflict.png',
	Notification.SEPARATION_INCIDENT: 'nearMiss.png'
} # No text notification for: Notification.UNRECOGNISED_VOICE_INSTR, Notification.TEXT_MSG


sound_files = { # If given, a sound can be toggled for this type of notification.
	Notification.ALARM_TIMEOUT: 'timeout.mp3',
	Notification.NEW_CONTACT: 'aeroplaneDing.mp3',
	Notification.NEW_ATC: 'aeroplaneDing.mp3',
	Notification.UNRECOGNISED_VOICE_INSTR: 'loBuzz.mp3',
	Notification.WEATHER_UPDATE: 'chime.mp3',
	Notification.FPL_FILED: 'printer.mp3',
	Notification.SOFT_LINK: 'detectorBeep.mp3',
	Notification.TEXT_MSG: 'typeWriter.mp3',
	Notification.LOST_CONTACT: 'turnedOff.mp3',
	Notification.STRIP_RECEIVED: 'strip.mp3',
	Notification.EMG_SQUAWK: 'sq-buzz.mp3',
	Notification.RWY_INCURSION: 'alarm.mp3',
	Notification.CONFLICT_WARNING: 'alarmHalf.mp3',
	Notification.SEPARATION_INCIDENT: 'alarm.mp3'
} # No sound notification for: Notification.GUI_INFO


def mkSound(file_base_name):
	return QMediaContent(QUrl.fromLocalFile(QFileInfo(path.join(sounds_directory, file_base_name)).absoluteFilePath()))

wilco_beep = mkSound('hiBuzz.mp3')

notification_sounds = { t: mkSound(f) for t, f in sound_files.items() }

def initial_sound_notifications():
	return settings.notification_sounds
	





class NotificationHistoryModel(QAbstractTableModel):

	columns = ['Time', 'Message']

	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.history = []

	def rowCount(self, parent=None):
		return len(self.history)

	def columnCount(self, parent):
		return len(NotificationHistoryModel.columns)

	def data(self, index, role):
		col = index.column()
		notification = self.history[index.row()]
		if role == Qt.DisplayRole:
			if col == 0:
				return timestr(notification.time, seconds=True)
			if col == 1:
				return notification.msg
		elif role == Qt.DecorationRole:
			if col == 0:
				try:
					pixmap = QPixmap(path.join(icons_directory, icon_files[notification.t]))
					return QIcon(pixmap)
				except KeyError:
					pass # No decoration for this notification type

	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return NotificationHistoryModel.columns[section]
	
	def addNotification(self, t, msg, onclick):
		position = self.rowCount()
		self.beginInsertRows(QModelIndex(), position, position)
		self.history.insert(position, Notification(t, now(), msg, onclick))
		self.endInsertRows()
		return True

	def clearNotifications(self):
		self.beginRemoveRows(QModelIndex(), 0, self.rowCount() - 1)
		self.history.clear()
		self.endRemoveRows()
		return True




class NotifierFrame(QWidget, Ui_notifierFrame):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.table_model = NotificationHistoryModel(self)
		self.notification_table.setModel(self.table_model)
		if settings.notification_sounds == None:
			settings.notification_sounds = {
				Notification.ALARM_TIMEOUT, Notification.LOST_CONTACT, Notification.UNRECOGNISED_VOICE_INSTR,
				Notification.NEW_ATC, Notification.WEATHER_UPDATE, Notification.STRIP_RECEIVED, Notification.EMG_SQUAWK,
				Notification.RWY_INCURSION, Notification.CONFLICT_WARNING, Notification.SEPARATION_INCIDENT
			}
		self.media_player = QMediaPlayer()
		self.PTT_active = False
		self.notification_table.doubleClicked.connect(self.doAction)
		self.cleanUp_button.clicked.connect(self.table_model.clearNotifications)
		# SHOW menu
		self.sound_menu = QMenu()
		self.PTTturnsOffSounds_action = QAction('Mute on PTT (recommended if no headset)', self)
		self.PTTturnsOffSounds_action.setCheckable(True)
		self.PTTturnsOffSounds_action.setChecked(settings.PTT_mutes_sound_notifications)
		self.sound_menu.addAction(self.PTTturnsOffSounds_action)
		self.PTTturnsOffSounds_action.toggled.connect(self.togglePTTmute)
		self.sound_menu.addSeparator()
		for t in notification_sounds:
			self._addSoundToggleAction(t)
		self.sounds_menuButton.setMenu(self.sound_menu)
		# Notification signals
		env.radar.newContact.connect(self.notifyNewContact)
		env.radar.emergencySquawk.connect(self.notifyEmergencySquawk)
		env.radar.pathConflict.connect(lambda: self.notify(Notification.CONFLICT_WARNING, 'Anticipated conflict'))
		env.radar.nearMiss.connect(lambda: self.notify(Notification.SEPARATION_INCIDENT, 'Loss of separation!'))
		env.radar.runwayIncursion.connect(self.notifyRwyIncursion)
		signals.gameStarted.connect(self.notifyGameStarted)
		signals.gameStopped.connect(lambda: self.notify(Notification.GUI_INFO, 'Game stopped.'))
		signals.gamePaused.connect(lambda: self.notify(Notification.GUI_INFO, 'Simulation paused.'))
		signals.gameResumed.connect(lambda: self.notify(Notification.GUI_INFO, 'Simulation resumed.'))
		signals.newWeather.connect(self.notifyNewWeather)
		signals.voiceMsgNotRecognised.connect(lambda: self.notify(Notification.UNRECOGNISED_VOICE_INSTR, 'Voice instruction not recognised.'))
		signals.newATC.connect(self.notifyNewATC)
		signals.newFPL.connect(self.notifyNewFlightPlan)
		signals.controlledContactLost.connect(self.notifyLostLinkedContact)
		signals.incomingTextChat.connect(lambda msg: self.notify(Notification.TEXT_MSG, None))
		signals.aircraftIdentification.connect(self.notifyAircraftIdentification)
		signals.receiveStrip.connect(self.notifyStripReceived)
		signals.radioPTT.connect(self.catchPTT) # Catches kbdPTT if needed because kbdPTT implies radioPTT for every PTTed radio
		signals.wilco.connect(self.wilco)
	
	def _addSoundToggleAction(self, t):
		action = QAction(Notification.tstr(t), self)
		action.setCheckable(True)
		self.sound_menu.addAction(action)
		action.setChecked(t in settings.notification_sounds)
		action.toggled.connect(lambda b: self.enableSound(t, b))
	
	def catchPTT(self, ignore_radio_identification, ptt_toggle):
		self.PTT_active = ptt_toggle
	
	def enableSound(self, t, toggle):
		if toggle:
			settings.notification_sounds.add(t)
		else:
			settings.notification_sounds.remove(t)
	
	def togglePTTmute(self, toggle):
		settings.PTT_mutes_sound_notifications = toggle
	
	def wilco(self):
		self.playSound(wilco_beep)
	
	def playSound(self, sound):
		self.media_player.setMedia(sound)
		self.media_player.play()
	
	def notify(self, t, msg, onclick=None):
		if msg != None:
			signals.statusBarMsg.emit(msg)
			if t in icon_files:
				self.table_model.addNotification(t, msg, onclick)
				self.notification_table.scrollToBottom()
		if settings.notification_sounds_enabled and not settings.game_start_sound_lock \
		and t in settings.notification_sounds and not (self.PTT_active and settings.PTT_mutes_sound_notifications):
			self.playSound(notification_sounds[t])
	
	def notifyNewATC(self, callsign):
		self.notify(Notification.NEW_ATC, 'ATC %s connected' % callsign)
	
	def notifyNewContact(self, radar_contact):
		pos = radar_contact.coords()
		msg = 'New contact ' + map_loc_str(pos)
		sq = radar_contact.xpdrSqCode()
		if sq != None:
			msg += ' squawking %04o' % sq
		self.notify(Notification.NEW_CONTACT, msg, onclick=(lambda: signals.indicatePoint.emit(pos)))
	
	def notifyLostLinkedContact(self, strip, pos):
		cs = strip.callsign(fpl=True)
		msg = 'Radar contact lost'
		if cs != None:
			msg += ' for ' + cs
		msg += ' ' + map_loc_str(pos)
		self.notify(Notification.LOST_CONTACT, msg, onclick=(lambda: signals.indicatePoint.emit(pos)))
	
	def notifyAircraftIdentification(self, strip, acft, modeS):
		if strip.linkedAircraft() is not acft: # could already be hard linked if XPDR was turned off and back on (avoid too many signals)
			if modeS:
				msg = 'Callsign %s identified (mode S)' % strip.lookup(FPL.CALLSIGN)
			else:
				msg = 'XPDR code %04o identified' % strip.lookup(assigned_SQ_detail)
			self.notify(Notification.SOFT_LINK, msg)
	
	def notifyAlarmClockTimedOut(self, timer):
		self.notify(Notification.ALARM_TIMEOUT, 'Alarm clock %s timed out.' % timer)
	
	def notifyStripReceived(self, strip):
		self.notify(Notification.STRIP_RECEIVED, 'Strip received from %s' % strip.lookup(received_from_detail))
	
	def notifyGameStarted(self):
		txt = {
				GameType.SOLO: 'Solo game started.',
				GameType.FLIGHTGEAR_MP: 'FlightGear multi-player connected.',
				GameType.STUDENT: 'Student session beginning.',
				GameType.TEACHER: 'Teacher session beginning.'
			}[settings.game_manager.game_type]
		self.notify(Notification.GUI_INFO, txt)
	
	def notifyNewWeather(self, station, weather):
		if station == settings.primary_METAR_station:
			self.notify(Notification.WEATHER_UPDATE, 'Weather update: %s' % weather.METAR())
	
	def notifyNewFlightPlan(self, fpl):
		self.notify(Notification.FPL_FILED, 'Online FPL downloaded')
	
	def notifyEmergencySquawk(self, acft):
		coords = acft.coords()
		f = lambda: signals.indicatePoint.emit(coords)
		self.notify(Notification.EMG_SQUAWK, 'Aircraft squawking emergency', onclick=f)
	
	def notifyRwyIncursion(self, phyrwy, acft):
		rwy = env.airport_data.physicalRunwayNameFromUse(phyrwy)
		f = lambda p=acft.coords(): signals.indicatePoint.emit(p)
		self.notify(Notification.RWY_INCURSION, 'Runway %s incursion!' % rwy, onclick=f)
	
	# On action double-click:
	def doAction(self):
		try:
			notification = self.table_model.history[self.notification_table.selectedIndexes()[0].row()]
		except IndexError:
			return
		if notification.double_click_function != None:
			notification.double_click_function()





def map_loc_str(pos):
	if env.pointOnMap(pos):
		return ' near %s' % env.navpoints.findClosest(pos)
	else:
		return ' far %s' % env.radarPos().headingTo(pos).approxCardinal(True)
