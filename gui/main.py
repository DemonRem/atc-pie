
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from os import path

from PyQt5.QtCore import Qt, QUrl, QTimer
from PyQt5.QtGui import QDesktopServices, QIcon
from PyQt5.QtWidgets import QMainWindow, QInputDialog, QLabel, QMessageBox, QMenu, QAction, QActionGroup

from ui.mainWindow import Ui_mainWindow

from models.gameStrips import default_rack_name
from models.discardedStrips import ShelfFilterModel

from data.params import hPa2inHg
from data.utc import timestr
from data.fpl import FPL

from game.config import settings
from game.env import env
from game.manager import GameManager, GameType
from game.flightGearMP import FlightGearMPgameManager
from game.teacher import TeacherSessionGameManager
from game.student import StudentSessionGameManager
from game.solo import SoloGameManager_AD, SoloGameManager_CTR

from ext.resources import read_bg_img, read_route_presets
from ext.osm import download_OSM_tile
from ext.fgfs import FlightGearTowerViewer

from gui.misc import Ticker, IconFile, signals, selection
from gui.stripRackActions import new_strip_dialog, edit_strip, receive_strip, recover_strip, recover_from_failed_handover
from gui.panels.radarScope import ScopeFrame
from gui.panels.stripPanes import LooseStripPane
from gui.widgets.basicWidgets import AlarmClockButton
from gui.widgets.miscWidgets import QuickReferenceWindow, RadioKeyEventFilter
from gui.dialog.startGame import StartSoloDialog_AD, StartFlightGearMPdialog, StartStudentSessionDialog
from gui.dialog.settings import LocalSettingsDialog, GeneralSettingsDialog, SystemSettingsDialog, SoloGameSettingsDialog
from gui.dialog.bgImg import DownloadOSMPictureDialog
from gui.dialog.runways import RunwayUseDialog
from gui.dialog.miscDialogs import yesNo_question, AboutDialog, \
		PostLennySessionDialog, DiscardedStripsDialog, EnvironmentInfoDialog


# ---------- Constants ----------

clock_tick_interval = 333 # ms
status_bar_message_timeout = 5000 # ms
game_start_sound_lock_duration = 3000 # ms

dock_flash_stylesheet = 'QDockWidget::title { background: yellow }'
dock_flash_time = 750 # ms

window_state_file = 'settings/window_state'
airport_gateway_URL = 'http://gateway.x-plane.com/airports/page'
video_tutorial_URL = 'http://www.youtube.com/playlist?list=PL1EQKKHhDVJvvWpcX_BqeOIsmeW2A_8Yb'

# -------------------------------


def setDockAndActionIcon(icon_file, action, dock):
	icon = QIcon(icon_file)
	action.setIcon(icon)
	dock.setWindowIcon(icon)



class MainWindow(QMainWindow, Ui_mainWindow):
	def __init__(self, launcher, parent=None):
		QMainWindow.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setAttribute(Qt.WA_DeleteOnClose)
		self.launcher = launcher
		settings.controlled_tower_viewer = FlightGearTowerViewer(self)
		settings.game_manager = GameManager(self)
		self.setWindowTitle('%s - %s (%s)' % (self.windowTitle(), env.locationName(), settings.location_code))
		self.game_start_sound_lock_timer = QTimer(self)
		self.game_start_sound_lock_timer.setSingleShot(True)
		self.game_start_sound_lock_timer.timeout.connect(self.unlockSounds)
		
		# set up window state
		try:
			with open(window_state_file, 'rb') as f:
				self.restoreState(f.read())
		except FileNotFoundError: # Initial dock placement
			self.tabifyDockWidget(self.FPLlist_dock, self.notepads_dock)
			self.tabifyDockWidget(self.FPLlist_dock, self.instructions_dock)
			self.tabifyDockWidget(self.FPLlist_dock, self.weather_dock)
			self.tabifyDockWidget(self.FPLlist_dock, self.navigator_dock)
			self.tabifyDockWidget(self.notification_dock, self.radio_dock)
			self.tabifyDockWidget(self.notification_dock, self.towerView_dock)
			self.tabifyDockWidget(self.notification_dock, self.teachingConsole_dock)
			self.radio_dock.hide()
			self.towerView_dock.hide()
			self.teachingConsole_dock.hide()
			self.FPLlist_dock.hide()
			self.instructions_dock.hide()
			self.notepads_dock.hide()
		
		## Status bar widgets (permanent, right-hand side)
		self.METAR_statusBarLabel = QLabel()
		self.PTT_statusBarLabel = QLabel()
		self.wind_statusBarLabel = QLabel()
		self.QNH_statusBarLabel = QLabel()
		self.clock_statusBarLabel = QLabel()
		self.alarmClock_statusBarButtons = [AlarmClockButton('1', self), AlarmClockButton('2', self)]
		self.statusbar.addWidget(self.METAR_statusBarLabel)
		self.statusbar.addPermanentWidget(self.PTT_statusBarLabel)
		self.statusbar.addPermanentWidget(self.wind_statusBarLabel)
		self.statusbar.addPermanentWidget(self.QNH_statusBarLabel)
		for b in self.alarmClock_statusBarButtons:
			self.statusbar.addPermanentWidget(b)
			b.alarm.connect(self.notification_pane.notifyAlarmClockTimedOut)
		self.statusbar.addPermanentWidget(self.clock_statusBarLabel)
		
		## Memory-persistent dialogs and windows
		self.solo_connect_dialog_AD = StartSoloDialog_AD(self)
		self.MP_connect_dialog = StartFlightGearMPdialog(self)
		self.start_student_session_dialog = StartStudentSessionDialog(self)
		self.recall_cheat_dialog = DiscardedStripsDialog(self, ShelfFilterModel(self, env.discarded_strips, False), 'Sent and deleted strips')
		self.shelf_dialog = DiscardedStripsDialog(self, ShelfFilterModel(self, env.discarded_strips, True), 'Strip shelf')
		self.environment_info_dialog = EnvironmentInfoDialog(self)
		self.download_OSM_bg_dialog = DownloadOSMPictureDialog(self)
		self.about_dialog = AboutDialog(self)
		self.quick_reference = QuickReferenceWindow(parent=None)
		
		# Make a few actions always visible
		self.addAction(self.newStrip_action)
		self.addAction(self.newLinkedStrip_action)
		self.addAction(self.newFPL_action)
		self.addAction(self.newLinkedFPL_action)
		self.addAction(self.startTimer1_action)
		self.addAction(self.forceStartTimer1_action)
		self.addAction(self.startTimer2_action)
		self.addAction(self.forceStartTimer2_action)
		self.addAction(self.quickReference_help_action)
		self.addAction(self.saveWindowState_view_action)
		self.addAction(self.recallWindowState_view_action)
		
		# Fill missing menu/toolbar actions
		centralView_menu = QMenu()
		centralView_actionGroup = QActionGroup(self)
		centralView_actionGroup.addAction(self.radarScope_mainWindowView_action) # action defined with designer
		centralView_actionGroup.addAction(self.stripRacks_mainWindowView_action) # action defined with designer
		centralView_actionGroup.addAction(self.looseStripBay_mainWindowView_action) # action defined with designer
		centralView_menu.addActions(centralView_actionGroup.actions())
		self.mainWindow_view_menuAction.setMenu(centralView_menu)
		
		toolbar_menu = QMenu()
		self.general_viewToolbar_action = self.general_toolbar.toggleViewAction()
		self.stripActions_viewToolbar_action = self.stripActions_toolbar.toggleViewAction()
		self.docks_viewToolbar_action = self.docks_toolbar.toggleViewAction()
		toolbar_menu.addAction(self.general_viewToolbar_action)
		toolbar_menu.addAction(self.stripActions_viewToolbar_action)
		toolbar_menu.addAction(self.docks_viewToolbar_action)
		self.toolbars_view_menuAction.setMenu(toolbar_menu)
		
		if env.airport_data == None or len(env.airport_data.viewpoints) == 0:
			self.viewpointSelection_view_menuAction.setEnabled(False)
		else:
			viewPointSelection_menu = QMenu()
			viewPointSelection_actionGroup = QActionGroup(self)
			for vp_i, (vp_pos, vp_h, vp_name) in enumerate(env.airport_data.viewpoints):
				action = QAction('%s - %d ft ASFC' % (vp_name, vp_h + .5), self)
				action.setCheckable(True)
				action.triggered.connect(lambda ignore_checked, i=vp_i: self.selectIndicateViewpoint(i))
				viewPointSelection_actionGroup.addAction(action)
			actions = viewPointSelection_actionGroup.actions()
			viewPointSelection_menu.addActions(actions)
			self.viewpointSelection_view_menuAction.setMenu(viewPointSelection_menu)
			try:
				actions[settings.selected_viewpoint].setChecked(True)
			except IndexError:
				actions[0].setChecked(True)
		
		# Populate icons
		self.newStrip_action.setIcon(QIcon(IconFile.action_newStrip))
		self.newLinkedStrip_action.setIcon(QIcon(IconFile.action_newLinkedStrip))
		self.newFPL_action.setIcon(QIcon(IconFile.action_newFPL))
		self.newLinkedFPL_action.setIcon(QIcon(IconFile.action_newLinkedFPL))
		self.environmentInfo_view_action.setIcon(QIcon(IconFile.action_envInfo))
		self.generalSettings_options_action.setIcon(QIcon(IconFile.action_generalSettings))
		self.runwaysInUse_options_action.setIcon(QIcon(IconFile.action_runwayUse))
		self.newRadarScope_view_action.setIcon(QIcon(IconFile.action_newRadar))
		self.newLooseStripBay_view_action.setIcon(QIcon(IconFile.action_newLooseBay))
		
		setDockAndActionIcon(IconFile.dock_ATCs, self.handovers_dockView_action, self.handover_dock)
		setDockAndActionIcon(IconFile.dock_FPLs, self.FPLs_dockView_action, self.FPLlist_dock)
		setDockAndActionIcon(IconFile.dock_instructions, self.instructions_dockView_action, self.instructions_dock)
		setDockAndActionIcon(IconFile.dock_navigator, self.navpoints_dockView_action, self.navigator_dock)
		setDockAndActionIcon(IconFile.dock_notepads, self.notepads_dockView_action, self.notepads_dock)
		setDockAndActionIcon(IconFile.dock_notifications, self.notificationArea_dockView_action, self.notification_dock)
		setDockAndActionIcon(IconFile.dock_radios, self.fgcom_dockView_action, self.radio_dock)
		setDockAndActionIcon(IconFile.dock_selInfo, self.infoPane_dockView_action, self.info_dock)
		setDockAndActionIcon(IconFile.dock_strips, self.strips_dockView_action, self.strip_dock)
		setDockAndActionIcon(IconFile.dock_teaching, self.teachingConsole_dockView_action, self.teachingConsole_dock)
		setDockAndActionIcon(IconFile.dock_txtChat, self.textChat_dockView_action, self.textChat_dock)
		setDockAndActionIcon(IconFile.dock_twrView, self.towerView_dockView_action, self.towerView_dock)
		setDockAndActionIcon(IconFile.dock_weather, self.weather_dockView_action, self.weather_dock)
		
		## CONNECT actions
		# non-menu actions
		self.newStrip_action.triggered.connect(lambda: new_strip_dialog(self, default_rack_name, linkToSelection=False))
		self.newLinkedStrip_action.triggered.connect(lambda: new_strip_dialog(self, default_rack_name, linkToSelection=True))
		self.newFPL_action.triggered.connect(lambda: self.FPLlist_pane.createLocalFPL(link=None))
		self.newLinkedFPL_action.triggered.connect(lambda: self.FPLlist_pane.createLocalFPL(link=selection.strip))
		self.startTimer1_action.triggered.connect(lambda: self.startTimer(0, False))
		self.forceStartTimer1_action.triggered.connect(lambda: self.startTimer(0, True))
		self.startTimer2_action.triggered.connect(lambda: self.startTimer(1, False))
		self.forceStartTimer2_action.triggered.connect(lambda: self.startTimer(1, True))
		# system menu
		self.playSoloGame_system_action.triggered.connect(lambda: self.toggleGame(self.start_solo))
		self.connectFlightGearMP_system_action.triggered.connect(lambda: self.toggleGame(self.start_FlightGearMP))
		self.studentSession_system_action.triggered.connect(lambda: self.toggleGame(self.start_learning))
		self.teacherSession_system_action.triggered.connect(lambda: self.toggleGame(self.start_teaching))
		self.announceFgSession_system_action.triggered.connect(self.announceFgSession)
		self.fgcomEchoTest_system_action.triggered.connect(self.radio_pane.performEchoTest)
		self.reloadAdditionalViewers_system_action.triggered.connect(self.reloadAdditionalViewers)
		self.reloadBgImages_system_action.triggered.connect(self.reloadBackgroundImages)
		self.reloadColourConfig_system_action.triggered.connect(self.reloadColourConfig)
		self.reloadRoutePresets_system_action.triggered.connect(self.reloadRoutePresets)
		self.airportGateway_system_action.triggered.connect(lambda: self.goToURL(airport_gateway_URL))
		self.downloadOSMbackground_system_action.triggered.connect(self.downloadOSMbackground)
		self.repositionBgImages_system_action.triggered.connect(self.central_radar_scope.positionVisibleBgImages)
		self.systemSettings_system_action.triggered.connect(self.openSystemSettings)
		self.changeLocation_system_action.triggered.connect(self.changeLocation)
		self.quit_system_action.triggered.connect(self.close)
		# view menu
		self.flashRaisedPanels_view_action.setChecked(settings.flash_raised_panels)
		self.radarScope_mainWindowView_action.triggered.connect(lambda: self.central_widget.setCurrentWidget(self.central_radar_page))
		self.stripRacks_mainWindowView_action.triggered.connect(lambda: self.central_widget.setCurrentWidget(self.central_racks_page))
		self.looseStripBay_mainWindowView_action.triggered.connect(lambda: self.central_widget.setCurrentWidget(self.central_loose_bay_page))
		self.flashRaisedPanels_view_action.triggered.connect(self.switchFlashDocks)
		self.saveWindowState_view_action.triggered.connect(self.saveWindowState)
		self.recallWindowState_view_action.triggered.connect(self.recallWindowState)
		self.towerView_view_action.triggered.connect(self.toggleTowerWindow)
		self.addViewer_view_action.triggered.connect(self.addView)
		self.listViewers_view_action.triggered.connect(self.listAdditionalViews)
		self.activateAdditionalViewers_view_action.toggled.connect(self.activateAdditionalViews)
		self.environmentInfo_view_action.triggered.connect(self.environment_info_dialog.exec)
		self.newLooseStripBay_view_action.triggered.connect(self.newLooseStripBay)
		self.newRadarScope_view_action.triggered.connect(self.newScopeWindow)
		self.removeViewer_view_action.triggered.connect(self.removeView)
		self.handovers_dockView_action.triggered.connect(lambda: self.raiseDock(self.handover_dock))
		self.FPLs_dockView_action.triggered.connect(lambda: self.raiseDock(self.FPLlist_dock))
		self.instructions_dockView_action.triggered.connect(lambda: self.raiseDock(self.instructions_dock))
		self.navpoints_dockView_action.triggered.connect(lambda: self.raiseDock(self.navigator_dock))
		self.notepads_dockView_action.triggered.connect(lambda: self.raiseDock(self.notepads_dock))
		self.notificationArea_dockView_action.triggered.connect(lambda: self.raiseDock(self.notification_dock))
		self.fgcom_dockView_action.triggered.connect(lambda: self.raiseDock(self.radio_dock))
		self.infoPane_dockView_action.triggered.connect(lambda: self.raiseDock(self.info_dock))
		self.strips_dockView_action.triggered.connect(lambda: self.raiseDock(self.strip_dock))
		self.teachingConsole_dockView_action.triggered.connect(lambda: self.raiseDock(self.teachingConsole_dock))
		self.textChat_dockView_action.triggered.connect(lambda: self.raiseDock(self.textChat_dock))
		self.towerView_dockView_action.triggered.connect(lambda: self.raiseDock(self.towerView_dock))
		self.weather_dockView_action.triggered.connect(lambda: self.raiseDock(self.weather_dock))
		# options menu
		self.notificationSounds_options_action.setChecked(settings.notification_sounds_enabled)
		self.primaryRadar_options_action.setChecked(settings.primary_radar_active)
		self.routeConflictWarnings_options_action.setChecked(settings.route_conflict_warnings)
		self.trafficIdentification_options_action.setChecked(settings.traffic_identification_assistant)
		self.runwayOccupationWarnings_options_action.setChecked(settings.monitor_runway_occupation)
		self.approachSpacingHints_options_action.setChecked(settings.APP_spacing_hints)
		self.runwaysInUse_options_action.triggered.connect(self.configureRunwayUse)
		self.notificationSounds_options_action.toggled.connect(self.switchNotificationSounds)
		self.primaryRadar_options_action.toggled.connect(self.activatePrimaryRadar)
		self.routeConflictWarnings_options_action.toggled.connect(self.switchConflictWarnings)
		self.trafficIdentification_options_action.toggled.connect(self.switchTrafficIdentification)
		self.runwayOccupationWarnings_options_action.toggled.connect(self.switchRwyOccupationIndications)
		self.approachSpacingHints_options_action.toggled.connect(self.switchApproachSpacingHints)
		self.soloGameSettings_options_action.triggered.connect(self.openSoloGameSettings)
		self.locationSettings_options_menuAction.triggered.connect(self.openLocalSettings)
		self.generalSettings_options_action.triggered.connect(self.openGeneralSettings)
		# cheat menu
		self.pauseSimulation_cheat_action.toggled.connect(self.pauseGame)
		self.spawnAircraft_cheat_action.triggered.connect(self.spawnAircraft)
		self.killSelectedAircraft_cheat_action.triggered.connect(self.killSelectedAircraft)
		self.popUpMsgOnRejectedInstr_cheat_action.toggled.connect(self.setRejectedInstrPopUp)
		self.showRecognisedVoiceStrings_cheat_action.toggled.connect(self.setShowRecognisedVoiceStrings)
		self.ensureClearWeather_cheat_action.triggered.connect(self.towerView_pane.ensureClearWeather)
		self.ensureDayLight_cheat_action.triggered.connect(self.towerView_pane.ensureDayLight)
		self.changeTowerHeight_cheat_action.triggered.connect(self.changeTowerHeight)
		self.recallDiscardedStrip_cheat_action.triggered.connect(self.recall_cheat_dialog.exec)
		self.radarCheatMode_cheat_action.toggled.connect(self.setRadarCheatMode)
		# help menu
		self.quickReference_help_action.triggered.connect(self.quick_reference.show)
		self.videoTutorial_help_action.triggered.connect(lambda: self.goToURL(video_tutorial_URL))
		self.about_help_action.triggered.connect(self.about_dialog.exec)
		
		## More signal connections
		signals.openShelfRequest.connect(self.shelf_dialog.exec)
		signals.stripRecall.connect(recover_strip)
		env.radar.blip.connect(env.strips.refreshViews)
		env.radar.lostContact.connect(self.aircraftHasDisappeared)
		signals.aircraftKilled.connect(self.aircraftHasDisappeared)
		env.strips.rwyBoxFreed.connect(lambda box, strip: env.airport_data.physicalRunway_restartWtcTimer(box, strip.lookup(FPL.WTC)))
		signals.statusBarMsg.connect(lambda msg: self.statusbar.showMessage(msg, status_bar_message_timeout))
		signals.newWeather.connect(self.updateStatusBarWeather)
		signals.kbdPTT.connect(self.updatePTT)
		signals.gameStarted.connect(self.gameHasStarted)
		signals.gameStopped.connect(self.gameHasStopped)
		signals.towerViewProcessToggled.connect(self.towerView_view_action.setChecked)
		signals.towerViewProcessToggled.connect(self.towerView_cheat_menu.setEnabled)
		signals.stripInfoChanged.connect(env.strips.refreshViews)
		signals.clockTick.connect(self.updateClock)
		signals.stripEditRequest.connect(lambda strip: edit_strip(self, strip))
		signals.selectionChanged.connect(self.updateStripFplActions)
		signals.receiveStrip.connect(receive_strip)
		signals.handoverFailure.connect(lambda strip, msg: recover_from_failed_handover(self, strip, msg))
		signals.gamePaused.connect(env.radar.stopSweeping)
		signals.gameResumed.connect(env.radar.startSweeping)
		signals.aircraftKilled.connect(env.radar.silentlyForgetContact)
		
		## MISC GUI setup
		self.UTC_ticker = Ticker(signals.clockTick.emit, parent=self)
		self.UTC_ticker.start_stopOnZero(clock_tick_interval)
		self.towerView_cheat_menu.setEnabled(False)
		self.solo_cheat_menu.setEnabled(False)
		self.updateClock()
		self.updateStatusBarWeather()
		self.updateStripFplActions()
		self.radarScope_mainWindowView_action.setChecked(True)
		self.central_widget.setCurrentWidget(self.central_radar_page)
		# Disable some base airport stuff if doing CTR
		if env.airport_data == None:
			self.towerView_view_action.setEnabled(False)
			self.runwaysInUse_options_action.setEnabled(False)
			self.runwayOccupationWarnings_options_action.setEnabled(False)
	
	def raiseDock(self, dock):
		dock.show()
		dock.raise_()
		dock.widget().setFocus()
		if settings.flash_raised_panels:
			dock.setStyleSheet(dock_flash_stylesheet)
			QTimer.singleShot(dock_flash_time, lambda: dock.setStyleSheet(None))
	
	def startTimer(self, i, force_start):
		if force_start or not self.alarmClock_statusBarButtons[i].timerIsRunning():
			self.alarmClock_statusBarButtons[i].setTimer()
	
	def goToURL(self, url):
		if not QDesktopServices.openUrl(QUrl(url)):
			QMessageBox.critical(self, 'Error opening web browser', \
				'Could not invoke desktop web browser.\nGo to: %s' % url)
	
	
	# ---------------------     GUI auto-update functions     ---------------------- #
	
	def updateClock(self):
		self.clock_statusBarLabel.setText('UTC ' + timestr(seconds=True))
	
	def updateStatusBarWeather(self, station=None):
		if station == None or station == settings.primary_METAR_station:
			w = env.primaryWeather()
			self.METAR_statusBarLabel.setText(None if w == None else w.METAR())
			self.wind_statusBarLabel.setText('Wind ' + ('---' if w == None else w.readWind()))
			qnh = env.QNH(noneSafe=False)
			self.QNH_statusBarLabel.setText('QNH ' + ('%d / %.2f' % (qnh, hPa2inHg * qnh) if qnh != None else '---'))
	
	def updatePTT(self, button, pressed):
		txt = ''
		if settings.game_manager.isRunning():
			if settings.game_manager.game_type == GameType.SOLO:
				if settings.solo_voice_instructions:
					txt = 'PTT' if pressed else 'VOICE'
				else:
					txt = 'MOUSE'
			elif pressed:
				txt = 'PTT'
		self.PTT_statusBarLabel.setText(txt)
	
	def updateGameToggleMenuActions(self):
		running = settings.game_manager.isRunning()
		for gt, ma in {
					GameType.SOLO: self.playSoloGame_system_action,
					GameType.FLIGHTGEAR_MP: self.connectFlightGearMP_system_action,
					GameType.STUDENT: self.studentSession_system_action,
					GameType.TEACHER: self.teacherSession_system_action
				}.items():
			if gt == settings.game_manager.game_type:
				ma.setEnabled(True)
				ma.setChecked(running)
			else:
				ma.setEnabled(not running)
				ma.setChecked(False)
	
	def updateStripFplActions(self):
		self.newLinkedStrip_action.setEnabled(selection.strip == None and not selection.acft == selection.fpl == None)
		self.newLinkedFPL_action.setEnabled(selection.strip != None and selection.strip.linkedFPL() == None)
	
	def gameHasStarted(self):
		self.game_start_sound_lock_timer.start(game_start_sound_lock_duration)
		self.updateGameToggleMenuActions()
		self.solo_cheat_menu.setEnabled(settings.game_manager.game_type == GameType.SOLO)
		self.updatePTT(0, False)
		env.radar.startSweeping()
		
	def gameHasStopped(self):
		env.radar.stopSweeping()
		env.radar.resetContacts()
		env.strips.removeAllStrips()
		self.updateGameToggleMenuActions()
		self.pauseSimulation_cheat_action.setChecked(False)
		self.solo_cheat_menu.setEnabled(False)
		self.updatePTT(0, False)
		print('Game has stopped.')
	
	def aircraftHasDisappeared(self, acft):
		strip = env.linkedStrip(acft)
		if strip != None:
			strip.linkAircraft(None)
		if selection.acft is acft:
			if strip == None: # was not linked
				selection.deselect()
			else:
				selection.selectStrip(strip)
	
	
	# ---------------------     Game start functions     ---------------------- #
	
	def start_solo(self):
		if env.airport_data == None: # start CTR solo game manager
			n, ok = QInputDialog.getInt(self, 'Solo CTR game', 'Initial traffic count:', value=2, min=0, max=99)
			if ok:
				settings.game_manager = SoloGameManager_CTR(self)
				settings.game_manager.start(n)
		else: # start AD solo game manager
			self.solo_connect_dialog_AD.exec()
			if self.solo_connect_dialog_AD.result() > 0: # not rejected
				settings.game_manager = SoloGameManager_AD(self)
				settings.game_manager.start(self.solo_connect_dialog_AD.initialTrafficCount())
	
	def start_FlightGearMP(self):
		self.MP_connect_dialog.exec()
		if self.MP_connect_dialog.result() > 0: # not rejected
			settings.game_manager = FlightGearMPgameManager(self, self.MP_connect_dialog.chosenCallsign())
			settings.game_manager.start()
	
	def start_teaching(self):
		port, ok = QInputDialog.getInt(self, 'Start a teaching session', 'Service port:', value=settings.teaching_service_port, max=99999)
		if ok:
			settings.teaching_service_port = port
			settings.game_manager = TeacherSessionGameManager(self)
			settings.game_manager.start()
	
	def start_learning(self):
		self.start_student_session_dialog.exec()
		if self.start_student_session_dialog.result() > 0: # not rejected
			settings.game_manager = StudentSessionGameManager(self)
			settings.game_manager.start()
	
	
	# ---------------------     GUI menu actions     ---------------------- #
	
	## SYSTEM MENU ##
	
	def toggleGame(self, start_func):
		if settings.game_manager.isRunning(): # Stop game
			selection.deselect()
			settings.game_manager.stop()
		else: # Start game
			settings.game_start_sound_lock = True
			start_func()
		self.updateGameToggleMenuActions()
		if not settings.game_manager.isRunning():
			settings.game_start_sound_lock = False
	
	def unlockSounds(self):
		settings.game_start_sound_lock = False
	
	def announceFgSession(self):
		if settings.lenny64_account_email == '':
			QMessageBox.critical(self, 'Lenny64 account details missing', \
				'This feature requires a Lenny64 dashboard. Please provide one in the FlightGear system set-up tab.')
		else:
			PostLennySessionDialog(self).exec()
	
	def reloadAdditionalViewers(self):
		print('Reload: additional viewers')
		settings.loadAdditionalViews()
		QMessageBox.information(self, 'Done reloading', 'Additional viewers reloaded. Check for console error messages.')
	
	def reloadBackgroundImages(self):
		print('Reload: background images')
		settings.radar_background_images, settings.loose_strip_bay_backgrounds = read_bg_img(settings.location_code, env.navpoints)
		signals.backgroundImagesReloaded.emit()
		QMessageBox.information(self, 'Done reloading', 'Background images reloaded. Check for console error messages.')
	
	def reloadColourConfig(self):
		print('Reload: colour configuration')
		settings.loadColourSettings()
		signals.colourConfigReloaded.emit()
		QMessageBox.information(self, 'Done reloading', 'Colour configuration reloaded. Check for console error messages.')
	
	def reloadRoutePresets(self):
		print('Reload: route presets')
		settings.route_presets = read_route_presets()
		QMessageBox.information(self, 'Done reloading', 'Route presets reloaded. Check for console error messages.')
	
	def downloadOSMbackground(self):
		self.download_OSM_bg_dialog.exec()
		if self.download_OSM_bg_dialog.result() > 0: # not rejected (incl. valid point specs)
			QMessageBox.information(self, 'OpenStreetMap download', \
					'Depending on the size of the picture, this may freeze the program for a minute.')
			s_nw, s_se = self.download_OSM_bg_dialog.imageCornerPoints()
			ok, s = download_OSM_tile(s_nw, s_se, self.download_OSM_bg_dialog.scaleValue())
			if ok: # s is the image file name saved
				p_nw, p_se = self.download_OSM_bg_dialog.imageCornerSpecs()
				spec_suggestion = '%s  %s:%s  OpenStreetMap download' % (path.basename(s), p_nw, p_se)
				msg = 'Check the retrieved image file: %s\n\n' % s
				msg += 'If it is OK, you should:\n' \
						' 1. move it to the bg-img spec dir;\n' \
						' 2. add a spec line to your .lst file, typically as bottom layer (see below);\n' \
						' 3. reload background images to check the result.\n\n'
				msg += 'Line to include in .lst file (also sent to console):\n%s' % spec_suggestion
				print('\nLine to include in .lst file, reusing your point specs (image file saved in output dir):')
				print(spec_suggestion)
				QMessageBox.information(self, 'OpenStreetMap download', msg)
			else: # s is a descr. of the error
				QMessageBox.critical(self, 'OpenStreetMap download', 'OSM query has failed with error:\n%s' % s)
	
	def openSystemSettings(self):
		SystemSettingsDialog(self).exec()
	
	def changeLocation(self):
		if yesNo_question(self, 'Change location', 'This will close the current session.', 'Are you sure?'):
			self.launcher.show()
			self.close()
	
	
	## VIEW MENU ##
	
	def saveWindowState(self):
		with open(window_state_file, 'wb') as f:
			f.write(self.saveState())
		QMessageBox.information(self, 'Save window state', 'Current window state saved.')
	
	def recallWindowState(self):
		try:
			with open(window_state_file, 'rb') as f:
				self.restoreState(f.read())
		except FileNotFoundError:
			QMessageBox.critical(self, 'Recall window state', 'No saved window state to recall.')
	
	def newLooseStripBay(self):
		window = LooseStripPane(parent=None)
		window.installEventFilter(RadioKeyEventFilter(window))
		window.setWindowFlags(Qt.Window)
		window.setAttribute(Qt.WA_DeleteOnClose)
		window.show()
	
	def newScopeWindow(self):
		# ScopeFrame parent=None closes unexpectedly (e.g. on 2nd trigger); DEBUG: why not with loose bays?
		window = ScopeFrame(parent=self, secondary=True)
		window.installEventFilter(RadioKeyEventFilter(window))
		window.setWindowFlags(Qt.Window)
		window.setAttribute(Qt.WA_DeleteOnClose)
		window.show()
	
	def switchFlashDocks(self, switch):
		settings.flash_raised_panels = switch
	
	def toggleTowerWindow(self):
		if self.towerView_view_action.isChecked():
			if env.airport_data.viewpoints == []:
				QMessageBox.warning(self, 'Missing viewpoint', 'No viewpoint defined in airport file. A default location will be used.')
			settings.controlled_tower_viewer.start()
		else:
			settings.controlled_tower_viewer.stop()
	
	def selectIndicateViewpoint(self, vp_index):
		if vp_index != settings.selected_viewpoint:
			settings.selected_viewpoint = vp_index
			settings.tower_height_cheat_offset = 0
			if settings.controlled_tower_viewer.running:
				self.towerView_pane.updateTowerPosition()
		signals.indicatePoint.emit(env.viewpoint()[0])
	
	def activateAdditionalViews(self, toggle):
		settings.additional_views_active = toggle
	
	def listAdditionalViews(self):
		if settings.additional_views == []:
			txt = 'No additional viewers registered.'
		else:
			lst = [' - %s on port %d' % host_port for host_port in settings.additional_views]
			txt = 'Additional viewers:\n%s' % '\n'.join(lst)
		QMessageBox.information(self, 'Additional viewers', txt)
	
	def addView(self):
		text, ok = QInputDialog.getText(self, 'Add an external viewer', 'Enter "host:port" to send traffic to:')
		if ok:
			split = text.rsplit(':', maxsplit=1)
			if len(split) == 2 and split[1].isdecimal():
				viewer_address = split[0], int(split[1])
				settings.additional_views.append(viewer_address)
				QMessageBox.information(self, 'Add an external viewer', 'Viewer added: %s on port %d.' % viewer_address)
			else:
				QMessageBox.critical(self, 'Add an external viewer', 'Bad "host:port" format.')
	
	def removeView(self):
		if settings.additional_views == []:
			QMessageBox.critical(self, 'Remove viewer', 'No additional viewers registered.')
		else:
			items = ['%d: %s on port %d' % (i, settings.additional_views[i][0], settings.additional_views[i][1]) \
								for i in range(len(settings.additional_views))]
			item, ok = QInputDialog.getItem(self, 'Remove viewer', 'Select viewer to remove:', items, editable=False)
			if ok:
				del settings.additional_views[int(item.split(':', maxsplit=1)[0])]
	
	
	## OPTIONS MENU ##
	
	def configureRunwayUse(self):
		RunwayUseDialog(self).exec()
	
	def switchNotificationSounds(self, toggle):
		settings.notification_sounds_enabled = toggle
	
	def activatePrimaryRadar(self, toggle):
		settings.primary_radar_active = toggle
		env.radar.scan()
	
	def switchConflictWarnings(self, toggle):
		settings.route_conflict_warnings = toggle
	
	def switchTrafficIdentification(self, toggle):
		settings.traffic_identification_assistant = toggle
	
	def switchRwyOccupationIndications(self, toggle):
		settings.monitor_runway_occupation = toggle
	
	def switchApproachSpacingHints(self, toggle):
		settings.APP_spacing_hints = toggle
		signals.stripInfoChanged.emit()
	
	def openLocalSettings(self):
		LocalSettingsDialog(self).exec()
	
	def openSoloGameSettings(self):
		SoloGameSettingsDialog().exec()
		self.updatePTT(0, False) # display "MOUSE/VOICE" as appropriate
	
	def openGeneralSettings(self):
		dialog = GeneralSettingsDialog(self)
		dialog.exec()
		if dialog.result() > 0:
			if settings.game_manager.isRunning():
				env.radar.startSweeping()
	
	
	## CHEAT MENU ##
	
	def pauseGame(self, toggle):
		if toggle:
			settings.game_manager.pauseGame()
		else:
			settings.game_manager.resumeGame()
	
	def spawnAircraft(self):
		n, ok = QInputDialog.getInt(self, 'Spawn new aircraft', 'Try to spawn:', value=1, min=1, max=99)
		if ok:
			for i in range(n): # WARNING: game should be running
				settings.game_manager.spawnNewAircraft()
	
	def killSelectedAircraft(self):
		selected = selection.acft
		if selected == None:
			QMessageBox.critical(self, 'Cheat error', 'No aircraft selected.')
		else:
			selection.deselect()
			settings.game_manager.killAircraft(selected) # WARNING: killAircraft method must exist
			env.radar.scan()
	
	def setRejectedInstrPopUp(self, toggle):
		settings.solo_erroneous_instruction_warning = toggle
	
	def setShowRecognisedVoiceStrings(self, toggle):
		settings.show_recognised_voice_strings = toggle
	
	def changeTowerHeight(self):
		hoff, ok = QInputDialog.getInt(self, 'Cheat tower height', \
			'Offset in feet:', value=settings.tower_height_cheat_offset, min=0, max=1500, step=10)
		if ok:
			settings.tower_height_cheat_offset = hoff
			self.towerView_pane.updateTowerPosition()
	
	def setRadarCheatMode(self, toggle):
		settings.radar_cheat = toggle
		env.radar.scan()
	
	
	# -----------------     Internal GUI events      ------------------ #
	
	def closeEvent(self, event):
		if settings.game_manager.isRunning():
			settings.game_manager.stop()
		if settings.controlled_tower_viewer.running:
			settings.controlled_tower_viewer.stop(wait=True)
		print('Closing main window.')
		signals.mainWindowClosing.emit()
		signals.disconnect()
		settings.saveGeneralAndSystemSettings()
		settings.saveLocalSettings(env.airport_data)
		settings.savePresetChatMessages()
		env.resetEnv()
		settings.resetSession()
		QMainWindow.closeEvent(self, event)

