
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re
from datetime import timedelta

from PyQt5.QtCore import Qt, pyqtSignal, QSize, QRectF, \
			QItemSelectionModel, QSortFilterProxyModel, QAbstractTableModel, QModelIndex
from PyQt5.QtWidgets import QTableView, QTabWidget, QAbstractItemView, QStyle, \
			QStyledItemDelegate, QTabBar, QHeaderView, QGraphicsView
from PyQt5.QtGui import QIcon, QPixmap, QPen, QBrush, QFont, QColor

from game.config import settings
from game.env import env
from data.utc import duration_str
from data.strip import Strip, strip_mime_type, received_from_detail, recycled_detail, \
			runway_box_detail, rack_detail, soft_link_detail

from gui.misc import signals, selection, IconFile
from gui.stripRackActions import rwys_tab_name, new_strip_dialog, edit_rack_dialog
from gui.graphics.flightStrips import strip_mouse_press, paint_strip_box
from gui.graphics.miscGraphics import new_pen


# ---------- Constants ----------

strip_placeholder_margin = 5 # for delegate text document and deco
max_strip_icon_width = 20
max_WTC_time_disp = timedelta(minutes=5)

# -------------------------------



class StripItemDelegate(QStyledItemDelegate):
	def __init__(self, parent):
		QStyledItemDelegate.__init__(self, parent)
		self.show_icons = True
	
	def setShowIcons(self, b):
		self.show_icons = b
	
	def sizeHint(self, option, index):
		return QSize(600, 46)
	
	def paint(self, painter, option, index):
		strip = index.model().stripAt(index)
		# STYLE: rely ONLY on models' data to avoid redefining "stripAt"
		# for every model, e.g. here: strip = env.strips.stripAt(index.data())
		icon = None
		if self.show_icons and strip != None:
			if strip.lookup(recycled_detail):
				icon = QIcon(IconFile.recycle_pixmap)
			elif strip.lookup(received_from_detail) != None:
				icon = QIcon(IconFile.handover_pixmap)
		smw = option.rect.width() # width of strip with margins
		m2 = 2 * strip_placeholder_margin
		h = option.rect.height()
		painter.save()
		painter.translate(option.rect.topLeft())
		if icon == None:
			iw = 0
		else:
			iw = min(max_strip_icon_width, h - m2) # icon width
			smw -= iw + strip_placeholder_margin
			icon.paint(painter, 0, (h - iw) / 2, iw, iw)
		if strip != None:
			rect = QRectF(strip_placeholder_margin + iw, strip_placeholder_margin, smw - m2, h - m2)
			paint_strip_box(self, painter, strip, rect)
		painter.restore()








class StripTableView(QTableView):
	'''
	CAUTION: this is derived for *ANY* table view with draggable strips,
	incl. full racks table view, filtered tabbed rack, and even RWY boxes
	'''
	def __init__(self, parent):
		QTableView.__init__(self, parent)
		self.horizontalHeader().setSectionsMovable(True)
		self.verticalHeader().setDefaultSectionSize(46)
		self.horizontalHeader().setDefaultSectionSize(220)
		self.setItemDelegate(StripItemDelegate(self))
		self.setSelectionMode(QAbstractItemView.SingleSelection)
		self.setDragEnabled(True)
		self.setAcceptDrops(True)
		self.setShowGrid(False)
		self.setModel(env.strips)
		self.horizontalHeader().sectionDoubleClicked.connect(self.columnDoubleClicked)
	
	def setDivideHorizWidth(self, toggle):
		for section in range(self.horizontalHeader().length()):
			self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch if toggle else QHeaderView.Interactive)
		self.horizontalHeader().setStretchLastSection(toggle)
	
	def updateSelection(self):
		self.clearSelection()
		if selection.strip != None:
			mi = self.model().stripModelIndex(selection.strip)
			if mi != None:
				self.selectionModel().select(mi, QItemSelectionModel.ClearAndSelect)
				if settings.auto_switch_racks:
					self.scrollTo(mi)
	
	def mousePressEvent(self, event):
		QTableView.mousePressEvent(self, event)
		index = self.indexAt(event.pos())
		strip = self.model().stripAt(index) if index.isValid() else None
		if strip == None:
			selection.deselect()
		else:
			strip_mouse_press(strip, event)
			signals.selectionChanged.emit() # resync selection if event did not change it
	
	def dropEvent(self, event):
		QTableView.dropEvent(self, event)
		if not event.isAccepted(): # happens when outside of table area
			column = self.horizontalHeader().logicalIndexAt(event.pos())
			row = self.verticalHeader().logicalIndexAt(event.pos())
			if self.model().dropMimeData(event.mimeData(), event.dropAction(), row, column, QModelIndex()):
				event.acceptProposedAction()
		if event.isAccepted():
			signals.selectionChanged.emit()
	
	def mouseDoubleClickEvent(self, event):
		strip = selection.strip
		if event.button() == Qt.LeftButton and strip != None: # double-clicked on a strip
			event.accept()
			if event.modifiers() & Qt.ShiftModifier: # indicate radar link or identification
				acft = strip.linkedAircraft()
				if acft == None:
					acft = strip.lookup(soft_link_detail)
				if acft != None:
					signals.indicatePoint.emit(acft.coords())
			else: # request strip edit
				signals.stripEditRequest.emit(strip)
		else: # double-clicked off strip
			self.doubleClickOffStrip(event)
		if not event.isAccepted():
			QTableView.mouseDoubleClickEvent(self, event)
	
	def columnDoubleClicked(self, column):
		if column != -1:
			edit_rack_dialog(self, self.model().rackName(column))
	
	def doubleClickOffStrip(self, event):
		column = self.horizontalHeader().logicalIndexAt(event.pos())
		if column != -1 and not event.modifiers() & Qt.ShiftModifier:
			rack = self.model().rackName(column)
			new_strip_dialog(self, rack)
			event.accept()





class SingleRackColumnView(StripTableView):
	def __init__(self, parent, rack_name):
		StripTableView.__init__(self, parent)
		self.setModel(StripRackFilterModel(self, rack_name))
		self.horizontalHeader().setStretchLastSection(True)
	
	def rackFilter(self):
		return self.model().rack_filter

	def setRackFilter(self, rack):
		self.model().rack_filter = rack
		self.model().setFilterFixedString('') # trigger refilter





class StripRackFilterModel(QSortFilterProxyModel):
	def __init__(self, parent, rack):
		QSortFilterProxyModel.__init__(self, parent)
		self.setSourceModel(env.strips)
		self.rack_filter = rack
	
	def rackName(self, column):
		assert column == 0
		return self.rack_filter
	
	def stripAt(self, model_index):
		return self.sourceModel().stripAt(self.mapToSource(model_index))
	
	def stripModelIndex(self, strip):
		smi = self.sourceModel().stripModelIndex(strip)
		return None if smi == None else self.mapFromSource(smi)

	## MODEL STUFF
	def filterAcceptsColumn(self, sourceCol, sourceParent):
		return self.sourceModel().rackName(sourceCol) == self.rack_filter
	
	def filterAcceptsRow(self, sourceRow, sourceParent):
		return sourceRow < self.sourceModel().rackLength(self.sourceModel().rackIndex(self.rack_filter))
	
	def dropMimeData(self, mime, drop_action, row, column, parent):
		if not parent.isValid() and column == 0: # capture dropping under last strip
			rack_index = self.sourceModel().rackIndex(self.rack_filter)
			return self.sourceModel().dropMimeData(mime, drop_action, row, rack_index, parent)
		return QSortFilterProxyModel.dropMimeData(self, mime, drop_action, row, column, parent)







	
	


###############################

##        TABBED VIEW        ##

###############################



class StripRackTabs(QTabWidget):
	def __init__(self, parent=None):
		QTabWidget.__init__(self, parent)
		self.tab_bar = StripRackTabBar(self)
		self.setTabBar(self.tab_bar)
		self.rwy_box_tab = RunwayBoxesView(self)
		self.rwy_box_tab.setVerticalLayout(True)
		for rack in env.strips.rackNames():
			self.addRackView(rack)
		self.insertTab(settings.saved_rwy_tab_pos, self.rwy_box_tab, rwys_tab_name)
		self.updateTabIcons()
		signals.selectionChanged.connect(self.updateSelection)
		signals.generalSettingsChanged.connect(self.updateTabIcons) # also from rack colour edit
		env.strips.rackRenamed.connect(self.rackRenamed)
		env.strips.rackDeleted.connect(self.rackDeleted)
		env.strips.columnsInserted.connect(self.racksCreated)
	
	def showRunwayBoxTab(self, toggle):
		itab = self.rwyBoxTabIndex()
		if itab == None and toggle:
			self.insertTab(0, self.rwy_box_tab, rwys_tab_name)
			self.updateSelection() # give a chance to show selected strip if it has just appeared
		elif itab != None and not toggle:
			self.removeTab(itab)
	
	def rwyBoxTabIndex(self):
		return next((i for i in range(self.count()) if self.widget(i) is self.rwy_box_tab), None)
	
	def rackTabs(self):
		'''
		Returns the tabbed rack views' indices and widgets, excluding the RWY box view tab.
		'''
		return [(i, self.widget(i)) for i in range(self.count()) if self.widget(i) is not self.rwy_box_tab]
	
	def rackRenamed(self, old_name, new_name):
		itab, w = next((i, w) for i, w in self.rackTabs() if w.rackFilter() == old_name)
		w.setRackFilter(new_name)
		self.setTabText(itab, new_name)
		self.updateTabIcons()
	
	def rackDeleted(self, old_name):
		itab, wtab = next((i, w) for i, w in self.rackTabs() if w.rackFilter() == old_name)
		self.removeTab(itab)
		wtab.deleteLater()
	
	def racksCreated(self, parent, first_index, last_index):
		if not parent.isValid():
			for i in range(first_index, last_index + 1):
				self.addRackView(env.strips.rackName(i))
	
	def addRackView(self, rack_name):
		tblview = SingleRackColumnView(self, rack_name)
		tblview.horizontalHeader().setVisible(False)
		self.addTab(tblview, rack_name)
	
	def updateTabIcons(self):
		for i, w in self.rackTabs():
			if settings.show_rack_colours and w.rackFilter() in settings.rack_colours:
				pixmap = QPixmap(12, 12)
				pixmap.fill(settings.rackColour(w.rackFilter()))
				self.setTabIcon(i, QIcon(pixmap))
			else:
				self.setTabIcon(i, QIcon())
	
	def updateSelection(self):
		for i in range(self.count()):
			self.widget(i).updateSelection()
		strip = selection.strip
		if settings.auto_switch_racks and strip != None:
			if strip.lookup(runway_box_detail) == None: # strip is racked or loose
				for i, w in self.rackTabs():
					if w.rackFilter() == strip.lookup(rack_detail):
						self.setCurrentWidget(w)
						break
			else: # strip is boxed
				self.setCurrentWidget(self.rwy_box_tab)




class StripRackTabBar(QTabBar):
	def __init__(self, parent=None):
		QTabBar.__init__(self, parent)
		self.setAcceptDrops(True)
		# self.setChangeCurrentOnDrag(True) # FUTURE insert? when Qt>=5.4
	
	def dragEnterEvent(self, event): # FUTURE Useless if self.changeCurrentOnDrag()
		if event.mimeData().hasFormat(strip_mime_type):
			event.acceptProposedAction()
	
	def dragMoveEvent(self, event): # FUTURE Useless if self.changeCurrentOnDrag()
		itab = self.tabAt(event.pos())
		if itab != -1:
			self.setCurrentIndex(itab)
	
	def dropEvent(self, event):
		if event.mimeData().hasFormat(strip_mime_type):
			itab = self.currentIndex()
			if itab != self.parentWidget().rwyBoxTabIndex():
				strip = env.strips.fromMimeDez(event.mimeData())
				rack = self.parentWidget().widget(itab).rackFilter()
				env.strips.repositionStrip(strip, rack)
				event.acceptProposedAction()
				signals.selectionChanged.emit()
	
	def mouseDoubleClickEvent(self, event):
		itab = self.tabAt(event.pos())
		if itab != -1 and itab != self.parentWidget().rwyBoxTabIndex():
			rack = self.parentWidget().widget(itab).rackFilter()
			edit_rack_dialog(self, rack)
			event.accept()
		else:
			QTabBar.mouseDoubleClickEvent(self, event)














##############################

##       RUNWAY BOXES       ##

##############################



class RunwayBoxItemDelegate(QStyledItemDelegate):
	def __init__(self, parent):
		QStyledItemDelegate.__init__(self, parent)
	
	def sizeHint(self, option, index):
		return QSize(600, 46)
	
	def paint(self, painter, option, index):
		strip = index.model().stripAt(index)
		# STYLE: rely ONLY on models' data to avoid needing stripAt redef for every model
		# e.g. here: strip = env.strips.stripAt(index.data())
		physical_RWY_index = index.model().boxAt(index)
		rwy_txt = env.airport_data.physicalRunwayNameFromUse(physical_RWY_index)
		m2 = 2 * strip_placeholder_margin
		painter.save()
		painter.translate(option.rect.topLeft())
		box = QRectF(strip_placeholder_margin, strip_placeholder_margin, option.rect.width() - m2, option.rect.height() - m2)
		vertical_sep = box.height() * .6
		if strip == None:
			timer, wtc = env.airport_data.physicalRunwayWtcTimer(physical_RWY_index)
			if timer > max_WTC_time_disp:
				timer_txt = ''
			else:
				timer_txt = duration_str(timer)
				if wtc != None:
					timer_txt += ' / %s' % wtc
			painter.setPen(QPen(Qt.NoPen))
			painter.setBrush(QBrush(QColor('#EEEEEE')))
			painter.drawRect(box)
			painter.setPen(new_pen(Qt.black))
			painter.drawText(box.adjusted(0, vertical_sep, 0, 0), Qt.AlignCenter, timer_txt) # Normal font
			font = QFont(painter.font())
			font.setPointSize(font.pointSize() + 3)
			painter.setFont(font)
			painter.drawText(box.adjusted(0, 0, 0, -vertical_sep), Qt.AlignCenter, rwy_txt)
		else:
			paint_strip_box(self, painter, strip, box)
			txt_box = box.adjusted(box.width() - 50, vertical_sep, 0, 0)
			painter.setPen(QPen(Qt.NoPen))
			painter.setBrush(QBrush(QColor('#EEEEEE')))
			painter.drawRect(txt_box)
			painter.setPen(new_pen(Qt.black))
			painter.drawText(txt_box, Qt.AlignCenter, rwy_txt)
		painter.restore()





class RunwayBoxTableModel(QAbstractTableModel):
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.box_count = 0 if env.airport_data == None else env.airport_data.physicalRunwayCount()
		self.vertical = False

	def rowCount(self, parent=QModelIndex()):
		if parent.isValid():
			return 0
		return self.box_count if self.vertical else 1

	def columnCount(self, parent=QModelIndex()):
		if parent.isValid():
			return 0
		return 1 if self.vertical else self.box_count
	
	def data(self, index, role):
		strip = self.stripAt(index)
		if role == Qt.DisplayRole:
			return str(strip) if strip != None else 'Physical RWY %s' % self.boxAt(index)
	
	def flags(self, index):
		flags = Qt.ItemIsEnabled
		if index.isValid():
			if self.stripAt(index) == None:
				flags |= Qt.ItemIsDropEnabled
			else:
				flags |= Qt.ItemIsDragEnabled | Qt.ItemIsSelectable
		return flags

	## DRAG AND DROP STUFF
	def supportedDragActions(self):
		return Qt.MoveAction
	
	def supportedDropActions(self):
		return Qt.MoveAction
	
	def mimeTypes(self):
		return [strip_mime_type]
	
	def mimeData(self, indices):
		assert len(indices) == 1
		return env.strips.mkMimeDez(self.stripAt(indices[0]))
	
	def dropMimeData(self, mime, drop_action, row, column, parent):
		if parent.isValid() and drop_action == Qt.MoveAction and mime.hasFormat(strip_mime_type):
			drop_rwy = self.boxAt(parent)
			dropped_strip = env.strips.fromMimeDez(mime)
			was_in_box = dropped_strip.lookup(runway_box_detail)
			if was_in_box != None:
				mi1 = self.boxModelIndex(was_in_box)
				self.dataChanged.emit(mi1, mi1)
			env.strips.repositionStrip(dropped_strip, None, box=drop_rwy)
			mi2 = self.boxModelIndex(drop_rwy)
			self.dataChanged.emit(mi2, mi2)
			return True
		return False

	## ACCESSORS
	def boxAt(self, index):
		return index.row() if self.vertical else index.column()
	
	def boxModelIndex(self, section):
		return self.index(section, 0) if self.vertical else self.index(0, section)
	
	def stripAt(self, index):
		try:
			return env.strips.findStrip(lambda strip: strip.lookup(runway_box_detail) == self.boxAt(index))
		except StopIteration:
			return None
	
	def stripModelIndex(self, strip):
		rwy = strip.lookup(runway_box_detail)
		return None if rwy == None else self.boxModelIndex(rwy)

	## MODIFIERS
	def setVertical(self, toggle):
		self.beginResetModel()
		self.vertical = toggle
		self.endResetModel()

	def updateViewsAfterBoxFreed(self, box):
		mi = self.boxModelIndex(box)
		self.dataChanged.emit(mi, mi)
	
	def updateVisibleWtcTimers(self):
		for i in range(self.box_count):
			mi = self.boxModelIndex(i)
			if self.stripAt(mi) == None:
				self.dataChanged.emit(mi, mi)




class RunwayBoxFilterModel(QSortFilterProxyModel):
	def __init__(self, parent, source_model):
		QSortFilterProxyModel.__init__(self, parent)
		self.setSourceModel(source_model)
	
	def boxAt(self, model_index):
		return self.sourceModel().boxAt(self.mapToSource(model_index))
	
	def stripAt(self, model_index):
		return self.sourceModel().stripAt(self.mapToSource(model_index))
	
	def stripModelIndex(self, strip):
		smi = self.sourceModel().stripModelIndex(strip)
		return None if smi == None else self.mapFromSource(smi)

	## FILTERING	
	def acceptPhysicalRunway(self, phyrwy):
		rwy1, rwy2 = env.airport_data.physicalRunway(phyrwy)
		return rwy1.inUse() or rwy2.inUse()

	## MODEL STUFF
	def filterAcceptsColumn(self, sourceCol, sourceParent):
		return self.sourceModel().vertical or self.acceptPhysicalRunway(sourceCol) \
			or self.sourceModel().stripAt(self.sourceModel().boxModelIndex(sourceCol)) != None
	
	def filterAcceptsRow(self, sourceRow, sourceParent):
		return not self.sourceModel().vertical or self.acceptPhysicalRunway(sourceRow) \
			or self.sourceModel().stripAt(self.sourceModel().boxModelIndex(sourceRow)) != None



class RunwayBoxesView(StripTableView):
	def __init__(self, parent=None):
		StripTableView.__init__(self, parent)
		self.full_model = RunwayBoxTableModel(self)
		self.filter_model = RunwayBoxFilterModel(self, self.full_model)
		self.setShowGrid(True)
		self.horizontalHeader().setVisible(False)
		self.verticalHeader().setVisible(False)
		self.setItemDelegate(RunwayBoxItemDelegate(self))
		for section in range(self.horizontalHeader().length()):
			self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
		self.setVerticalLayout(False)
		if env.airport_data != None:
			self.setModel(self.filter_model)
		env.strips.rwyBoxFreed.connect(self.full_model.updateViewsAfterBoxFreed)
		env.strips.rwyBoxFreed.connect(lambda: self.filter_model.setFilterFixedString('')) # trigger refilter
		signals.runwayUseChanged.connect(lambda: self.filter_model.setFilterFixedString('')) # trigger refilter
		signals.clockTick.connect(self.full_model.updateVisibleWtcTimers)
	
	def setVerticalLayout(self, toggle):
		self.full_model.setVertical(toggle)
		self.verticalHeader().setStretchLastSection(not toggle)
	
	def doubleClickOffStrip(self, event):
		pass


