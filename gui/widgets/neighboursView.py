
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QTableView, QInputDialog
from PyQt5.QtCore import Qt

from game.config import settings
from game.env import env
from models.gameStrips import default_rack_name
from gui.misc import signals


# ---------- Constants ----------

# -------------------------------



class AtcNeighboursTableView(QTableView):
	def __init__(self, parent=None):
		QTableView.__init__(self, parent)
	
	def mouseDoubleClickEvent(self, event):
		index = self.indexAt(event.pos())
		if index.isValid() and event.button() == Qt.LeftButton:
			atc = env.ATCs.callsignOnRow(index.row())
			if event.modifiers() & Qt.ShiftModifier:
				try:
					pos = env.ATCs.ATCpos(atc)
					if pos == None:
						signals.statusBarMsg.emit('Position unknown for %s' % atc)
					else:
						signals.indicatePoint.emit(pos)
				except KeyError:
					pass
			else: # double-click, no SHIFT
				items = env.strips.rackNames()
				try:
					idx = items.index(settings.collecting_racks.get(atc, default_rack_name))
				except ValueError:
					idx = 0 # safeguard only
				rack, ok = QInputDialog.getItem(self, 'Set collecting rack', \
						'Rack strips from %s on:' % atc, items, current=idx, editable=False)
				if ok and rack != default_rack_name:
					settings.collecting_racks[atc] = rack
			event.accept()
		else:
			QTableView.mouseDoubleClickEvent(self, event)


