
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QWidget, QInputDialog, QMessageBox
from PyQt5.QtCore import QObject, QEvent, pyqtSignal, Qt
from PyQt5.QtGui import QIcon

from game.config import settings, PTT_keys
from gui.misc import signals, IconFile

from game.env import env
from data.params import hPa2inHg
from data.nav import Navpoint, Airfield, world_nav_data, NavpointError

from ui.weatherDispWidget import Ui_weatherDispWidget
from ui.quickReference import Ui_quickReference
from ui.airportPicker import Ui_airportPicker
from ui.xpdrCodeSelector import Ui_xpdrCodeSelectorWidget


# ---------- Constants ----------

airportPicker_shortcutToHere = '.'
airportPicker_searchMinimumLength = 3

recognisedValue_lineEdit_styleSheet = 'QLineEdit { color: black; background-color: rgb(200, 255, 200) }' # pale green
unrecognisedValue_lineEdit_styleSheet = 'QLineEdit { color: black; background-color: rgb(255, 200, 200) }' # pale red

quick_ref_disp = 'resources/quick-ref/display-conventions.html'
quick_ref_kbd = 'resources/quick-ref/kbd-shortcuts.html'
quick_ref_mouse = 'resources/quick-ref/mouse-gestures.html'
quick_ref_aliases = 'resources/quick-ref/text-aliases.html'
quick_ref_voice = 'resources/quick-ref/voice-instructions.html'

# -------------------------------



class RadioKeyEventFilter(QObject):
	def eventFilter(self, receiver, event):
		t = event.type()
		if t == QEvent.KeyPress or t == QEvent.KeyRelease:
			#print('EVENT key=%s, nvk=%s, nsc=%s' % (event.key(), event.nativeVirtualKey(), event.nativeScanCode())) # DEBUG
			try:
				key_number = next(i for i, key in enumerate(PTT_keys) if key == event.key())
				signals.kbdPTT.emit(key_number, t == QEvent.KeyPress)
				return True
			except StopIteration:
				return False
		else:
			return QObject.eventFilter(self, receiver, event)





##-------------------------------------##
##                                     ##
##           AIRPORT CHOOSER           ##
##                                     ##
##-------------------------------------##



class AirportPicker(QWidget, Ui_airportPicker):
	# SIGNALS
	unrecognised = pyqtSignal(str) # Not emitted if an ICAO code is recognised
	recognised = pyqtSignal(Airfield) # Emitted when either set or recognised
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.search_button.setIcon(QIcon(IconFile.button_search))
		self.setFocusProxy(self.airport_edit)
		self.search_button.setEnabled(False)
		self.search_button.clicked.connect(self.searchAirport)
		self.airport_edit.textEdited.connect(self.tryRecognising)
		self.recognised.connect(lambda ad: self.airport_edit.setToolTip(ad.long_name))
		self.unrecognised.connect(lambda: self.airport_edit.setToolTip(''))
	
	def currentText(self):
		return self.airport_edit.text()
	
	def setEditText(self, txt):
		self.airport_edit.setText(txt)
		self.tryRecognising(txt)
	
	def tryRecognising(self, txt):
		if txt == airportPicker_shortcutToHere and env.airport_data != None:
			txt = settings.location_code
		self.search_button.setEnabled(len(txt) >= airportPicker_searchMinimumLength)
		try:
			self.recognise(world_nav_data.findAirfield(txt))
		except NavpointError:
			self.airport_edit.setStyleSheet('' if txt == '' else unrecognisedValue_lineEdit_styleSheet)
			self.unrecognised.emit(txt)

	def searchAirport(self):
		txt = self.currentText()
		lst = [ad for ad in world_nav_data.byType(Navpoint.AD) if txt.lower() in ad.long_name.lower()]
		if lst == []:
			QMessageBox.critical(self, 'No match found', 'No airport found whose name contains "%s".' % txt)
		elif len(lst) == 1:
			self.recognise(lst[0])
		else:
			items = ['%s  %s' % (ad.code, ad.long_name) for ad in lst]
			text, ok = QInputDialog.getItem(self, 'Multiple matches', 'Select airport:', sorted(items), editable=False)
			if ok:
				self.tryRecognising(text.split(maxsplit=1)[0])
		self.setFocus()
	
	def recognise(self, ad):
		self.airport_edit.setText(ad.code)
		self.airport_edit.setStyleSheet(recognisedValue_lineEdit_styleSheet)
		self.recognised.emit(ad)







##----------------------------------------##
##                                        ##
##         RELATED TO TRANSPONDERS        ##
##                                        ##
##----------------------------------------##


class XpdrCodeSelectorWidget(QWidget, Ui_xpdrCodeSelectorWidget):
	codeChanged = pyqtSignal(int)
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.setFocusProxy(self.xpdrCode_edit)
		self.updateXPDRranges()
		self.xpdrRange_select.currentIndexChanged.connect(self.selectXpdrRange)
		self.xpdrCode_edit.valueChanged.connect(self.codeChanged.emit)
	
	def updateXPDRranges(self):
		self.xpdrRange_select.setCurrentIndex(0)
		while self.xpdrRange_select.count() > 1:
			self.xpdrRange_select.removeItem(1)
		self.xpdrRange_select.addItems([r.name for r in settings.XPDR_assignment_ranges if r != None])
	
	def selectXpdrRange(self, row):
		if row != 0:
			name = self.xpdrRange_select.itemText(row)
			assignment_range = next(r for r in settings.XPDR_assignment_ranges if r != None and r.name == name)
			self.xpdrCode_edit.setValue(env.nextSquawkCodeAssignment(assignment_range))
			self.xpdrRange_select.setCurrentIndex(0)
			self.xpdrCode_edit.setFocus()
	
	def getSQ(self):
		return self.xpdrCode_edit.value()
	
	def setSQ(self, value):
		return self.xpdrCode_edit.setValue(value)




##------------------------------------##
##                                    ##
##         RELATED TO WEATHER         ##
##                                    ##
##------------------------------------##



class WeatherDispWidget(QWidget, Ui_weatherDispWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
	
	def updateDisp(self, new_weather):
		if new_weather == None:
			self.METAR_info.setText('N/A')
			self.wind_info.setText('N/A')
			self.visibility_info.setText('N/A')
			self.QNH_info.setText('N/A')
		else:
			self.METAR_info.setText(new_weather.METAR())
			self.wind_info.setText(new_weather.readWind())
			self.visibility_info.setText(new_weather.readVisibility())
			qnh = new_weather.QNH()
			if qnh == None:
				self.QNH_info.setText('N/A')
			else:
				self.QNH_info.setText('%d hPa, %.2f inHg' % (qnh, hPa2inHg * qnh))
		





##------------------------------------##
##                                    ##
##           WINDOW WIDGETS           ##
##                                    ##
##------------------------------------##


class QuickReferenceWindow(QWidget, Ui_quickReference):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.setWindowFlags(Qt.Window)
		with open(quick_ref_disp) as f:
			self.disp_pane.setHtml(f.read())
		with open(quick_ref_kbd) as f:
			self.kbd_pane.setHtml(f.read())
		with open(quick_ref_mouse) as f:
			self.mouse_pane.setHtml(f.read())
		with open(quick_ref_aliases) as f:
			self.aliases_pane.setHtml(f.read())
		with open(quick_ref_voice) as f:
			self.voice_pane.setHtml(f.read())
		signals.mainWindowClosing.connect(self.close)


