
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re

from PyQt5.QtCore import Qt, QDateTime, QTime, QAbstractTableModel
from PyQt5.QtWidgets import QDialog, QMessageBox, QColorDialog, QLabel
from PyQt5.QtGui import QIcon, QPixmap

from ui.postLennySessionDialog import Ui_postLennySessionDialog
from ui.envInfoDialog import Ui_envInfoDialog
from ui.aboutDialog import Ui_aboutDialog
from ui.routeSpecsLostDialog import Ui_routeSpecsLostDialog
from ui.discardedStripsDialog import Ui_discardedStripsDialog
from ui.editRackDialog import Ui_editRackDialog

from game.config import settings, version_string
from game.env import env

from ext.xplane import surface_types
from ext.lenny64 import post_session, Lenny64Error

from data.util import some
from data.utc import now
from data.coords import m2NM
from data.strip import Strip, recycled_detail, rack_detail, runway_box_detail

from models.gameStrips import default_rack_name
from gui.misc import signals
from gui.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

version_string_placeholder = '##version##'

# -------------------------------


def yesNo_question(parent_window, title, text, question):
	return QMessageBox.question(parent_window, title, text + '\n' + question) == QMessageBox.Yes




class PostLennySessionDialog(QDialog, Ui_postLennySessionDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		t = now()
		qdt = QDateTime(t.year, t.month, t.day, t.hour, t.minute, timeSpec=Qt.UTC)
		self.beginTime_edit.setDateTime(qdt)
		self.endTime_edit.setTime(QTime((t.hour + 2) % 24, 0))
		self.frequency_edit.addFrequencies([(frq, descr) for frq, descr, t in env.frequencies])
		self.frequency_edit.clearEditText()
		self.announce_button.clicked.connect(self.announceSession)
		self.cancel_button.clicked.connect(self.reject)

	def announceSession(self):
		beg = self.beginTime_edit.dateTime().toPyDateTime()
		end = self.endTime_edit.time().toPyTime()
		try:
			post_session(beg, end, frq=self.frequency_edit.getFrequency())
			QMessageBox.information(self, 'Session announced', 'Session successfully announced!')
			self.accept()
		except Lenny64Error as err:
			if err.srvResponse() == None:
				QMessageBox.critical(self, 'Network error', 'A network problem occured: %s' % err)
			else:
				QMessageBox.critical(self, 'Lenny64 error', 'Announcement failed.\nCheck session details and Lenny64 identification settings.')







class AboutDialog(QDialog, Ui_aboutDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.about_text.setHtml(re.sub(version_string_placeholder, version_string, self.about_text.toHtml()))







class RwyInfoTableModel(QAbstractTableModel):
	'''
	CAUTION: Do not build if no airport data
	'''
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.column_headers = ['RWY', 'Length', 'DTHR', 'Width', 'Surface', 'Magn. course']
		if env.elevation_map != None:
			self.column_headers.append('THR elev.')
		self.runways = env.airport_data.allRunways(sortByName=True)
	
	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return self.column_headers[section]

	def rowCount(self, parent=None):
		return len(self.runways)

	def columnCount(self, parent=None):
		return len(self.column_headers)

	def data(self, index, role):
		if role == Qt.DisplayRole:
			rwy = self.runways[index.row()]
			col = index.column()
			width, surface = env.airport_data.physicalRunwayData(rwy.physicalRwyIndex())
			if col == 0:
				return rwy.name
			elif col == 1:
				return '%d m' % (rwy.length(dthr=False) / m2NM)
			elif col == 2:
				return 'none' if rwy.dthr == 0 else '%d m' % rwy.dthr
			elif col == 3:
				return '%d m' % width
			elif col == 4:
				return surface_types.get(surface, 'Unknown')
			elif col == 5:
				return '%s°' % rwy.orientation().read()
			elif col == 6: # only if elev map exists
				return '%.1f ft' % env.elevation(rwy.threshold())


class EnvironmentInfoDialog(QDialog, Ui_envInfoDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.magneticDeclination_infoLabel.setText(some(env.readDeclination(), 'N/A'))
		self.radarPosition_infoLabel.setText(str(env.radarPos()))
		if env.airport_data == None:
			self.airport_tab.setEnabled(False)
		else:
			self.airportName_infoLabel.setText(env.locationName())
			self.airportElevation_infoLabel.setText('%.1f ft' % env.airport_data.navpoint.altitude)
			table_model = RwyInfoTableModel(self)
			self.runway_tableView.setModel(table_model)
			for i in range(table_model.columnCount()):
				self.runway_tableView.resizeColumnToContents(i)
	
	def showEvent(self, event):
		total = env.strips.count()
		linked = env.strips.count(lambda s: s.linkedAircraft() != None)
		racked = env.strips.count(lambda s: s.lookup(rack_detail) != None)
		boxed = env.strips.count(lambda s: s.lookup(runway_box_detail) != None)
		self.linkedStrips_infoLabel.setText(str(linked))
		self.unlinkedStrips_infoLabel.setText(str(total - linked))
		self.playingStrips_infoLabel.setText(str(total))
		self.boxedStrips_infoLabel.setText(str(boxed))
		self.looseStrips_infoLabel.setText(str(total - racked - boxed))
		self.rackedStrips_infoLabel.setText(str(racked))













class RouteSpecsLostDialog(QDialog, Ui_routeSpecsLostDialog):
	def __init__(self, parent, title, lost_specs_text):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(title)
		self.lostSpecs_box.setText(lost_specs_text)
	
	def mustOpenStripDetails(self):
		return self.openStripDetailSheet_tickBox.isChecked()







class DiscardedStripsDialog(QDialog, Ui_discardedStripsDialog):
	def __init__(self, parent, view_model, dialog_title):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(dialog_title)
		self.model = view_model
		self.strip_view.setModel(view_model)
		self.recall_button.clicked.connect(self.recallSelectedStrip)
		self.close_button.clicked.connect(self.accept)
		self.clear_button.clicked.connect(self.model.forgetStrips)
	
	def recallSelectedStrip(self):
		try:
			strip = self.model.stripAt(self.strip_view.selectedIndexes()[0])
			signals.stripRecall.emit(strip)
		except IndexError:
			pass





class EditRackDialog(QDialog, Ui_editRackDialog):
	def __init__(self, parent, rack_name):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.rackDeletion_info.clear()
		self.installEventFilter(RadioKeyEventFilter(self))
		self.initial_rack_name = rack_name
		self.rack_colour = settings.rack_colours.get(rack_name, None)
		self.flagged_for_deletion = False
		self.rackName_edit.setText(self.initial_rack_name)
		self.updateColourIcon()
		if rack_name == default_rack_name:
			self.rackName_edit.setEnabled(False)
			self.collectsFrom_edit.setPlainText('(default collecting rack)')
			self.collectsFrom_edit.setEnabled(False)
			self.deleteRack_button.setEnabled(False)
		else:
			self.collectsFrom_edit.setToolTip('Callsigns from which received strips should be collected on this rack (one per line)')
			self.collectsFrom_edit.setPlainText('\n'.join(atc for atc, rack in settings.collecting_racks.items() if rack == rack_name))
		self.pickColour_button.clicked.connect(self.chooseRackColour)
		self.clearColour_button.clicked.connect(self.clearRackColour)
		self.deleteRack_button.toggled.connect(self.flagRackForDeletion)
	
	def flaggedForDeletion(self):
		return self.deleteRack_button.isChecked()
	
	def getEntries(self):
		return self.rackName_edit.text(), self.rack_colour, self.collectsFrom_edit.toPlainText().split('\n')
	
	def updateColourIcon(self):
		if self.rack_colour == None:
			self.pickColour_button.setIcon(QIcon())
		else:
			pixmap = QPixmap(24, 24)
			pixmap.fill(self.rack_colour)
			self.pickColour_button.setIcon(QIcon(pixmap))
	
	def chooseRackColour(self):
		colour = QColorDialog.getColor(initial=some(self.rack_colour, settings.colour('uncoloured_rack')), \
			parent=self, title='Set colour for linked contacts')
		if colour.isValid():
			self.rack_colour = colour
			self.updateColourIcon()
			
	def clearRackColour(self):
		self.rack_colour = None
		self.updateColourIcon()

	def flagRackForDeletion(self, toggle):
		if toggle:
			if env.strips.count(lambda s: s.lookup(rack_detail) == self.initial_rack_name) > 0:
				QMessageBox.warning(self, 'Non-empty rack deletion', 'Rack not empty. Strips will be reracked before deletion.')
			self.rackDeletion_info.setText('Flagged for deletion')
		else:
			self.rackDeletion_info.clear()
