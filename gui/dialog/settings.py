
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta
from hashlib import md5

from PyQt5.QtCore import Qt, QStringListModel
from PyQt5.QtWidgets import QWidget, QDialog, QMessageBox, QInputDialog, QFileDialog

from ui.localSettingsDialog import Ui_localSettingsDialog
from ui.generalSettingsDialog import Ui_generalSettingsDialog
from ui.systemSettingsDialog import Ui_systemSettingsDialog
from ui.soloGameSettingsDialog import Ui_soloGameSettingsDialog
from ui.airspaceRules_AD import Ui_airspaceRulesWidget_AD
from ui.airspaceRules_CTR import Ui_airspaceRulesWidget_CTR

from data.util import all_diff
from game.config import settings, SemiCircRule, XpdrAssignmentRange

from game.env import env
from data.acft import snapshot_history_size
from data.params import StdPressureAlt
from data.nav import Navpoint
from ext.fgfs import fgTwrCommonOptions
from ext.tts import speech_synthesis_available
from ext.sr import speech_recognition_available, get_pyaudio_devices_info
from gui.misc import signals
from gui.widgets.basicWidgets import RunwayTabWidget
from gui.widgets.miscWidgets import RadioKeyEventFilter
from gui.dialog.runways import RunwayParametersWidget


# ---------- Constants ----------

# -------------------------------


def show_pyaudio_device_info(info_key):
	return 'Channels' in info_key or 'Rate' in info_key






# =================================
#
#           S Y S T E M
#
# =================================

class SystemSettingsDialog(QDialog, Ui_systemSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.towerView_page.setEnabled(env.airport_data != None and not settings.controlled_tower_viewer.running)
		self.fgcom_page.setEnabled(not settings.game_manager.isRunning())
		self.FG_network_groupBox.setEnabled(not settings.game_manager.isRunning())
		self.FG_lenny64_groupBox.setEnabled(not settings.game_manager.isRunning())
		self.solo_page.setEnabled(not settings.game_manager.isRunning())
		self.SR_groupBox.setEnabled(speech_recognition_available)
		if speech_recognition_available:
			self.audio_devices = get_pyaudio_devices_info()
			self.sphinxAcousticModel_edit.setClearButtonEnabled(True)
			self.audioDeviceIndex_edit.setMaximum(len(self.audio_devices) - 1)
			self.audioDeviceInfo_button.clicked.connect(self.showAudioDeviceInfo)
		self.lennyPassword_edit.setPlaceholderText('No password set' if settings.lenny64_password_md5 == '' else '(unchanged)')
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(SystemSettingsDialog.last_tab_used)
		self.produceExtViewerCmd_button.clicked.connect(self.showExternalViewerFgOptions)
		self.lennyPassword_edit.textChanged.connect(self._pwdTextChanged)
		self.browseForSphinxAcousticModel_button.clicked.connect(self.browseForSphinxAcousticModel)
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def _pwdTextChanged(self, s):
		self.passwordChange_info.setText('Password will be changed!' if s != '' and settings.lenny64_password_md5 != '' else '')
	
	def browseForSphinxAcousticModel(self):
		txt = QFileDialog.getExistingDirectory(self, caption='Choose Sphinx acoustic model directory')
		if txt != '':
			self.sphinxAcousticModel_edit.setText(txt)
	
	def showAudioDeviceInfo(self):
		txt = 'PyAudio detects %d devices.' % len(self.audio_devices)
		for index, info in enumerate(self.audio_devices):
			txt += '\n\n[%d] %s\n' % (index, info.get('name', '???'))
			txt += ', '.join('%s: %s' % item for item in info.items() if show_pyaudio_device_info(item[0]))
		QMessageBox.information(self, 'PyAudio devices information', txt)
	
	def showExternalViewerFgOptions(self):
		required_options = fgTwrCommonOptions()
		required_options.append('--multiplay=out,100,this_host,%d' % settings.FGFS_views_send_port)
		required_options.append('--multiplay=in,100,,%d' % self.towerView_fgmsPort_edit.value())
		required_options.append('--telnet=,,100,,%d,' % self.towerView_telnetPort_edit.value())
		print('Options required for external FlightGear viewer with current dialog options: ' + ' '.join(required_options))
		msg = 'Options required with present configuration (also sent to console):\n'
		msg += '\n'.join('  ' + opt for opt in required_options)
		msg += '\n\nNB: Replace "this_host" with appropriate value.'
		QMessageBox.information(self, 'Required FlightGear options', msg)

	def fillFromSettings(self):
		## Tower view
		(self.towerView_external_radioButton if settings.external_tower_viewer_process else self.towerView_internal_radioButton).setChecked(True)
		self.towerView_fgmsPort_edit.setValue(settings.tower_viewer_UDP_port)
		self.towerView_telnetPort_edit.setValue(settings.tower_viewer_telnet_port)
		self.fgCommand_edit.setText(settings.FGFS_executable)
		self.fgRootDir_edit.setText(settings.FGFS_root_dir)
		self.fgAircraftDir_edit.setText(settings.FGFS_aircraft_dir)
		self.fgSceneryDir_edit.setText(settings.FGFS_scenery_dir)
		self.externalTowerViewerHost_edit.setText(settings.external_tower_viewer_host)
		## FGCom
		self.fgcomExe_edit.setText(settings.fgcom_exe_path)
		self.fgcomServer_edit.setText(settings.fgcom_server)
		self.fgcomReservedPort_edit.setValue(settings.reserved_fgcom_port)
		self.fgcomRadioBoxPorts_edit.setText(','.join(str(n) for n in settings.radio_fgcom_ports))
		## FlightGear MP
		self.MPserverHost_edit.setText(settings.FGMS_server_name)
		self.MPserverPort_edit.setValue(settings.FGMS_server_port)
		self.fgmsLegacyProtocol_tickBox.setChecked(settings.FGMS_legacy_protocol)
		self.SXserver_edit.setText(settings.FGSX_server_name)
		self.SXenabled_tickBox.setChecked(settings.FGSX_enabled)
		self.lennyAccountEmail_edit.setText(settings.lenny64_account_email)
		self.lennyPassword_edit.setText('') # unchanged if stays blank
		self.FPLupdateInterval_edit.setValue(int(settings.FPL_update_interval.total_seconds()) / 60)
		self.METARupdateInterval_edit.setValue(int(settings.METAR_update_interval.total_seconds()) / 60)
		self.xpdrModeForNonEquippedAcft_select.setCurrentIndex('0ACS'.index(settings.FG_XPDR_mode_if_not_equipped))
		self.strictAcftTypeDesignators_tickBox.setChecked(settings.FG_strict_ATDs)
		## Solo and teacher session types
		self.soloAircraftTypes_edit.setPlainText('\n'.join(settings.solo_aircraft_types))
		self.restrictAirlineChoiceToLiveries_tickBox.setChecked(settings.solo_restrict_to_available_liveries)
		self.sphinxAcousticModel_edit.setText(settings.sphinx_acoustic_model_dir)
		self.audioDeviceIndex_edit.setValue(settings.audio_input_device_index)

	def storeSettings(self):
		try:
			fgcom_reserved_port = self.fgcomReservedPort_edit.value()
			fgcom_radio_ports = [int(p) for p in self.fgcomRadioBoxPorts_edit.text().split(',')]
			if fgcom_reserved_port in fgcom_radio_ports or not all_diff(fgcom_radio_ports):
				raise ValueError
		except ValueError:
			QMessageBox.critical(self, 'Invalid entry', 'Error or duplicates in FGCom port configuration.')
			return
		
		SystemSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		
		## Tower view
		settings.external_tower_viewer_process = self.towerView_external_radioButton.isChecked()
		settings.tower_viewer_UDP_port = self.towerView_fgmsPort_edit.value()
		settings.tower_viewer_telnet_port = self.towerView_telnetPort_edit.value()
		settings.FGFS_executable = self.fgCommand_edit.text()
		settings.FGFS_root_dir = self.fgRootDir_edit.text()
		settings.FGFS_aircraft_dir = self.fgAircraftDir_edit.text()
		settings.FGFS_scenery_dir = self.fgSceneryDir_edit.text()
		settings.external_tower_viewer_host = self.externalTowerViewerHost_edit.text()
		
		## FGCom
		settings.fgcom_exe_path = self.fgcomExe_edit.text()
		settings.fgcom_server = self.fgcomServer_edit.text()
		settings.reserved_fgcom_port = fgcom_reserved_port
		settings.radio_fgcom_ports = fgcom_radio_ports
		
		## FlightGear MP
		settings.FGMS_server_name = self.MPserverHost_edit.text()
		settings.FGMS_server_port = self.MPserverPort_edit.value()
		settings.FGMS_legacy_protocol = self.fgmsLegacyProtocol_tickBox.isChecked()
		settings.FGSX_server_name = self.SXserver_edit.text()
		settings.FGSX_enabled = self.SXenabled_tickBox.isChecked()
		settings.lenny64_account_email = self.lennyAccountEmail_edit.text()
		new_lenny64_pwd = self.lennyPassword_edit.text()
		if new_lenny64_pwd != '': # password change!
			digester = md5()
			digester.update(bytes(new_lenny64_pwd, 'utf-8'))
			settings.lenny64_password_md5 = ''.join('%02x' % x for x in digester.digest())
		settings.METAR_update_interval = timedelta(minutes=self.METARupdateInterval_edit.value())
		settings.FPL_update_interval = timedelta(minutes=self.FPLupdateInterval_edit.value())
		settings.FG_XPDR_mode_if_not_equipped = '0ACS'[self.xpdrModeForNonEquippedAcft_select.currentIndex()]
		settings.FG_strict_ATDs = self.strictAcftTypeDesignators_tickBox.isChecked()
		
		## Solo and teacher session types
		stripped_lines = [s.strip() for s in self.soloAircraftTypes_edit.toPlainText().split('\n')]
		settings.solo_aircraft_types = [s for s in stripped_lines if s != '']
		settings.solo_restrict_to_available_liveries = self.restrictAirlineChoiceToLiveries_tickBox.isChecked()
		settings.sphinx_acoustic_model_dir = self.sphinxAcousticModel_edit.text()
		settings.audio_input_device_index = self.audioDeviceIndex_edit.value()
		
		signals.systemSettingsChanged.emit()
		self.accept()






# =================================
#
#           G E N E R A L
#
# =================================


class GeneralSettingsDialog(QDialog, Ui_generalSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.positionHistoryTraceTime_edit.setMaximum(snapshot_history_size)
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(GeneralSettingsDialog.last_tab_used)
		self.addMsgPreset_button.clicked.connect(self.addPresetMessage)
		self.rmMsgPreset_button.clicked.connect(self.removePresetMessage)
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def addPresetMessage(self):
		msg, ok = QInputDialog.getText(self, 'New preset text message', 'Enter message (aliases allowed):')
		if ok:
			self.msg_presets_model.setStringList(self.msg_presets_model.stringList() + [msg])
	
	def removePresetMessage(self):
		ilst = self.messageList_view.selectedIndexes()
		if ilst != []:
			self.msg_presets_model.removeRow(ilst[0].row())

	def fillFromSettings(self):
		self.autoFillStripFromXPDR_tickBox.setChecked(settings.strip_autofill_on_ACFT_link)
		self.autoFillStripFromFPL_tickBox.setChecked(settings.strip_autofill_on_FPL_link)
		self.autoFillStripBeforeHandovers_tickBox.setChecked(settings.strip_autofill_before_handovers)
		self.confirmHandovers_tickBox.setChecked(settings.confirm_handovers)
		self.confirmLossyStripReleases_tickBox.setChecked(settings.confirm_lossy_strip_releases)
		self.confirmLinkedStripDeletions_tickBox.setChecked(settings.confirm_linked_strip_deletions)
		self.routeVectWarnings_tickBox.setChecked(settings.strip_route_vect_warnings)
		self.autoSwitchRacks_tickBox.setChecked(settings.auto_switch_racks)
		self.showRackColourDeco_tickBox.setChecked(settings.show_rack_colours)
		
		self.radarHorizontalRange_edit.setValue(settings.radar_range)
		self.radarFloor_edit.setValue(settings.radar_signal_floor_height)
		self.ignoreFloorOnRadarCheat_tickBox.setChecked(settings.ignore_floor_on_radar_cheat)
		self.radarUpdateInterval_edit.setValue(int(settings.radar_sweep_interval.total_seconds()))
		self.capability_modeA_radioButton.setChecked(settings.radar_mode_capability == 'A')
		self.capability_modeC_radioButton.setChecked(settings.radar_mode_capability == 'C')
		self.capability_modeS_radioButton.setChecked(settings.radar_mode_capability == 'S')
		self.radarContactTimeout_edit.setValue(int(settings.invisible_blips_before_contact_lost))
		self.positionHistoryTraceTime_edit.setValue(int(settings.radar_contact_trace_time.total_seconds()))
		
		self.horizontalSeparation_edit.setValue(settings.horizontal_separation)
		self.verticalSeparation_edit.setValue(settings.vertical_separation)
		self.conflictWarningFloorFL_edit.setValue(settings.conflict_warning_floor_FL)
		self.conflictWarningTime_edit.setValue(int(settings.route_conflict_anticipation.total_seconds()) / 60)
		self.trafficConsidered_select.setCurrentIndex(settings.route_conflict_traffic)
		self.headingTolerance_edit.setValue(settings.heading_tolerance)
		self.altitudeTolerance_edit.setValue(settings.altitude_tolerance)
		self.speedTolerance_edit.setValue(settings.speed_tolerance)
		self.instructRouteChangeImmediately_tickBox.setChecked(settings.instruct_route_change_immediately)
		self.openLostLegSpecsDialog_tickBox.setChecked(settings.open_lost_leg_specs_dialog)
		
		self.msg_presets_model = QStringListModel_noDropReplacement(settings.preset_chat_messages)
		self.messageList_view.setModel(self.msg_presets_model)
	
	def storeSettings(self):
		GeneralSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		
		settings.strip_autofill_on_ACFT_link = self.autoFillStripFromXPDR_tickBox.isChecked()
		settings.strip_autofill_on_FPL_link = self.autoFillStripFromFPL_tickBox.isChecked()
		settings.strip_autofill_before_handovers = self.autoFillStripBeforeHandovers_tickBox.isChecked()
		settings.confirm_handovers = self.confirmHandovers_tickBox.isChecked()
		settings.confirm_lossy_strip_releases = self.confirmLossyStripReleases_tickBox.isChecked()
		settings.confirm_linked_strip_deletions = self.confirmLinkedStripDeletions_tickBox.isChecked()
		settings.strip_route_vect_warnings = self.routeVectWarnings_tickBox.isChecked()
		settings.auto_switch_racks = self.autoSwitchRacks_tickBox.isChecked()
		settings.show_rack_colours = self.showRackColourDeco_tickBox.isChecked()
		
		settings.radar_range = self.radarHorizontalRange_edit.value()
		settings.radar_signal_floor_height = self.radarFloor_edit.value()
		settings.ignore_floor_on_radar_cheat = self.ignoreFloorOnRadarCheat_tickBox.isChecked()
		settings.radar_sweep_interval = timedelta(seconds=self.radarUpdateInterval_edit.value())
		settings.radar_mode_capability = 'A' if self.capability_modeA_radioButton.isChecked() \
				else 'C' if self.capability_modeC_radioButton.isChecked() else 'S'
		settings.invisible_blips_before_contact_lost = self.radarContactTimeout_edit.value()
		settings.radar_contact_trace_time = timedelta(seconds=self.positionHistoryTraceTime_edit.value())
		
		settings.horizontal_separation = self.horizontalSeparation_edit.value()
		settings.vertical_separation = self.verticalSeparation_edit.value()
		settings.conflict_warning_floor_FL = self.conflictWarningFloorFL_edit.value()
		settings.route_conflict_anticipation = timedelta(minutes=self.conflictWarningTime_edit.value())
		settings.route_conflict_traffic = self.trafficConsidered_select.currentIndex()
		settings.heading_tolerance = self.headingTolerance_edit.value()
		settings.altitude_tolerance = self.altitudeTolerance_edit.value()
		settings.speed_tolerance = self.speedTolerance_edit.value()
		settings.instruct_route_change_immediately = self.instructRouteChangeImmediately_tickBox.isChecked()
		settings.open_lost_leg_specs_dialog = self.openLostLegSpecsDialog_tickBox.isChecked()
		
		settings.preset_chat_messages = self.msg_presets_model.stringList()
		
		signals.generalSettingsChanged.emit()
		self.accept()




# =================================
#
#         S O L O   G A M E
#
# =================================


class QStringListModel_noDropReplacement(QStringListModel):
	def __init__(self, strlst):
		QStringListModel.__init__(self, strlst)
	
	def flags(self, index):
		flags = QStringListModel.flags(self, index)
		if index.isValid():
			flags &= ~Qt.ItemIsDropEnabled
		return flags



class SoloGameSettingsDialog(QDialog, Ui_soloGameSettingsDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.voiceInstr_off_radioButton.setChecked(True) # sets a defult; auto-excludes if voice instr selected below
		self.readBack_off_radioButton.setChecked(True) # sets a defult; auto-excludes if other selection below
		self.voiceInstr_on_radioButton.setEnabled(speech_recognition_available)
		self.readBack_voice_radioButton.setEnabled(speech_synthesis_available)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.fillFromSettings()
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def fillFromSettings(self):
		self.maxAircraftCount_edit.setValue(settings.solo_max_aircraft_count)
		self.minSpawnDelay_seconds_edit.setValue(int(settings.solo_min_spawn_delay.total_seconds()))
		self.maxSpawnDelay_minutes_edit.setValue(int(settings.solo_max_spawn_delay.total_seconds()) / 60)
		self.ARRvsDEP_edit.setValue(int(100 * settings.solo_ARRvsDEP_balance))
		self.ILSvsVisual_edit.setValue(int(100 * settings.solo_ILSvsVisual_balance))
		self.soloWeatherChangeInterval_edit.setValue(int(settings.solo_weather_change_interval.total_seconds()) / 60)
		self.voiceInstr_on_radioButton.setChecked(self.voiceInstr_on_radioButton.isEnabled() and settings.solo_voice_instructions)
		self.readBack_wilcoBeep_radioButton.setChecked(settings.solo_wilco_beeps)
		self.readBack_voice_radioButton.setChecked(self.readBack_voice_radioButton.isEnabled() and settings.solo_voice_readback)
	
	def storeSettings(self):
		settings.solo_max_aircraft_count = self.maxAircraftCount_edit.value()
		settings.solo_min_spawn_delay = timedelta(seconds=self.minSpawnDelay_seconds_edit.value())
		settings.solo_max_spawn_delay = timedelta(minutes=self.maxSpawnDelay_minutes_edit.value())
		settings.solo_ARRvsDEP_balance = self.ARRvsDEP_edit.value() / 100
		settings.solo_ILSvsVisual_balance = self.ILSvsVisual_edit.value() / 100
		settings.solo_weather_change_interval = timedelta(minutes=self.soloWeatherChangeInterval_edit.value())
		settings.solo_voice_instructions = self.voiceInstr_on_radioButton.isChecked()
		settings.solo_wilco_beeps = self.readBack_wilcoBeep_radioButton.isChecked()
		settings.solo_voice_readback = self.readBack_voice_radioButton.isChecked()
		signals.soloGameSettingsChanged.emit()
		self.accept()







# =================================
#
#           L O C A L
#
# =================================


class LocalSettingsDialog(QDialog, Ui_localSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.runway_tabs = None # Only added in AD mode
		self.airspaceRules_tab = None # AD or CTR variant added
		self.setWindowTitle('%s - %s (%s)' % (self.windowTitle(), env.locationName(), settings.location_code))
		self.previous_METAR_station = settings.primary_METAR_station
		self.range_group_boxes = [self.range1_groupBox, self.range2_groupBox, self.range3_groupBox, self.range4_groupBox]
		self.range_name_edits = [self.range1_name_edit, self.range2_name_edit, self.range3_name_edit, self.range4_name_edit]
		self.range_lo_edits = [self.range1_lo_edit, self.range2_lo_edit, self.range3_lo_edit, self.range4_lo_edit]
		self.range_hi_edits = [self.range1_hi_edit, self.range2_hi_edit, self.range3_hi_edit, self.range4_hi_edit]
		if env.airport_data == None:
			self.altOffsetForAiTraffic_label.setEnabled(False)
			self.altOffsetForAiTraffic_edit.setEnabled(False)
			self.airspaceRules_tab = AirspaceRulesWidget_CTR(self)
			self.settings_tabs.addTab(self.airspaceRules_tab, 'Airspace rules (solo CTR)')
		else:
			self.runway_tabs = RunwayTabWidget(self, lambda rwy: RunwayParametersWidget(self, rwy))
			self.settings_tabs.addTab(self.runway_tabs, 'Runway parameters')
			self.airspaceRules_tab = AirspaceRulesWidget_AD(self)
			self.settings_tabs.addTab(self.airspaceRules_tab, 'Airspace rules (solo AD)')
		self.installEventFilter(RadioKeyEventFilter(self))
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(GeneralSettingsDialog.last_tab_used)
		self.buttonBox.accepted.connect(self.storeSettings)

	def fillFromSettings(self):
		self.primaryMetarStation_edit.setText(settings.primary_METAR_station)
		self.transitionAltitude_edit.setValue(settings.transition_altitude)
		self.uncontrolledVFRcode_edit.setValue(settings.uncontrolled_VFR_XPDR_code)
		self.radioName_edit.setText(settings.location_radio_name)
		self.altOffsetForAiTraffic_edit.setValue(settings.AI_traffic_FG_AMSL_correction)
		
		for i, rng in enumerate(settings.XPDR_assignment_ranges[:len(self.range_group_boxes)]):
			self.range_group_boxes[i].setChecked(True)
			self.range_name_edits[i].setText(rng.name)
			self.range_lo_edits[i].setValue(rng.lo)
			self.range_hi_edits[i].setValue(rng.hi)
	
	def storeSettings(self):
		try:
			new_ranges = []
			for i in range(len(self.range_group_boxes)):
				if self.range_group_boxes[i].isChecked():
					name = self.range_name_edits[i].text()
					if any(rng.name == name for rng in new_ranges):
						raise ValueError('Duplicate range name')
					new_ranges.append(XpdrAssignmentRange(name, self.range_lo_edits[i].value(), self.range_hi_edits[i].value()))
		except ValueError as err:
			QMessageBox.critical(self, 'Assignment range error', str(err))
			return
		
		if self.airspaceRules_tab.checkAndSaveSettings():
			GeneralSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
			
			settings.primary_METAR_station = self.primaryMetarStation_edit.text().upper()
			settings.transition_altitude = self.transitionAltitude_edit.value()
			settings.uncontrolled_VFR_XPDR_code = self.uncontrolledVFRcode_edit.value()
			settings.location_radio_name = self.radioName_edit.text()
			settings.AI_traffic_FG_AMSL_correction = self.altOffsetForAiTraffic_edit.value()
			settings.XPDR_assignment_ranges = new_ranges
			if env.airport_data != None:
				for w in self.runway_tabs.rwyWidgets():
					w.applyToRWY()
			
			signals.localSettingsChanged.emit()
			self.accept()







class AirspaceRulesWidget_AD(QWidget, Ui_airspaceRulesWidget_AD):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.spawnAPP_minFL_edit.setValue(settings.solo_APP_ceiling_FL_min)
		self.spawnAPP_maxFL_edit.setValue(settings.solo_APP_ceiling_FL_max)
		self.TWRrangeDistance_edit.setValue(settings.solo_TWR_range_dist)
		self.TWRrangeCeiling_edit.setValue(settings.solo_TWR_ceiling_FL)
		self.entryPoints_edit.setText(' '.join(settings.solo_entry_points))
		self.exitPoints_edit.setText(' '.join(settings.solo_exit_points))
		self.initialClimb_edit.setText(settings.solo_initial_climb_reading)
		self.speedRestriction_tickBox.setChecked(settings.solo_enforce_speed_restriction)
		self.spawnAPP_minFL_edit.valueChanged.connect(self.spawnAPP_minFL_valueChanged)
		self.spawnAPP_maxFL_edit.valueChanged.connect(self.spawnAPP_maxFL_valueChanged)
		self.TWRrangeCeiling_edit.valueChanged.connect(self.TWRrangeCeiling_valueChanged)
	
	def spawnAPP_minFL_valueChanged(self, v):
		if v < self.TWRrangeCeiling_edit.value():
			self.TWRrangeCeiling_edit.setValue(v)
		if v > self.spawnAPP_maxFL_edit.value():
			self.spawnAPP_maxFL_edit.setValue(v)
	
	def spawnAPP_maxFL_valueChanged(self, v):
		if v < self.spawnAPP_minFL_edit.value():
			self.spawnAPP_minFL_edit.setValue(v)
	
	def TWRrangeCeiling_valueChanged(self, v):
		if v > self.spawnAPP_minFL_edit.value():
			self.spawnAPP_minFL_edit.setValue(v)
	
	def checkAndSaveSettings(self):
		alt_ok, alt_reading = StdPressureAlt.ckReading(self.initialClimb_edit.text())
		if not alt_ok:
			QMessageBox.critical(self, 'Invalid entry', 'Could not read altitude or level for default initial climb.')
			return False
		entry_points = self.entryPoints_edit.text().split()
		exit_points = self.exitPoints_edit.text().split()
		bad_points = set(pname for pname in entry_points + exit_points \
			if len(env.navpoints.findAll(code=pname, types=[Navpoint.VOR, Navpoint.NDB, Navpoint.FIX])) != 1)
		if len(bad_points) != 0:
			QMessageBox.critical(self, 'Invalid entry', 'Bad navpoint choices (unknown, not unique or bad type):\n%s' % ', '.join(bad_points))
			return False
		settings.solo_APP_ceiling_FL_min = self.spawnAPP_minFL_edit.value() // 10 * 10
		settings.solo_APP_ceiling_FL_max = ((self.spawnAPP_maxFL_edit.value() - 1) // 10 + 1) * 10
		settings.solo_TWR_range_dist = self.TWRrangeDistance_edit.value()
		settings.solo_TWR_ceiling_FL = self.TWRrangeCeiling_edit.value()
		settings.solo_entry_points = entry_points
		settings.solo_exit_points = exit_points
		settings.solo_enforce_speed_restriction = self.speedRestriction_tickBox.isChecked()
		settings.solo_initial_climb_reading = self.initialClimb_edit.text()
		return True




class AirspaceRulesWidget_CTR(QWidget, Ui_airspaceRulesWidget_CTR):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.spawnCTR_minFL_edit.setValue(settings.solo_CTR_floor_FL)
		self.spawnCTR_maxFL_edit.setValue(settings.solo_CTR_ceiling_FL)
		self.CTRrangeDistance_edit.setValue(settings.solo_CTR_range_dist)
		self.routingPoints_edit.setText(' '.join(settings.solo_CTR_routing_points))
		self.selectSemiCircRule(settings.solo_CTR_semi_circular_rule)
		self.spawnCTR_minFL_edit.valueChanged.connect(self.spawnCTR_minFL_valueChanged)
		self.spawnCTR_maxFL_edit.valueChanged.connect(self.spawnCTR_maxFL_valueChanged)
	
	def spawnCTR_minFL_valueChanged(self, v):
		if v > self.spawnCTR_maxFL_edit.value():
			self.spawnCTR_maxFL_edit.setValue(v)
	
	def spawnCTR_maxFL_valueChanged(self, v):
		if v < self.spawnCTR_minFL_edit.value():
			self.spawnCTR_minFL_edit.setValue(v)
		
	def selectedSemiCircRule(self):
		if self.semiCircRule_radioButton_off.isChecked(): return SemiCircRule.OFF
		elif self.semiCircRule_radioButton_EW.isChecked(): return SemiCircRule.E_W
		elif self.semiCircRule_radioButton_NS.isChecked(): return SemiCircRule.N_S
		
	def selectSemiCircRule(self, rule):
		radio_button = {
				SemiCircRule.OFF: self.semiCircRule_radioButton_off,
				SemiCircRule.E_W: self.semiCircRule_radioButton_EW,
				SemiCircRule.N_S: self.semiCircRule_radioButton_NS
			}[rule]
		radio_button.setChecked(True)
	
	def checkAndSaveSettings(self):
		routing_points = self.routingPoints_edit.text().split()
		bad_points = set(pname for pname in routing_points \
			if len(env.navpoints.findAll(code=pname, types=[Navpoint.VOR, Navpoint.NDB, Navpoint.FIX])) != 1)
		if len(bad_points) != 0:
			QMessageBox.critical(self, 'Invalid entry', 'Bad navpoint choices (unknown, not unique or bad type):\n%s' % ', '.join(bad_points))
			return False
		settings.solo_CTR_floor_FL = self.spawnCTR_minFL_edit.value()
		settings.solo_CTR_ceiling_FL = self.spawnCTR_maxFL_edit.value()
		settings.solo_CTR_range_dist = self.CTRrangeDistance_edit.value()
		settings.solo_CTR_routing_points = routing_points
		settings.solo_CTR_semi_circular_rule = self.selectedSemiCircRule()
		return True






