
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtGui import QTransform

from ui.positionDrawingDialog import Ui_positionDrawingDialog
from ui.downloadOSMPictureDialog import Ui_downloadOSMPictureDialog

from game.config import settings
from data.nav import world_nav_data
from ext.resources import read_point_spec
from game.env import env


# ---------- Constants ----------

fine_step_NM = .1
gross_step_NM = 2

# -------------------------------



class PositionBgImgDialog(QDialog, Ui_positionDrawingDialog):
	def __init__(self, graphics_items, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.items = graphics_items
		self.updateDisplay()
		self.moveUp_button.clicked.connect(lambda: self.moveImages(0, -1))
		self.moveDown_button.clicked.connect(lambda: self.moveImages(0, 1))
		self.moveLeft_button.clicked.connect(lambda: self.moveImages(-1, 0))
		self.moveRight_button.clicked.connect(lambda: self.moveImages(1, 0))
		self.increaseWidth_button.clicked.connect(lambda: self.scaleImages(1, 0))
		self.reduceWidth_button.clicked.connect(lambda: self.scaleImages(-1, 0))
		self.increaseHeight_button.clicked.connect(lambda: self.scaleImages(0, 1))
		self.reduceHeight_button.clicked.connect(lambda: self.scaleImages(0, -1))
	
	def tuningStep(self):
		return fine_step_NM if self.fineTuning_tickBox.isChecked() else gross_step_NM
	
	def updateDisplay(self):
		if self.items == []:
			txt = 'Make at least one image visible!'
		else:
			txt = ''
			for item in self.items:
				nw = item.NWcoords()
				se = item.SEcoords()
				txt += '\n%s\n' % item.title
				txt += 'NW: %s\n' % nw.toString()
				txt += 'SE: %s\n' % se.toString()
		self.central_text_area.setPlainText(txt)
	
	def moveImages(self, kx, ky):
		for item in self.items:
			item.moveBy(kx * self.tuningStep(), ky * self.tuningStep())
		self.updateDisplay()
	
	def scaleImages(self, kx, ky):
		for item in self.items:
			rect = item.boundingRect()
			scale = QTransform.fromScale(1 + kx * self.tuningStep() / rect.width(), 1 + ky * self.tuningStep() / rect.height())
			item.setTransform(scale, combine=True)
		self.updateDisplay()






class DownloadOSMPictureDialog(QDialog, Ui_downloadOSMPictureDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.last_NW_spec = self.NWcornerSpec_edit.text()
		self.last_SE_spec = self.SEcornerSpec_edit.text()
		self.last_scale_value = self.scale_edit.value()
		self.NW_point = None
		self.SE_point = None
		self.fullMapRange_button.clicked.connect(self.fillFullMapRange)
		self.OK_button.clicked.connect(self.go)
		self.cancel_button.clicked.connect(self.cancel)
	
	def fillFullMapRange(self):
		centre_point_spec = env.radarPos().toString() if env.airport_data == None else settings.location_code
		self.NWcornerSpec_edit.setText('%s>360,%d>270,%d' % (centre_point_spec, settings.map_range, settings.map_range))
		self.SEcornerSpec_edit.setText('%s>180,%d>090,%d' % (centre_point_spec, settings.map_range, settings.map_range))
	
	def go(self):
		try:
			self.NW_point = read_point_spec(self.NWcornerSpec_edit.text(), world_nav_data)
			self.SE_point = read_point_spec(self.SEcornerSpec_edit.text(), world_nav_data)
		except ValueError as err:
			QMessageBox.critical(self, 'Point specification error', 'Error in corner specification.')
			self.NW_point = self.SE_point = None
		else:
			self.last_NW_spec = self.NWcornerSpec_edit.text()
			self.last_SE_spec = self.SEcornerSpec_edit.text()
			self.accept()
	
	def cancel(self):
		self.NWcornerSpec_edit.setText(self.last_NW_spec)
		self.SEcornerSpec_edit.setText(self.last_SE_spec)
		self.scale_edit.setValue(self.last_scale_value)
		self.reject()
	
	def imageCornerPoints(self):
		return self.NW_point, self.SE_point
	
	def imageCornerSpecs(self):
		return self.last_NW_spec, self.last_SE_spec
	
	def scaleValue(self):
		return self.last_scale_value
