
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QMessageBox, QInputDialog

from game.config import settings
from game.env import env

from data.strip import Strip, recycled_detail, shelved_detail, runway_box_detail, rack_detail, received_from_detail
from models.gameStrips import default_rack_name

from gui.misc import signals, selection
from gui.dialog.miscDialogs import yesNo_question, EditRackDialog
from gui.dialog.detailSheets import StripDetailSheetDialog


# ---------- Constants ----------

rwys_tab_name = 'RWYs'

# -------------------------------



############################

##    MANIPULATE RACKS    ##

############################



def valid_new_rack_name(name):
	return name not in ['', rwys_tab_name, default_rack_name] + env.strips.rackNames()



def new_rack_dialog(parent_widget):
	new_rack_name, ok = QInputDialog.getText(parent_widget, 'New strip rack', 'Rack name:')
	if ok:
		new_rack_name = new_rack_name.strip()
		if valid_new_rack_name(new_rack_name):
			env.strips.addRack(new_rack_name)
		else:
			QMessageBox.critical(parent_widget, 'Rack name error', 'Name is reserved or already used.')
	



def edit_rack_dialog(parent_widget, old_name):
	dialog = EditRackDialog(parent_widget, old_name)
	if dialog.exec() > 0:
		if dialog.flaggedForDeletion():
			for strip in env.strips.listStrips(lambda s: s.lookup(rack_detail) == old_name):
				strip.writeDetail(recycled_detail, True)
				env.strips.repositionStrip(strip, default_rack_name)
			for atc, rack in list(settings.collecting_racks.items()):
				if rack == old_name:
					del settings.collecting_racks[atc]
			env.strips.removeRack(old_name)
		else:
			new_name, new_colour, new_collects_from = dialog.getEntries()
			if new_name != old_name: # renaming
				if valid_new_rack_name(new_name):
					env.strips.renameRack(old_name, new_name)
				else:
					QMessageBox.critical(parent_widget, 'Rack name error', 'Name is reserved or already used.')
					return # abort
			# update settings (colour)
			if old_name in settings.rack_colours:
				del settings.rack_colours[old_name]
			if new_colour != None:
				settings.rack_colours[new_name] = new_colour
			# update settings (collecting racks)
			for atc, rack in list(settings.collecting_racks.items()):
				if rack == old_name:
					del settings.collecting_racks[atc]
			for atc in new_collects_from:
				settings.collecting_racks[atc] = new_name
			# done
			signals.generalSettingsChanged.emit() # to trigger tab icon update in strip pane







#############################

##    MANIPULATE STRIPS    ##

#############################
	

def new_strip_dialog(parent_widget, rack, linkToSelection=False):
	'''
	Returns the created strip if operation not aborted
	'''
	new_strip = Strip()
	new_strip.writeDetail(rack_detail, rack)
	if linkToSelection:
		new_strip.linkAircraft(selection.acft)
		if settings.strip_autofill_on_ACFT_link:
			new_strip.fillFromXPDR()
		new_strip.linkFPL(selection.fpl)
		if settings.strip_autofill_on_FPL_link:
			new_strip.fillFromFPL()
	dialog = StripDetailSheetDialog(parent_widget, new_strip)
	dialog.exec()
	if dialog.result() > 0: # not rejected
		new_strip.writeDetail(rack_detail, dialog.selectedRack())
		env.strips.addStrip(new_strip)
		selection.selectStrip(new_strip)
		return new_strip
	else:
		return None


def discard_strip(parent_widget, strip, shelve):
	'''
	Returns the discarded strip if operation not aborted
	Argument "shelve" is True, or False to delete
	'''
	if strip == None:
		return
	acft = strip.linkedAircraft()
	fpl = strip.linkedFPL()
	if shelve:
		if fpl == None or strip.FPLconflictList() != []:
			if settings.confirm_lossy_strip_releases and not yesNo_question(parent_widget, 'Releasing strip details', \
					'Strip has conflicts or no linked flight plan.', 'Release contact losing strip details?'):
				signals.stripEditRequest.emit(strip)
				return # abort sending/discarding
		elif not fpl.existsOnline() or fpl.needsUpload():
			if settings.confirm_lossy_strip_releases and not yesNo_question(parent_widget, 'Releasing strip details', \
					'Linked FPL is not online or has local changes.', 'Release contact without pushing your details online?'):
				signals.FPLeditRequest.emit(fpl)
				return # abort sending/discarding
	else: # deleting strip (not shelving)
		if acft != None or fpl != None:
			if settings.confirm_linked_strip_deletions and not yesNo_question(parent_widget, 'Delete strip', 'Strip is linked.', 'Delete?'):
				return # abort sending/discarding
	strip.linkAircraft(None)
	strip.linkFPL(None)
	env.strips.removeStrip(strip)
	strip.writeDetail(shelved_detail, shelve)
	env.discarded_strips.addStrip(strip)
	if strip is selection.strip:
		selection.deselect()
	return strip




def edit_strip(parent_widget, strip):
	old_rack = strip.lookup(rack_detail) # may be None
	dialog = StripDetailSheetDialog(parent_widget, strip)
	dialog.exec()
	new_rack = dialog.selectedRack()
	strip.writeDetail(rack_detail, new_rack)
	if dialog.result() > 0 and new_rack != old_rack: # not rejected and rack changed
		env.strips.repositionStrip(strip, new_rack)
	signals.selectionChanged.emit()
	signals.stripInfoChanged.emit()





def pull_XPDR_details(parent_widget):
	if yesNo_question(parent_widget, 'Pull XPDR details', 'This will overwrite strip details.', 'Are you sure?'):
		selection.strip.fillFromXPDR(ovr=True)
	signals.stripInfoChanged.emit()

def pull_FPL_details(parent_widget):
	if yesNo_question(parent_widget, 'Pull FPL details', 'This will overwrite strip details.', 'Are you sure?'):
		selection.strip.fillFromFPL(ovr=True)
	signals.stripInfoChanged.emit()



def receive_strip(strip):
	rack = strip.lookup(rack_detail)
	if rack == None:
		recv_from = strip.lookup(received_from_detail)
		if recv_from != None:
			rack = settings.collecting_racks.get(recv_from, default_rack_name)
	if rack not in env.strips.rackNames():
		rack = default_rack_name
	strip.writeDetail(rack_detail, rack)
	env.strips.addStrip(strip)

def recover_strip(strip):
	env.discarded_strips.remove(strip)
	strip.writeDetail(recycled_detail, True)
	strip.writeDetail(shelved_detail, None)
	strip.writeDetail(runway_box_detail, None)
	strip.writeDetail(rack_detail, default_rack_name)
	env.strips.addStrip(strip)

def recover_from_failed_handover(parent_widget, strip, msg):
	recover_strip(strip)
	QMessageBox.critical(parent_widget, 'Handover failed', '%s\nStrip has been recovered.' % msg)



