
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import Qt, QRectF, QPointF, QTimer, pyqtSignal
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsScene, QInputDialog

from game.config import settings

from game.env import env
from game.manager import GameType
from ext.xplane import get_taxiways, get_airport_boundary, get_airport_linear_objects
from ext.resources import navpointFromSpec

from data.acft import Aircraft
from data.coords import RadarCoords, EarthCoords, dist_str
from data.params import Heading, TTF_str
from data.nav import Navpoint, NavpointError

from gui.misc import signals, selection
from gui.graphics.radarContact import AircraftItem
from gui.graphics.infoBox import TextBoxItem
from gui.graphics.airport import RunwayItem, HelipadItem, TarmacSectionItem, \
		ParkingPositionItem, AirportGroundNetItem, AirportLinearObjectItem, WindsockItem, TowerItem
from gui.graphics.navpoints import NavVORItem, NavNDBItem, NavFixItem, NavAirfieldItem, RnavItem
from gui.graphics.miscGraphics import new_pen, EmptyGraphicsItem, \
		MeasuringToolItem, CustomLabelItem, BgPixmapItem, BgHandDrawingItem, MouseOverLabelledItem


# ---------- Constants ----------

bg_radar_circle_step = 20
navpoint_indicator_timeout = 3000 # milliseconds
init_speed_mark_count = 2 # one minute of fly time each

# -------------------------------


class Layer:
	radar_layer_names = RADAR_BACKGROUND, TARMAC, BG_IMAGES, RUNWAYS, AIRPORT_LINES, GROUND_NET, PARKING_POSITIONS, \
			AIRPORT_OBJECTS, NAV_AIRFIELDS, NAV_FIXES, RNAV_POINTS, NAV_AIDS, CUSTOM_LABELS, AIRCRAFT, RADAR_FOREGROUND = range(15)


navpoint_layers = {
	Navpoint.AD: Layer.NAV_AIRFIELDS, Navpoint.VOR: Layer.NAV_AIDS, Navpoint.NDB: Layer.NAV_AIDS,
	Navpoint.FIX: Layer.NAV_FIXES, Navpoint.RNAV: Layer.RNAV_POINTS
}

navpoint_colours = {
	Navpoint.AD: 'nav_airfield', Navpoint.VOR: 'nav_aid', Navpoint.NDB: 'nav_aid',
	Navpoint.FIX: 'nav_fix', Navpoint.RNAV: 'nav_RNAV'
}








class RadarCircleItem(QGraphicsItem):
	def __init__(self, init_radius, colour_name, pen_style):
		QGraphicsItem.__init__(self, parent=None)
		self.setAcceptedMouseButtons(Qt.NoButton)
		self.radius = init_radius
		self.colour_name = colour_name
		self.pen_style = pen_style
	
	def updateRadius(self):
		self.prepareGeometryChange()
		self.radius = settings.radar_range
	
	def boundingRect(self):
		return QRectF(-self.radius, -self.radius, 2 * self.radius, 2 * self.radius)

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour(self.colour_name), style=self.pen_style))
		painter.drawEllipse(self.boundingRect())



class PointIndicatorItem(QGraphicsItem):
	def __init__(self):
		QGraphicsItem.__init__(self, None)
		self.setFlag(QGraphicsItem.ItemIgnoresTransformations, True)
		self.setVisible(False)
		self.timer = QTimer(self.scene())
		self.timer.setSingleShot(True)
		self.timer.timeout.connect(lambda: self.setVisible(False))
		
	def boundingRect(self):
		return QRectF(-15, -15, 30, 30)

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour('point_indicator'), width=2))
		painter.drawEllipse(QPointF(0, 0), 12, 6)
		painter.drawEllipse(QPointF(0, 0), 6, 12)
	
	def indicate(self, coords):
		self.setPos(coords.toQPointF())
		self.setVisible(True)
		self.timer.start(navpoint_indicator_timeout)





class RadarScene(QGraphicsScene):
	mouseInfo = pyqtSignal(str)
	addRemoveRouteNavpoint = pyqtSignal(Navpoint)
	imagesRedrawn = pyqtSignal()
	
	def __init__(self, parent):
		QGraphicsScene.__init__(self, parent)
		if TextBoxItem.rectangle.isNull():
			TextBoxItem.setRectangleFromFont(self.font())
		self.show_info_boxes = False
		self.show_all_vectors = False
		self.show_all_routes = False
		self.show_separation_rings = False
		self.show_selected_ACFT_assignments = False
		self.show_slope_altitudes = True
		self.show_interception_cones = False
		self.show_ground_networks = False
		self.show_taxiway_names = False
		self.show_GND_modes = True
		self.runway_names_always_visible = False
		self.speed_mark_count = init_speed_mark_count
		self.mouseover_highlights_groundnet_edges = False
		self.mouse_info_shows_coords = False
		self.point_indicator = PointIndicatorItem()
		self.measuring_tool = MeasuringToolItem()
		self.using_special_tool = False # If measuring, this holds whether SHIFT was pressed on click before drag
		self.setBackgroundBrush(settings.colour('radar_background'))
		self.additional_AD_items = []
		# Create layers
		self.layers = { l:EmptyGraphicsItem() for l in Layer.radar_layer_names }
		for layer in self.layers.values():
			self.addItem(layer)
		self.pinned_navpoints_layer = EmptyGraphicsItem()
		self.addItem(self.pinned_navpoints_layer)
		self.pinned_pkpos_layer = EmptyGraphicsItem()
		self.addItem(self.pinned_pkpos_layer)
		self.replaced_AD_items_layer = EmptyGraphicsItem()
		self.addItem(self.replaced_AD_items_layer)
		self.replaced_AD_items_layer.setVisible(False)
		
		# Populate radar back- and fore-ground
		if env.airport_data != None:
			for n in range(1, int(settings.map_range / bg_radar_circle_step) + 1):
				dist = n * bg_radar_circle_step
				self.addToLayer(Layer.RADAR_BACKGROUND, RadarCircleItem(dist, 'radar_circle', Qt.SolidLine))
		self.radar_range_item = RadarCircleItem(settings.radar_range, 'radar_range_limit', Qt.DotLine)
		self.addToLayer(Layer.RADAR_BACKGROUND, self.radar_range_item)
		self.addToLayer(Layer.RADAR_FOREGROUND, self.point_indicator)
		self.addToLayer(Layer.RADAR_FOREGROUND, self.measuring_tool)
		
		# Populate background images
		self.redrawBackgroundImages()
		
		# Populate airport objects
		if env.airport_data != None:
			self._drawAirportData(env.airport_data, False) # all things that are drawn for additional airports
		
		# Populate navpoints
		to_pin = []
		for pspec in settings.saved_pinned_navpoints: # str specs
			try:
				to_pin.append(navpointFromSpec(pspec, env.navpoints))
			except NavpointError:
				print('Cannot identify what to pin: %s' % pspec)
		for p in env.navpoints.findAll(types=navpoint_layers.keys()):
			if p.type == Navpoint.AD and p.code == settings.location_code:
				continue # do not draw base airport navpoint
			if p.type == Navpoint.VOR:
				base_item = NavVORItem(p)
			elif p.type == Navpoint.NDB:
				base_item = NavNDBItem(p)
			elif p.type == Navpoint.FIX:
				base_item = NavFixItem(p)
			elif p.type == Navpoint.RNAV:
				base_item = RnavItem(p)
			elif p.type == Navpoint.AD:
				base_item = NavAirfieldItem(p)
			label = '%s\n%s' % (p.code, p.frequency) if p.type in [Navpoint.VOR, Navpoint.NDB] else p.code
			item = MouseOverLabelledItem(base_item, label, navpoint_colours[p.type], self.pinned_navpoints_layer)
			item.setPos(p.coordinates.toQPointF())
			self.addToLayer(navpoint_layers[p.type], item)
			if any(tp.type == p.type and tp.code == p.code for tp in to_pin):
				item.pinLabel(True)
		
		# Populate aircraft already in contact
		for acft in env.radar.contacts():
			self.addAircraftItem(acft)
		
		# Restore custom labels
		for lbl, pos_str in settings.saved_custom_labels:
			try:
				self.addCustomLabel(lbl, EarthCoords.fromString(pos_str).toQPointF())
			except ValueError:
				print('Cannot restore custom label "%s": bad position string.' % pos_str)
		
		# Connect signals
		env.radar.blip.connect(self.updateContacts)
		env.radar.blip.connect(self.updateRunways)
		env.radar.newContact.connect(self.addAircraftItem)
		env.radar.lostContact.connect(self.removeAircraftItem)
		env.strips.rwyBoxFilled.connect(self.updateRunways)
		env.strips.rwyBoxFreed.connect(self.updateRunways)
		signals.aircraftKilled.connect(self.removeAircraftItem)
		signals.stripInfoChanged.connect(self.updateContacts)
		signals.selectionChanged.connect(self.updateAfterSelectionChanged)
		signals.generalSettingsChanged.connect(self.updateAfterGeneralSettingsChanged)
		signals.runwayUseChanged.connect(self.updateRunwayNamesVisibility)
		signals.localSettingsChanged.connect(self.updateLocaliserItems)
		signals.backgroundImagesReloaded.connect(self.redrawBackgroundImages)
		signals.colourConfigReloaded.connect(self.updateBgColours)
		signals.gameStopped.connect(self.clearAircraftItems)
	
	## DELETING
	def disconnectAllSignals(self):
		env.radar.blip.disconnect(self.updateContacts)
		env.radar.blip.disconnect(self.updateRunways)
		env.radar.newContact.disconnect(self.addAircraftItem)
		env.radar.lostContact.disconnect(self.removeAircraftItem)
		env.strips.rwyBoxFilled.disconnect(self.updateRunways)
		env.strips.rwyBoxFreed.disconnect(self.updateRunways)
		signals.aircraftKilled.disconnect(self.removeAircraftItem)
		signals.stripInfoChanged.disconnect(self.updateContacts)
		signals.selectionChanged.disconnect(self.updateAfterSelectionChanged)
		signals.generalSettingsChanged.disconnect(self.updateAfterGeneralSettingsChanged)
		signals.runwayUseChanged.disconnect(self.updateRunwayNamesVisibility)
		signals.localSettingsChanged.disconnect(self.updateLocaliserItems)
		signals.backgroundImagesReloaded.disconnect(self.redrawBackgroundImages)
		signals.colourConfigReloaded.disconnect(self.updateBgColours)
		signals.gameStopped.disconnect(self.clearAircraftItems)
	
	
	## MISC.
	def _drawAirportData(self, ad_data, resets):
		for twy in get_taxiways(ad_data.navpoint.code):
			self.addToLayer(Layer.TARMAC, TarmacSectionItem(*twy), resets)
		ad_boundary_path = get_airport_boundary(ad_data.navpoint.code)
		self.addToLayer(Layer.AIRPORT_OBJECTS, AirportLinearObjectItem(ad_boundary_path, 'nav_airfield'), resets)
		for pos, height, name in ad_data.viewpoints:
			item = TowerItem(name)
			item.setPos(pos.toQPointF())
			self.addToLayer(Layer.AIRPORT_OBJECTS, item, resets)
		for windsock_coords in ad_data.windsocks:
			self.addToLayer(Layer.AIRPORT_OBJECTS, WindsockItem(windsock_coords), resets)
		for linobj, qpath in get_airport_linear_objects(ad_data.navpoint.code):
			self.addToLayer(Layer.AIRPORT_LINES, AirportLinearObjectItem(qpath, 'airport_lines'), resets)
		for i in range(ad_data.physicalRunwayCount()):
			self.addToLayer(Layer.RUNWAYS, RunwayItem(ad_data, i), resets)
		for h in ad_data.helipads:
			self.addToLayer(Layer.RUNWAYS, HelipadItem(h), resets)
		for pk in ad_data.ground_net.parkingPositions():
			pos, hdg, typ = ad_data.ground_net.parkingPosInfo(pk)[0:3]
			pk_item = MouseOverLabelledItem(ParkingPositionItem(pk, hdg), pk, 'parking_position', self.pinned_pkpos_layer)
			pk_item.setPos(pos.toQPointF())
			self.addToLayer(Layer.PARKING_POSITIONS, pk_item, resets)
			if ad_data.navpoint.code == settings.location_code and pk in settings.saved_pinned_parking_positions:
				pk_item.pinLabel(True)
		self.addToLayer(Layer.GROUND_NET, AirportGroundNetItem(ad_data.ground_net), resets)
	
	def updateBgColours(self):
		self.setBackgroundBrush(settings.colour('radar_background'))
	
	def redrawBackgroundImages(self):
		for img_item in self.layerItems(Layer.BG_IMAGES):
			self.removeItem(img_item)
		for is_pixmap, src, title, constr in settings.radar_background_images:
			item = BgPixmapItem(src, title, *constr) if is_pixmap else BgHandDrawingItem(src, title, constr)
			self.addToLayer(Layer.BG_IMAGES, item)
		self.imagesRedrawn.emit()
	
	def addToLayer(self, layer, item, resets=False):
		item.setParentItem(self.layers[layer])
		if resets:
			self.additional_AD_items.append(item)
	
	def layerItems(self, layer):
		return self.layers[layer].childItems()
	
	def pinnedNavpoints(self):
		return [item.child_item.navpoint for item in self.pinned_navpoints_layer.childItems()]
	
	def pinnedParkingPositions(self):
		return [item.child_item.name for item in self.pinned_pkpos_layer.childItems()]
	
	def customLabels(self):
		return [(item.label(), item.earthCoords()) for item in self.layerItems(Layer.CUSTOM_LABELS)]
	
	def speedMarkCount(self):
		return self.speed_mark_count
	
	
	## SHOW TIME
	
	def showLandingHelper(self, rwy, toggle):
		for item in self.layerItems(Layer.RUNWAYS):
			if isinstance(item, RunwayItem):
				LOC_item = item.localiserItemForRWY(rwy)
				if LOC_item != None:
					LOC_item.setVisible(toggle)
					return
	
	def showGroundNetworks(self, toggle):
		self.show_ground_networks = toggle
		self.layers[Layer.GROUND_NET].setVisible(False)
		self.layers[Layer.GROUND_NET].setVisible(True)
	
	def showTaxiwayNames(self, toggle):
		self.show_taxiway_names = toggle
		for gnd_net_item in self.layerItems(Layer.GROUND_NET):
			gnd_net_item.updateLabelsVisibility()
	
	def highlightEdgesOnMouseover(self, toggle):
		self.mouseover_highlights_groundnet_edges = toggle
	
	def setMouseInfoShowsCoords(self, toggle):
		self.mouse_info_shows_coords = toggle
	
	def setRunwayNamesAlwaysVisible(self, toggle):
		self.runway_names_always_visible = toggle
		for item in self.layerItems(Layer.RUNWAYS):
			item.updateRwyNamesVisibility()
	
	def showSlopeAltitudes(self, toggle):
		self.show_slope_altitudes = toggle
		self.updateLocaliserItems()
	
	def showInterceptionCones(self, toggle):
		self.show_interception_cones = toggle
		self.updateLocaliserItems()
	
	def showInfoBoxes(self, toggle):
		self.show_info_boxes = toggle
		self.updateContacts()
	
	def showVectors(self, toggle):
		self.show_all_vectors = toggle
		self.updateContacts()
	
	def showRoutes(self, toggle):
		self.show_all_routes = toggle
		self.updateContacts()
	
	def showSeparationRings(self, toggle):
		self.show_separation_rings = toggle
		self.updateContacts()
	
	def showSelectionAssignments(self, toggle):
		self.show_selected_ACFT_assignments = toggle
		self.updateContacts()
	
	def showGndModes(self, toggle):
		self.show_GND_modes = toggle
		self.updateContacts()
	
	def drawAirportData(self, ad_data):
		try: # look for item to replace
			try: # in pinned items...
				replaced = next(item for item in self.pinned_navpoints_layer.childItems() if \
						isinstance(item.child_item, NavAirfieldItem) and item.child_item.navpoint.code == ad_data.navpoint.code)
				replaced._was_pinned = True
			except StopIteration: # ... and in other airfields
				replaced = next(item for item in self.layerItems(Layer.NAV_AIRFIELDS) if item.child_item.navpoint.code == ad_data.navpoint.code)
				replaced._was_pinned = False
		except StopIteration: # item already drawn; indicate point
			self.point_indicator.indicate(ad_data.navpoint.coordinates)
		else: # item found!
			replaced.setParentItem(self.replaced_AD_items_layer)
			self._drawAirportData(ad_data, True)
	
	def resetAirportItems(self):
		while self.additional_AD_items != []:
			self.removeItem(self.additional_AD_items.pop())
		for item in self.replaced_AD_items_layer.childItems():
			item.setParentItem(self.pinned_navpoints_layer if item._was_pinned else self.layers[Layer.NAV_AIRFIELDS])
	
	def setSpeedMarkCount(self, new_value):
		self.speed_mark_count = new_value
		self.updateContacts()

	
	## UPDATING AIRCRAFT CONTACTS
	
	def addAircraftItem(self, contact):
		self.addToLayer(Layer.AIRCRAFT, AircraftItem(contact))
	
	def addCustomLabel(self, name, qpos):
		lbl = CustomLabelItem(name)
		lbl.setPos(qpos)
		self.addToLayer(Layer.CUSTOM_LABELS, lbl)
	
	def removeAircraftItem(self, zombie):
		try:
			acft_item = next(item for item in self.layerItems(Layer.AIRCRAFT) if item.radar_contact is zombie)
			self.removeItem(acft_item)
		except StopIteration:
			print('Graphics item not found for zombie %s' % zombie.identifier)
	
	def updateRunways(self):
		for item in self.layerItems(Layer.RUNWAYS):
			if isinstance(item, RunwayItem):
				item.updateItem()
	
	def updateContacts(self):
		for item in self.layerItems(Layer.AIRCRAFT):
			item.updateItem()
			item.setVisible(self.show_GND_modes or not item.radar_contact.xpdrGND() or env.linkedStrip(item.radar_contact) != None)
	
	def updateAfterSelectionChanged(self):
		for item in self.layerItems(Layer.AIRCRAFT):
			item.setSelected(item.radar_contact is selection.acft)
			item.updateItem()
	
	def updateAfterGeneralSettingsChanged(self):
		self.radar_range_item.updateRadius()
		for item in self.layerItems(Layer.AIRCRAFT):
			item.updateAfterGeneralSettingsChanged()
	
	def updateLocaliserItems(self):
		for item in self.layerItems(Layer.RUNWAYS):
			if isinstance(item, RunwayItem):
				item.LOC_item1.updateFromSettings()
				item.LOC_item2.updateFromSettings()
	
	def updateRunwayNamesVisibility(self):
		for item in self.layerItems(Layer.RUNWAYS):
			item.updateRwyNamesVisibility()
	
	def clearAircraftItems(self):
		for item in self.layerItems(Layer.AIRCRAFT):
			self.removeItem(item)

	
	## MOUSE EVENTS

	def _mouseInfo_flyToMouse(self, radarXY):
		ref_contact = selection.acft
		if ref_contact == None:
			self.mouseInfo.emit('')
		else:
			acftXY = ref_contact.coords().toRadarCoords()
			dist = acftXY.distanceTo(radarXY)
			disp = '%s°, %s' % (acftXY.headingTo(radarXY).read(), dist_str(dist))
			spd = ref_contact.groundSpeed()
			if spd != None:
				try:
					disp += ', TTF ' + TTF_str(dist, spd)
				except ValueError:
					pass
			self.mouseInfo.emit('To mouse: ' + disp)
	
	def _mouseInfo_measuringPointData(self, radarXY):
		if self.mouse_info_shows_coords:
			self.mouseInfo.emit('Coords: %s' % EarthCoords.fromRadarCoords(radarXY))
		else: # mouse info shows elevation
			if env.elevation_map == None:
				datastr = 'N/A' 
			else:
				try:
					datastr = '%.1f' % env.elevation_map.elev(radarXY)
				except ValueError: # outside of map
					datastr = 'N/A' 
			self.mouseInfo.emit('Elevation: %s' % datastr)
	
	def mousePressEvent(self, event):
		if event.button() != Qt.RightButton:
			QGraphicsScene.mousePressEvent(self, event)
		if not event.isAccepted():
			if event.modifiers() & Qt.ShiftModifier and (event.button() == Qt.RightButton \
					or event.button() == Qt.LeftButton and settings.game_manager.isRunning() and settings.game_manager.game_type == GameType.TEACHER):
				self.using_special_tool = event.button() == Qt.LeftButton
				self.measuring_tool.setPos(event.scenePos())
				self.measuring_tool.start(not self.using_special_tool)
				rxy = RadarCoords.fromQPointF(event.scenePos())
				self._mouseInfo_measuringPointData(rxy)
			elif event.button() == Qt.LeftButton and self.mouseGrabberItem() == None:
				selection.deselect()
			event.accept()
	
	def mouseMoveEvent(self, event):
		rxy = RadarCoords.fromQPointF(event.scenePos())
		if self.measuring_tool.isVisible():
			self.measuring_tool.updateMouseXY(event.scenePos() - self.measuring_tool.pos())
			if self.using_special_tool:
				self.mouseInfo.emit('Creating traffic...')
			else:
				self._mouseInfo_measuringPointData(rxy)
		elif not event.isAccepted():
			self._mouseInfo_flyToMouse(rxy)
		QGraphicsScene.mouseMoveEvent(self, event)
	
	def mouseReleaseEvent(self, event):
		rxy = RadarCoords.fromQPointF(event.scenePos())
		if self.measuring_tool.isVisible() and event.button() in [Qt.LeftButton, Qt.RightButton]:
			if self.using_special_tool:
				stxy = RadarCoords.fromQPointF(self.measuring_tool.pos())
				signals.specialTool.emit(EarthCoords.fromRadarCoords(stxy), self.measuring_tool.measuredHeading())
			self.measuring_tool.stop()
		self._mouseInfo_flyToMouse(rxy)
		QGraphicsScene.mouseReleaseEvent(self, event)

	def mouseDoubleClickEvent(self, event):
		QGraphicsScene.mouseDoubleClickEvent(self, event)
		if not event.isAccepted() and event.button() == Qt.LeftButton and event.modifiers() & Qt.ShiftModifier:
			text, ok = QInputDialog.getText(self.parent(), 'Add custom label', 'Text:')
			if ok and text.strip() != '':
				self.addCustomLabel(text, event.scenePos())
		

