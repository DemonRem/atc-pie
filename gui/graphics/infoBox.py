
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import Qt, QRect, QRectF, QPointF
from PyQt5.QtWidgets import QGraphicsItem
from PyQt5.QtGui import QFontMetrics

from data.util import some
from game.config import settings
from data.radar import XPDR_emergency_codes
from data.strip import parsed_route_detail, assigned_altitude_detail
from data.params import StdPressureAlt
from game.env import env
from data.fpl import FPL
from data.nav import world_nav_data, NavpointError
from gui.misc import signals, selection
from gui.graphics.miscGraphics import withMargins, new_pen, ACFT_pen_colour


# ---------- Constants ----------

vertical_speed_sensitivity = 100 # feet per minute
init_info_box_offset = QPointF(-65, -35)

# -------------------------------



def infoTextLines(radar_contact):
	strip = env.linkedStrip(radar_contact)
	# LINE 1
	cs = radar_contact.xpdrCallsign()
	if cs == None and strip != None:
		cs = strip.callsign(fpl=True)
	pcs = ('*%s' if strip != None and strip.lookup(FPL.COMMENTS) != None else '%s') % some(cs, '?')
	line1 = '[%s]' % pcs if radar_contact.individual_cheat else pcs
	if radar_contact.xpdrIdent():
		line1 += '  !!ident'
	t = radar_contact.xpdrAcftType()
	if t == None and strip != None:
		t = strip.lookup(FPL.ACFT_TYPE, fpl=True)
	if t != None:
		line1 += '  %s' % t
	
	# LINE 2
	if strip == None or radar_contact.xpdrSqCode() in XPDR_emergency_codes: # Show XPDR code
		xpdr_code = radar_contact.xpdrSqCode()
		line2 = '' if xpdr_code == None else '%04o' % xpdr_code
		if xpdr_code in XPDR_emergency_codes:
			line2 += '  !!EMG'
	else:
		parsed_route = strip.lookup(parsed_route_detail)
		if parsed_route == None:
			dest = some(strip.lookup(FPL.ICAO_ARR, fpl=True), '')
			try:
				ad = world_nav_data.findAirfield(dest)
				line2 = '%s  %s°' % (ad.code, radar_contact.coords().headingTo(ad.coordinates).read())
			except NavpointError: # not an airport
				line2 = dest
		else: # got parsed route
			leg = parsed_route.currentLegIndex(radar_contact.coords())
			if leg == 0 and parsed_route.SID() != None:
				line2 = 'SID %s' % parsed_route.SID()
			elif leg == parsed_route.legCount() - 1 and parsed_route.STAR() != None:
				line2 = 'STAR %s' % parsed_route.STAR()
			else:
				wp = parsed_route.waypoint(leg)
				line2 = '%s  %s°' % (wp.code, radar_contact.coords().headingTo(wp.coordinates).read())
	# LINE 3
	vs = radar_contact.verticalSpeed()
	if vs == None:
		comp_char = '-'
	elif abs(vs) < vertical_speed_sensitivity:
		comp_char = '='
	elif vs > 0:
		comp_char = '^'
	else:
		comp_char = 'v'
	ass_alt = None if strip == None else strip.lookup(assigned_altitude_detail)
	if ass_alt != None:
		ok, ass_alt = StdPressureAlt.ckReading(ass_alt)
		if not ok:
			ass_alt = '!!alt?'
	if settings.radar_mode_capability == 'A':
		line3 = ''
	else:
		if radar_contact.xpdrGND():
			line3 = 'GND '
		else:
			xpdr_alt = radar_contact.xpdrAlt()
			if xpdr_alt == None:
				line3 = 'alt? '
			else:
				line3 = '%s %c ' % (xpdr_alt.read(env.QNH(), unit=False), comp_char)
	if ass_alt != None:
		line3 += ass_alt
	line3 += '  '
	if strip != None:
		line3 += some(strip.lookup(FPL.WTC, fpl=True), '')
	speed = radar_contact.groundSpeed()
	if speed != None:
		line3 += '%03d' % speed.kt
	
	return '%s\n%s\n%s' % (line1, line2, line3)






















class RadarInfoBoxItem(QGraphicsItem):
	def __init__(self, acft_item):
		QGraphicsItem.__init__(self, acft_item)
		self.setVisible(False)
		self.radar_contact = acft_item.radar_contact
		self.setFlag(QGraphicsItem.ItemIgnoresTransformations, True)
		self.text_box_item = TextBoxItem(self)
		self.callout_line_start = QPointF(0, 0)
		self.callout_line_end = self.text_box_item.pos() + self.text_box_item.calloutConnectingPoint()
	
	def updateInfoText(self):
		self.text_box_item.info_text = infoTextLines(self.radar_contact)
		self.update(self.boundingRect())
	
	def textBoxMoved(self):
		self.prepareGeometryChange()
		self.callout_line_end = self.text_box_item.pos() + self.text_box_item.calloutConnectingPoint()
		self.update(self.boundingRect())
		
	def paint(self, painter, option, widget):
		# Draw callout line; child text box draws itself
		painter.setPen(new_pen(settings.colour('info_box_line')))
		painter.drawLine(self.callout_line_start, self.callout_line_end)
		
	def boundingRect(self):
		return QRectF(self.callout_line_start, self.callout_line_end).normalized() | self.childrenBoundingRect()







class TextBoxItem(QGraphicsItem):
	max_rect = QRect(-56, -20, 112, 40)
	dummy_contents = 'XX-ABCDE  ######\n##### xxxxx\n10000 = 10000  H####'
	rectangle = QRectF() # STATIC
	
	def setRectangleFromFont(font):
		TextBoxItem.rectangle = QRectF(QFontMetrics(font).boundingRect(TextBoxItem.max_rect, Qt.AlignLeft, TextBoxItem.dummy_contents))
	
	def __init__(self, parent_item):
		QGraphicsItem.__init__(self, parent_item)
		self.radar_contact = parent_item.radar_contact
		self.info_text = infoTextLines(self.radar_contact)
		self.setCursor(Qt.PointingHandCursor)
		self.setPos(init_info_box_offset)
		self.moved_since_last_switch = False
		self.setFlag(QGraphicsItem.ItemIsMovable, True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
	
	def positionQuadrant(self):
		return (1 if self.pos().x() > 0 else -1), (1 if self.pos().y() > 0 else -1)
	
	def calloutConnectingPoint(self):
		q = self.positionQuadrant()
		if q == (-1, -1): return TextBoxItem.rectangle.bottomRight()
		elif q == (-1, 1): return TextBoxItem.rectangle.topRight()
		elif q == (1, -1): return TextBoxItem.rectangle.bottomLeft()
		elif q == (1, 1): return TextBoxItem.rectangle.topLeft()
	
	def paint(self, painter, option, widget):
		coloured_pen = new_pen(ACFT_pen_colour(self.radar_contact))
		# 1. Write info text
		painter.setPen(coloured_pen)
		painter.drawText(TextBoxItem.rectangle, Qt.AlignLeft | Qt.AlignVCenter, self.info_text)
		# 2. Draw container box.
		if self.radar_contact is selection.acft:
			painter.setPen(coloured_pen)
		else:
			painter.setPen(new_pen(settings.colour('info_box_line')))
		painter.drawRect(TextBoxItem.rectangle)
		
	def boundingRect(self):
		return TextBoxItem.rectangle
	

	# EVENTS
	
	def itemChange(self, change, value):
		if change == QGraphicsItem.ItemPositionChange:
			self.parentItem().textBoxMoved()
		return QGraphicsItem.itemChange(self, change, value)

	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton:
			selection.selectAircraft(self.radar_contact)
		elif event.button() == Qt.MiddleButton:
			if event.modifiers() & Qt.ShiftModifier:
				selection.unlinkAircraft(self.radar_contact)
			else:
				selection.linkAircraft(self.radar_contact)
			event.accept()
		QGraphicsItem.mousePressEvent(self, event)
	
	def mouseMoveEvent(self, event):
		if event.buttons() & Qt.LeftButton: # No direct button() in mouseMoveEvents
			self.moved_since_last_switch = True
		QGraphicsItem.mouseMoveEvent(self, event)

	def mouseDoubleClickEvent(self, event):
		if event.button() == Qt.LeftButton:
			if event.modifiers() & Qt.ShiftModifier: # reset box position
				if self.moved_since_last_switch:
					qx, qy = self.positionQuadrant()
					self.setPos(qx * abs(init_info_box_offset.x()), qy * abs(init_info_box_offset.y()))
					self.moved_since_last_switch = False
					self.parentItem().textBoxMoved()
			else:
				strip = selection.strip
				if strip != None:
					signals.stripEditRequest.emit(strip)
			event.accept()
		else:
			QGraphicsItem.mouseDoubleClickEvent(self, event)
