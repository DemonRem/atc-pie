
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta

from PyQt5.QtCore import pyqtSignal, QObject, QTimer

from data.acft import Aircraft
from data.strip import Strip, assigned_heading_detail, assigned_altitude_detail, assigned_speed_detail, assigned_SQ_detail
from data.fpl import FPL
from data.coords import EarthCoords
from data.params import Heading, Speed
from data.nav import Navpoint
from data.comms import ChatMessage
from data.instruction import Instruction
from data.weather import Weather

from game.config import settings
from game.env import env
from ext.tts import PilotVoice


# ---------- Constants ----------

clock_ticker_interval = 333 # ms

# -------------------------------




##-----------------------------------##
##                                   ##
##          GUI ENVIRONMENT          ##
##                                   ##
##-----------------------------------##




class IconFile:
	action_envInfo = 'resources/pixmap/info.png'
	action_generalSettings = 'resources/pixmap/tools.png'
	action_runwayUse = 'resources/pixmap/AD.png'
	action_newRadar = 'resources/pixmap/radar.png'
	action_newLooseBay = 'resources/pixmap/looseStrips.png'
	action_newStrip = 'resources/pixmap/newStrip.png'
	action_newLinkedStrip = 'resources/pixmap/newLinkedStrip.png'
	action_newFPL = 'resources/pixmap/newFPL.png'
	action_newLinkedFPL = 'resources/pixmap/newLinkedFPL.png'
	action_newRack = 'resources/pixmap/newRack.png'

	dock_ATCs = 'resources/pixmap/envelope.png'
	dock_FPLs = 'resources/pixmap/FPLicon.png'
	dock_instructions = 'resources/pixmap/instruction.png'
	dock_navigator = 'resources/pixmap/compass.png'
	dock_notepads = 'resources/pixmap/notepad.png'
	dock_notifications = 'resources/pixmap/lightBulb.png'
	dock_radios = 'resources/pixmap/radio.png'
	dock_selInfo = 'resources/pixmap/plane.png'
	dock_strips = 'resources/pixmap/rack.png'
	dock_teaching = 'resources/pixmap/teaching.png'
	dock_txtChat = 'resources/pixmap/chat.png'
	dock_twrView = 'resources/pixmap/control-TWR.png'
	dock_weather = 'resources/pixmap/weather.png'

	button_view = 'resources/pixmap/eye.png'
	button_search = 'resources/pixmap/magnifying-glass.png'
	button_save = 'resources/pixmap/floppy-disk.png'
	button_bin = 'resources/pixmap/bin.png'
	button_shelf = 'resources/pixmap/shelf.png'

	handover_pixmap = 'resources/pixmap/envelope.png'
	recycle_pixmap = 'resources/pixmap/recycle.png'
	button_alarmClock = 'resources/pixmap/stopwatch.png'




class SignalCentre(QObject):
	selectionChanged = pyqtSignal()
	stripInfoChanged = pyqtSignal()
	statusBarMsg = pyqtSignal(str)
	gameStarted = pyqtSignal()
	gameStopped = pyqtSignal()
	gamePaused = pyqtSignal()
	gameResumed = pyqtSignal()
	clockTick = pyqtSignal()
	aircraftKilled = pyqtSignal(Aircraft)
	towerViewProcessToggled = pyqtSignal(bool) # True=started; False=finished
	systemSettingsChanged = pyqtSignal()
	generalSettingsChanged = pyqtSignal()
	soloGameSettingsChanged = pyqtSignal()
	localSettingsChanged = pyqtSignal()
	runwayUseChanged = pyqtSignal()
	backgroundImagesReloaded = pyqtSignal()
	colourConfigReloaded = pyqtSignal()
	mainWindowClosing = pyqtSignal()
	kbdPTT = pyqtSignal(int, bool)   # kbd PTT key number, mic PTT
	radioPTT = pyqtSignal(str, bool) # address string, mic PTT
	
	indicatePoint = pyqtSignal(EarthCoords)
	navpointClick = pyqtSignal(Navpoint)
	pkPosClick = pyqtSignal(str)
	specialTool = pyqtSignal(EarthCoords, Heading)
	voiceMsgRecognised = pyqtSignal(list, list) # callsign tokens used, recognised instructions
	voiceMsgNotRecognised = pyqtSignal()
	wilco = pyqtSignal()
	
	weatherUpdateRequest = pyqtSignal()
	fplUpdateRequest = pyqtSignal()
	newATC = pyqtSignal(str) # callsign
	newFPL = pyqtSignal(FPL)
	controlledContactLost = pyqtSignal(Strip, EarthCoords)
	aircraftIdentification = pyqtSignal(Strip, Aircraft, bool) # bool True if mode S identification
	newWeather = pyqtSignal(str, Weather)
	incomingTextChat = pyqtSignal(ChatMessage)
	voiceMsg = pyqtSignal(PilotVoice, str)
	chatInstructionSuggestion = pyqtSignal(str, str, bool) # dest callsign, instr message, send immediately
	
	openShelfRequest = pyqtSignal()
	stripRecall = pyqtSignal(Strip)
	stripEditRequest = pyqtSignal(Strip)
	FPLeditRequest = pyqtSignal(FPL)
	newLinkedFPLrequest = pyqtSignal(Strip)
	launchADmode = pyqtSignal(str)
	launchCTRmode = pyqtSignal(str, str)
	receiveStrip = pyqtSignal(Strip)
	handoverFailure = pyqtSignal(Strip, str)
	
	def __init__(self):
		QObject.__init__(self)


signals = SignalCentre()






class Selection:
	def __init__(self):
		self.acft = None
		self.strip = None
		self.fpl = None
	
	def none(self):
		return self.acft == None and self.strip == None and self.fpl == None
	
	def deselect(self):
		self.acft = self.strip = self.fpl = None
		signals.selectionChanged.emit()
		
	def selectStrip(self, select):
		self.strip = select
		self.fpl = self.strip.linkedFPL()
		self.acft = self.strip.linkedAircraft()
		signals.selectionChanged.emit()
		
	def selectAircraft(self, select):
		self.acft = select
		self.strip = env.linkedStrip(self.acft)
		if self.strip == None:
			self.fpl = None
		else:
			self.fpl = self.strip.linkedFPL()
		signals.selectionChanged.emit()
		
	def selectFPL(self, select):
		self.fpl = select
		self.strip = env.linkedStrip(self.fpl)
		if self.strip == None:
			self.acft = None
		else:
			self.acft = self.strip.linkedAircraft()
		signals.selectionChanged.emit()
	
	def selectedCallsign(self):
		if self.strip == None:
			if self.acft == None:
				if self.fpl == None:
					return None
				else:
					return self.fpl[FPL.CALLSIGN]
			else:
				return self.acft.xpdrCallsign()
		else:
			if self.strip.callsign(fpl=False) == None:
				if self.acft == None:
					return self.strip.callsign(fpl=True)
				else:
					return self.acft.xpdrCallsign()
			else:
				return self.strip.callsign(fpl=False)
	
	def linkAircraft(self, acft):
		if self.strip != None and self.strip.linkedAircraft() == None and env.linkedStrip(acft) == None:
			self.strip.linkAircraft(acft)
			if settings.strip_autofill_on_ACFT_link:
				self.strip.fillFromXPDR()
			signals.stripInfoChanged.emit()
			self.selectAircraft(acft)

	def unlinkAircraft(self, acft):
		if self.strip != None and self.strip.linkedAircraft() is acft:
			self.strip.linkAircraft(None)
			signals.stripInfoChanged.emit()
			self.selectStrip(self.strip)
	
	def writeStripAssignment(self, instr):
		if self.strip != None:
			# heading assignment
			if instr.type == Instruction.VECTOR_HDG:
				self.strip.writeDetail(assigned_heading_detail, instr.arg)
			elif instr.type in [Instruction.VECTOR_DCT, Instruction.FOLLOW_ROUTE, Instruction.HOLD, \
					Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC, Instruction.CLEARED_APP, Instruction.CLEARED_TO_LAND]:
				self.strip.writeDetail(assigned_heading_detail, None)
			# altitude assignment
			if instr.type == Instruction.VECTOR_ALT:
				self.strip.writeDetail(assigned_altitude_detail, instr.arg)
			elif instr.type in [Instruction.CLEARED_APP, Instruction.CLEARED_TO_LAND]:
				self.strip.writeDetail(assigned_altitude_detail, None)
			# speed assignment
			if instr.type == Instruction.VECTOR_SPD:
				self.strip.writeDetail(assigned_speed_detail, instr.arg)
			elif instr.type in [Instruction.CANCEL_VECTOR_SPD, Instruction.HOLD, Instruction.CLEARED_TO_LAND]:
				self.strip.writeDetail(assigned_speed_detail, None)
			# transponder assignment
			if instr.type == Instruction.SQUAWK:
				self.strip.writeDetail(assigned_SQ_detail, instr.arg)
			signals.stripInfoChanged.emit()
	
	def __str__(self):
		return '{strip:%s, fpl:%s, acft:%s}' % (self.strip, self.fpl, self.acft)



selection = Selection()









##------------------------------------##
##                                    ##
##            USEFUL TOOLS            ##
##                                    ##
##------------------------------------##


class Ticker(QTimer):
	def __init__(self, action_callback, parent=None):
		'''
		start can be overridden with a timedelta or numeric milliseconds
		'''
		QTimer.__init__(self, parent)
		self.do_action = action_callback
		self.timeout.connect(self.do_action)
	
	def start_stopOnZero(self, t, immediate=True):
		t = int(1000 * t.total_seconds() if isinstance(t, timedelta) else t)
		if t == 0:
			self.stop()
		else:
			if immediate:
				self.do_action()
			self.start(t)



