
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from game.config import settings
from data.utc import now


# ---------- Constants ----------

# -------------------------------


# Pronunciation dictionaries: code -> (TTS str, SR phoneme list)
phon_airlines = {}
phon_navpoints = {}


def get_TTS_string(dct, key):
	return dct[key][0]

def get_phonemes(dct, key):
	return dct[key][1]



class ChatMessage:
	def __init__(self, sender, text, dest=None, private=False):
		self.sent_by = sender
		self.dest = dest
		self.text = text
		self.private = private
		self.msg_time_stamp = now()

	def txtMsg(self):
		if self.dest == None or self.dest == '' or self.private:
			return self.text
		else:
			return '%s: %s' % (self.dest, self.text)

	def isFromMe(self):
		return self.sent_by == settings.game_manager.myCallsign()
	
	def isPrivate(self):
		return self.private
	
	def timeStamp(self):
		return self.msg_time_stamp

	def sender(self):
		return self.sent_by

