
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR
from datetime import datetime, timedelta, timezone

from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtNetwork import QTcpServer, QHostAddress
from PyQt5.QtWidgets import QMessageBox, QInputDialog

from ai.aircraft import AI_Aircraft

from data.util import discard_one, pop_all
from data.fpl import FPL
from data.utc import now
from data.db import wake_turb_cat
from data.instruction import Instruction
from data.params import Heading, Speed
from data.comms import ChatMessage
from data.weather import Weather
from data.strip import Strip, HandoverError, received_from_detail, sent_to_detail, \
	assigned_SQ_detail, assigned_heading_detail, assigned_altitude_detail, assigned_speed_detail

from game.config import settings
from game.env import env
from game.manager import GameManager, GameType, student_callsign, teacher_callsign
from game.solo import Status, SoloParams

from ext.fgfs import send_packet_to_views
from ext.tts import speech_str2txt

from gui.misc import selection, signals, Ticker
from gui.dialog.createTraffic import CreateTrafficDialog


# ---------- Constants ----------

game_tick_interval = 200 # ms
max_noACK_traffic = 20
new_traffic_XPDR_mode = 'C'
handover_details = list(FPL.details) + [assigned_SQ_detail, assigned_heading_detail, assigned_altitude_detail, assigned_speed_detail]

# -------------------------------



class TeachingMsg:
	msg_types = ACFT_KILLED, SIM_PAUSED, SIM_RESUMED, TEXT_CHAT, STRIP_EXCHANGE, SX_LIST, WEATHER, TRAFFIC = range(8)
	
	def __init__(self, msg_type, data=None):
		self.type = msg_type
		self.data = b''
		if data != None:
			self.appendData(data)
	
	def appendData(self, data):
		self.data += data if isinstance(data, bytes) else data.encode('utf8')
	
	def binData(self):
		return self.data
	
	def strData(self):
		return self.data.decode('utf8')
	
	def decodeDetails(self):
		details = {}
		lines = self.strData().split('\n')
		line1 = lines.pop(0)
		while lines != []:
			detail_token, line_count_token = lines.pop(0).split()
			try:
				detail = int(detail_token)
			except ValueError:
				detail = detail_token
			vstr = lines.pop(0) # cannot be zero lines
			for i in range(int(line_count_token) - 1):
				vstr += '\n' + lines.pop(0)
			details[detail] = str2detail(detail, vstr)
		return line1, details
	
	# STATIC
	def detailExchangeMsg(msg_type, line1, details):
		msg = TeachingMsg(msg_type)
		msg.appendData(line1)
		for d, v in details.items():
			vstr = detail2str(d, v)
			msg.appendData('\n%s %d\n' % (d, len(vstr.split('\n')))) # detail (whether FPL int or str key) and line count
			msg.appendData(vstr)
		return msg
	


## DETAIL STRING CONVERSIONS

def detail2str(d, v):
	if d in [FPL.CALLSIGN, FPL.ACFT_TYPE, FPL.WTC, FPL.ICAO_DEP, FPL.ICAO_ARR, FPL.ICAO_ALT, \
			FPL.CRUISE_ALT, FPL.ROUTE, FPL.COMMENTS, FPL.FLIGHT_RULES, assigned_altitude_detail]: # str
		return v
	elif d in [FPL.SOULS, assigned_SQ_detail]: # int
		return str(v)
	elif d in [FPL.TAS, assigned_speed_detail]: # Speed
		return str(int(v.kt))
	elif d == FPL.TIME_OF_DEP: # datetime
		return '%d %d %d %d %d' % (v.year, v.month, v.day, v.hour, v.minute)
	elif d == FPL.EET: # timedelta
		return str(int(v.total_seconds()))
	elif d == assigned_heading_detail: # Heading
		return str(int(v.read()))
	else:
		print('Not converting detail [%s] = %s' % (d, v))


def str2detail(d, s):
	if d in [FPL.CALLSIGN, FPL.ACFT_TYPE, FPL.WTC, FPL.ICAO_DEP, FPL.ICAO_ARR, FPL.ICAO_ALT, \
			FPL.CRUISE_ALT, FPL.ROUTE, FPL.COMMENTS, FPL.FLIGHT_RULES, assigned_altitude_detail]: # str
		return s
	elif d in [FPL.SOULS, assigned_SQ_detail]: # int
		return int(s)
	elif d in [FPL.TAS, assigned_speed_detail]: # Speed
		return Speed(int(s))
	elif d == FPL.TIME_OF_DEP: # datetime
		y, m, d, hour, minute = s.split()
		return datetime(year=int(y), month=int(m), day=int(d), hour=int(hour), minute=int(minute), tzinfo=timezone.utc)
	elif d == FPL.EET: # timedelta
		return timedelta(seconds=int(s))
	elif d == assigned_heading_detail: # Heading
		return Heading(int(s), False)
	else:
		print('Not converting detail [%s] = %s' % (d, s))












class TeachingSessionWire(QObject):
	messageArrived = pyqtSignal(TeachingMsg)
	
	def __init__(self, socket):
		QObject.__init__(self)
		self.socket = socket
		self.got_msg_type = None
		self.got_data_len = None
		self.socket.readyRead.connect(self.readAvailableBytes)

	def readAvailableBytes(self):
		if self.got_msg_type == None:
			if self.socket.bytesAvailable() < 1:
				return
			self.got_msg_type = int.from_bytes(self.socket.read(1), 'big')
		if self.got_data_len == None:
			if self.socket.bytesAvailable() < 4:
				return
			self.got_data_len = int.from_bytes(self.socket.read(4), 'big')
		if self.socket.bytesAvailable() < self.got_data_len:
			return
		self.messageArrived.emit(TeachingMsg(self.got_msg_type, data=self.socket.read(self.got_data_len)))
		self.got_msg_type = self.got_data_len = None
		if self.socket.bytesAvailable() > 0:
			self.socket.readyRead.emit()
	
	def sendMessage(self, msg):
		buf = msg.type.to_bytes(1, 'big') # message type code
		buf += len(msg.data).to_bytes(4, 'big') # length of data
		buf += msg.data # message data
		self.socket.write(buf)












# -------------------------------

class TeacherSessionGameManager(GameManager):
	def __init__(self, gui):
		GameManager.__init__(self, gui)
		self.game_type = GameType.TEACHER
		self.game_ticker = Ticker(self.tickOnce, parent=gui)
		self.game_paused_at = None # pause time if game is paused; None otherwise
		self.server = QTcpServer(gui)
		self.student_socket = None
		self.server.newConnection.connect(self.studentConnects)
		self.aircraft_list = [] # AI_Aircraft list
		self.current_local_weather = None # initialised by the teaching console on gameStarted
		self.noACK_traffic_count = 0
	
	def start(self):
		self.aircraft_list.clear()
		self.game_paused_at = None
		self.game_ticker.start_stopOnZero(game_tick_interval)
		self.server.listen(port=settings.teaching_service_port)
		print('Teaching server ready on port %d' % settings.teaching_service_port)
		signals.specialTool.connect(self.createNewTraffic)
		signals.gameStarted.emit()
	
	def stop(self):
		if self.isRunning():
			self.game_ticker.stop()
			if self.studentConnected():
				self.shutdownStudentConnection()
			signals.specialTool.disconnect(self.createNewTraffic)
			self.server.close()
			self.aircraft_list.clear()
			signals.gameStopped.emit()
	
	def studentConnected(self):
		return self.student_socket != None
	
	def isRunning(self):
		return self.game_ticker.isActive() or self.game_paused_at != None
	
	def myCallsign(self):
		return teacher_callsign
	
	def getAircraft(self):
		return self.aircraft_list[:]
	
	def getWeather(self, station):
		return self.current_local_weather if station == settings.primary_METAR_station else None
	
	def postTextChat(self, msg):
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.TEXT_CHAT, data=msg.txtMsg()))
		else:
			raise ValueError('No student connected.')
	
	
	## CONNECTION MANAGEMENT
	
	def studentConnects(self):
		new_connection = self.server.nextPendingConnection()
		if new_connection:
			peer_address = new_connection.peerAddress().toString()
			print('Contacted by %s' % peer_address)
			if self.studentConnected():
				new_connection.disconnectFromHost()
				print('Client rejected. Student already connected.')
			else:
				self.student_socket = new_connection
				self.student_socket.disconnected.connect(self.studentDisconnects)
				self.student_socket.disconnected.connect(self.student_socket.deleteLater)
				self.student = TeachingSessionWire(self.student_socket)
				self.student.messageArrived.connect(self.receiveMsgFromStudent)
				env.ATCs.updateATC(student_callsign, True, None, None, None)
				self.noACK_traffic_count = 0
				self.sendWeather()
				self.sendATCs()
				self.tickOnce()
				if self.game_paused_at != None:
					self.student.sendMessage(TeachingMsg(TeachingMsg.SIM_PAUSED))
				QMessageBox.information(self.gui, 'Student connection', 'Student accepted from %s' % peer_address)
		else:
			print('WARNING: Connection attempt failed.')
	
	def studentDisconnects(self):
		self.shutdownStudentConnection()
		QMessageBox.information(self.gui, 'Student disconnection', 'Your student has disconnected.')
	
	def shutdownStudentConnection(self):
		self.student_socket.disconnected.disconnect(self.studentDisconnects)
		env.ATCs.removeATC(student_callsign)
		self.student.messageArrived.disconnect(self.receiveMsgFromStudent)
		self.student_socket.disconnectFromHost()
		self.student_socket = None
	
	
	## TEACHER MANAGEMENT
	
	def instructAircraft(self, instr, callsign=None): # WARNING: callsign bypassed in teacher mode; direct acft command instead
		acft = selection.acft
		if acft == None:
			QMessageBox.critical(self.gui, 'Traffic command error', 'No aircraft selected.')
		else:
			try:
				acft.instruct([instr])
				acft.readBack([instr])
				selection.writeStripAssignment(instr)
			except Instruction.Error as err:
				QMessageBox.critical(self.gui, 'Traffic management error', speech_str2txt(str(err)))
	
	def createNewTraffic(self, spawn_coords, spawn_hdg):
		dialog = CreateTrafficDialog(spawn_coords, spawn_hdg, parent=self.gui)
		dialog.exec()
		if dialog.result() > 0:
			init_params = dialog.acftInitParams()
			init_params.XPDR_mode = new_traffic_XPDR_mode
			acft = AI_Aircraft(dialog.acftCallsign(), init_params, dialog.acftType(), None)
			acft._spawned = False
			acft.frozen = dialog.startFrozen()
			acft.tickOnce()
			self.aircraft_list.append(acft)
			if dialog.createStrip():
				strip = Strip()
				strip.writeDetail(FPL.CALLSIGN, acft.identifier)
				strip.writeDetail(FPL.ACFT_TYPE, acft.aircraft_type)
				strip.writeDetail(FPL.WTC, wake_turb_cat(acft.aircraft_type))
				strip.linkAircraft(acft)
				signals.receiveStrip.emit(strip)
			selection.selectAircraft(acft)
	
	def killAircraft(self, acft):
		discard_one(self.aircraft_list, lambda a: a is acft)
		if acft._spawned and self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.ACFT_KILLED, data=acft.identifier))
		signals.aircraftKilled.emit(acft)
	
	def sendATCs(self):
		if self.studentConnected():
			msg = TeachingMsg(TeachingMsg.SX_LIST)
			for atc in env.ATCs.knownATCs(sx=False): # all but the student
				try:
					frq = env.ATCs.ATCfrq(atc)
				except KeyError:
					frq = None
				msg.appendData(atc if frq == None else '%s\t%s' % (atc, frq))
				msg.appendData('\n')
			self.student.sendMessage(msg)
	
	def setWeatherFromMetar(self, metar): # assumed at primary location and newer
		self.current_local_weather = Weather(metar)
		signals.newWeather.emit(settings.primary_METAR_station, self.current_local_weather)
		self.sendWeather()
	
	def sendWeather(self):
		if self.studentConnected() and self.current_local_weather != None:
			self.student.sendMessage(TeachingMsg(TeachingMsg.WEATHER, data=self.current_local_weather.METAR()))
	
	def pauseGame(self):
		self.game_ticker.stop()
		self.game_paused_at = now()
		signals.gamePaused.emit()
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.SIM_PAUSED))
	
	def resumeGame(self):
		pause_delay = now() - self.game_paused_at
		for acft in self.aircraft_list:
			acft.moveHistoryTimesForward(pause_delay)
		self.game_paused_at = None
		self.game_ticker.start_stopOnZero(game_tick_interval)
		signals.gameResumed.emit()
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.SIM_RESUMED))
	
	
	## MESSAGES FROM STUDENT
	
	def receiveMsgFromStudent(self, msg):
		#print('=== TEACHERS RECEIVES ===\n%s\n=== End ===' % msg.data) # DEBUG
		if msg.type == TeachingMsg.TEXT_CHAT:
			signals.incomingTextChat.emit(ChatMessage(student_callsign, msg.strData(), dest=teacher_callsign, private=True))
		elif msg.type == TeachingMsg.STRIP_EXCHANGE:
			toATC, details = msg.decodeDetails()
			strip = Strip()
			for d, v in details.items():
				strip.writeDetail(d, v)
			strip.writeDetail(received_from_detail, student_callsign)
			if toATC != teacher_callsign:
				strip.writeDetail(sent_to_detail, toATC)
			signals.receiveStrip.emit(strip)
		elif msg.type == TeachingMsg.WEATHER: # requesting weather information
			if msg.strData() == settings.primary_METAR_station:
				self.sendWeather()
		elif msg.type == TeachingMsg.TRAFFIC: # acknowledging a traffic message
			if self.noACK_traffic_count > 0:
				self.noACK_traffic_count -= 1
			else:
				print('Student acknowledging unsent traffic?!')
		else:
			print('Unhandled message type from student: %s' % msg.type)
	
	
	## TICK
	
	def tickOnce(self):
		pop_all(self.aircraft_list, lambda a: not env.pointInRadarRange(a.params.position))
		send_traffic_this_tick = self.studentConnected() and self.noACK_traffic_count < max_noACK_traffic
		for acft in self.aircraft_list:
			acft.round_body = not acft._spawned
			acft.tickOnce()
			fgms_packet = acft.FGMSlivePositionPacket()
			send_packet_to_views(fgms_packet)
			if send_traffic_this_tick and acft._spawned:
				self.student.sendMessage(TeachingMsg(TeachingMsg.TRAFFIC, data=fgms_packet))
				self.noACK_traffic_count += 1
	
	
	## STRIP EXCHANGE
	
	def handOverStrip(self, strip, sendto):
		if sendto == student_callsign:
			items = [teacher_callsign] + env.ATCs.knownATCs(sx=False) # all but the student
			sender, ok = QInputDialog.getItem(self.gui, 'Send strip to student', 'Hand over strip from:', items, editable=False)
			if ok and self.studentConnected():
				details = { d:strip.lookup(d) for d in handover_details if strip.lookup(d) != None }
				self.student.sendMessage(TeachingMsg.detailExchangeMsg(TeachingMsg.STRIP_EXCHANGE, sender, details))
			else:
				raise HandoverError('Cancelled by teacher.', silent=True)
		else:
			raise HandoverError('Strips can only be sent to the student!')
	
	
	## SNAPSHOTTING
	
	def situationSnapshot(self):
		return [acft.statusSnapshot() for acft in self.aircraft_list]
	
	def restoreSituation(self, situation_snapshot):
		while self.aircraft_list != []:
			self.killAircraft(self.aircraft_list[0])
		for acft_snapshot in situation_snapshot:
			self.aircraft_list.append(AI_Aircraft.fromStatusSnapshot(acft_snapshot))
		self.tickOnce()

