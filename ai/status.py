
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from data.util import some
from game.config import settings
from game.env import env
from data.params import StdPressureAlt


# ---------- Constants ----------

default_XPDR_mode = 'C'

# -------------------------------

#
# Statuses and arg types
#
# On ground:
#  TAXIING
#  READY: str (RWY name)
#  LINED_UP: str (RWY name)
#  RWY_TKOF: str (RWY name)
#  RWY_LDG: str (RWY name)
# Off ground:
#  AIRBORNE
#  HLDG: Heading (outbound leg direction), timedelta (time left to fly outbound)
#  LANDING: str (RWY name)
#

class Status:
	enum = TAXIING, READY, LINED_UP, RWY_TKOF, AIRBORNE, HLDG, LANDING, RWY_LDG = range(8)
	
	def __init__(self, init_status, arg=None):
		'''
		Creates an airborne aircraft, unless RWY is given (starts ready for DEP)
		'''
		self.type = init_status
		self.arg = arg
	
	def __str__(self):
		arg_suffix = '' if self.arg == None else ':%s' % self.arg
		return 'S:%d%s' % (self.type, arg_suffix)
	
	def dup(self):
		return Status(self.type, arg=self.arg)
	



class SoloParams:
	
	def __init__(self, init_status):
		self.status = init_status
		self.position = None # MUST set immediately!
		self.heading = None  # MUST set immediately!
		self.altitude = None # MUST set immediately!
		self.ias = None    # MUST set immediately!
		self.XPDR_mode = default_XPDR_mode # possible values are: '0', 'A', 'C', 'G', 'S' ('S' may squat depending on ACFT setting)
		self.XPDR_code = settings.uncontrolled_VFR_XPDR_code
		self.XPDR_idents = False
		self.runway_reported_in_sight = False
	
	def dup(self):
		params = SoloParams(self.status.dup())
		params.position = self.position
		params.heading = self.heading
		params.altitude = self.altitude
		params.ias = self.ias
		params.XPDR_mode = self.XPDR_mode
		params.XPDR_code = self.XPDR_code
		params.XPDR_idents = self.XPDR_idents
		params.runway_reported_in_sight = self.runway_reported_in_sight
		return params
	
	def geometricAltitude(self):
		return self.altitude.ftAMSL(env.QNH())

