
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#


from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMessageBox

from game.config import settings
from game.env import env
from game.manager import GameType, student_callsign, teacher_callsign

from data.coords import dist_str
from data.strip import strip_mime_type, sent_to_detail, HandoverError
from data.instruction import Instruction

from gui.misc import signals, selection
from gui.dialog.miscDialogs import yesNo_question


# ---------- Constants ----------

sx_able_pixmap = 'resources/pixmap/envelope.png'
nearby_dist_threshold = .1 # NM

# -------------------------------

# [[*]] WARNING [[*]]
# dataChanged.emit instructions in QAbstractTableModels cause error
# "QObject::connect: Cannot queue arguments of type 'QVector<int>'"
# if connected across threads, because of optional argument "roles".
# See also: https://bugreports.qt.io/browse/QTBUG-46517
# The work-around here is to use "refreshViews" from main thread after
# data changes have been made. Less efficient but OK as lists are small.


class AtcNeighboursModel(QAbstractTableModel):
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.gui = parent
		self.ATCs = [] # list of (identifier/callsign, SX-able bool, EarthCoords/None, name/None, frequency/None)
		self.sx_able_icon = QIcon(sx_able_pixmap)

	def columnCount(self, parent=QModelIndex()):
		return 1

	def rowCount(self, parent=QModelIndex()):
		return len(self.ATCs)
		
	def data(self, index, role):
		row = index.row()
		col = index.column()
		cs, sx, pos, name, frq = self.ATCs[row]
		
		if role == Qt.DisplayRole:
			if col == 0:
				return cs if frq == None else '%s (%s)' % (cs, frq)
		
		elif role == Qt.DecorationRole:
			if col == 0 and sx:
				return self.sx_able_icon
		
		elif role == Qt.ToolTipRole:
			txt = ''
			if name != None:
				txt += name + '\n'
			txt += 'Position: '
			if pos == None:
				txt += 'unknown'
			else:
				distance = env.radarPos().distanceTo(pos)
				if distance < nearby_dist_threshold:
					txt += 'nearby'
				else:
					txt += '%s°, %s' % (env.radarPos().headingTo(pos).readTrue(), dist_str(distance))
			return txt
	
	def flags(self, index):
		flags = Qt.ItemIsEnabled
		if index.isValid() and index.column() == 0 and self.ATCs[index.row()][1]: # capable of SX
			flags |= Qt.ItemIsDropEnabled
		return flags
	
	
	
	## ACCESS FUNCTIONS
	
	def knownATCs(self, sx=None):
		'''
		Default: all ATCs. With sx=True: SX-capable only; sx=False: non-SX only.
		'''
		return [atc[0] for atc in self.ATCs if sx == None or sx and atc[1] or not sx and not atc[1]]
	
	def callsignOnRow(self, row):
		return self.ATCs[row][0]
	
	## by FGMS identifier, raise KeyError if not in model
	
	def ATCpos(self, cs):
		try:
			return next(atc[2] for atc in self.ATCs if atc[0] == cs)
		except StopIteration:
			raise KeyError(cs)
	
	def ATCfrq(self, cs):
		try:
			return next(atc[4] for atc in self.ATCs if atc[0] == cs)
		except StopIteration:
			raise KeyError(cs)
	
	def handoverInstructionTo(self, atc):
		try:
			frq = self.ATCfrq(atc)
		except KeyError:
			frq = None
		return Instruction(Instruction.HAND_OVER, arg=(atc, frq))
	
	
	## UPDATE FUNCTIONS
	
	def updateATC(self, callsign, sx, pos, name, frq):
		'''
		Updates an ATC if already present; adds it otherwise with the given details.
		'''
		try:
			row = next(i for i, atc in enumerate(self.ATCs) if atc[0] == callsign)
			self.ATCs[row] = callsign, sx, pos, name, frq
			# FUTURE enable? [[*]] self.dataChanged.emit(self.index(row, 0), self.index(row, self.columnCount() - 1))
		except StopIteration:
			self.beginInsertRows(QModelIndex(), len(self.ATCs), len(self.ATCs))
			self.ATCs.append((callsign, sx, pos, name, frq))
			signals.newATC.emit(callsign)
			self.endInsertRows()
	
	def removeATC(self, callsign):
		try:
			row = next(i for i, atc in enumerate(self.ATCs) if atc[0] == callsign)
			self.beginRemoveRows(QModelIndex(), row, row)
			del self.ATCs[row]
			self.endRemoveRows()
		except StopIteration:
			raise KeyError(callsign)
	
	def clear(self):
		self.beginResetModel()
		self.ATCs.clear()
		self.endResetModel()
	
	def refreshViews(self): # [[*]]
		self.dataChanged.emit(self.index(0, 0), self.index(self.rowCount() - 1, self.columnCount() - 1))
	
	
	## DRAG AND DROP STUFF
	
	def supportedDropActions(self):
		return Qt.MoveAction
	
	def mimeTypes(self):
		return [strip_mime_type]
	
	def dropMimeData(self, mime, drop_action, row, column, parent):
		if drop_action == Qt.MoveAction and mime.hasFormat(strip_mime_type):
			strip = env.strips.fromMimeDez(mime)
			atc_callsign = self.ATCs[parent.row()][0]
			if not settings.confirm_handovers or settings.game_manager.game_type == GameType.TEACHER or \
					yesNo_question(self.gui, 'Confirm handover', 'You are about to send your strip to %s.' % atc_callsign, 'Confirm?'):
				if settings.strip_autofill_before_handovers:
					strip.fillFromXPDR(ovr=False)
					strip.fillFromFPL(ovr=False)
				try:
					settings.game_manager.handOverStrip(strip, atc_callsign)
				except HandoverError as err:
					if not err.silent:
						QMessageBox.critical(self.gui, 'Handover aborted', str(err))
				else:
					selection.deselect()
					strip.writeDetail(sent_to_detail, atc_callsign)
					if settings.game_manager.game_type != GameType.TEACHER:
						strip.linkAircraft(None)
						strip.linkFPL(None)
						env.strips.removeStrip(strip)
						env.discarded_strips.addStrip(strip)
		return True

