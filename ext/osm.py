
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from urllib.request import urlretrieve
from urllib.error import URLError

from game.config import settings


# ---------- Constants ----------

OSM_tile_base_location = 'http://render.openstreetmap.org/cgi-bin/export'  # OSM tile server URL
OSM_img_file_name_prefix = 'osm-'  # ATC location, image format
OSM_img_format = 'png'

# -------------------------------



def download_OSM_tile(NW_coords, SE_coords, scale):
	query = '%s?bbox=%f,%f,%f,%f' % (OSM_tile_base_location, NW_coords.lon, SE_coords.lat, SE_coords.lon, NW_coords.lat)
	query += '&scale=%d&format=%s' % (scale, OSM_img_format)
	file_name = settings.outputFileName(OSM_img_file_name_prefix + settings.location_code, ext=OSM_img_format, sessionID=False)
	try:
		urlretrieve(query, file_name)
	except URLError as err:
		return False, str(err)
	else:
		return True, file_name

