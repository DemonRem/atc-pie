
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import QThread
from xml.etree import ElementTree
from time import sleep
from socket import timeout
from datetime import timedelta

from urllib.parse import urlencode
from urllib.request import urlopen
from urllib.error import URLError

from data.util import some
from game.config import settings, URL_request
from data.coords import EarthCoords
from data.params import StdPressureAlt, Speed
from game.env import env
from data.utc import now
from data.fpl import FPL
from data.strip import Strip, HandoverError, received_from_detail, assigned_altitude_detail, assigned_SQ_detail
from gui.misc import Ticker, signals


# ---------- Constants ----------

SX_update_interval = 3 * 1000 # milliseconds
ATCpie_hidden_string = '__ATC-pie__' # to recognise ATC-pie sender in <pilot> elements

WWSX_account_name = 'ATC-pie'
WWSX_account_password = ''

# -------------------------------







def server_query(cmd, dict_data):
	qdict = {
		'user': WWSX_account_name,
		'password': WWSX_account_password,
		'atc': settings.game_manager.myCallsign()
	}
	if env.airport_data != None:
		qdict['airport'] = settings.location_code
	qdict.update(dict_data)
	try:
		req = URL_request('%s/%s' % (settings.FGSX_server_name, cmd), postData=bytes(urlencode(qdict), encoding='utf-8'))
		#print('\nPOST %s DATA: %s' % (cmd, post_data))
		response = urlopen(req).read()
		#print('RESPONSE: %s\n' % response)
		return response
	except (URLError, timeout) as error:
		print('URL error or request timed out: %s' % error)



def send_update_msg(fgms_id, strip, owner, handover):
	xml = ElementTree.Element('flightplanlist')
	xml.set('version', '1.0')
	xml.append(make_WW_XML(fgms_id, strip, owner, handover))
	qdict = {'flightplans': ElementTree.tostring(xml)}
	return server_query('updateFlightplans', qdict) != None # returns True if OK



def send_update(fgms_id, strip, handover=None): # if handover None: acknowledge strip
	# 1st message is identical between handover and acknowledgement
	if send_update_msg(fgms_id, strip, settings.game_manager.myCallsign(), ''): # 1st message OK
		sleep(2)
		if handover == None:
			if not send_update_msg(fgms_id, strip, '', ''): # 2nd acknowledgement message fails
				print('ERROR: Received strip probably seen as yours by OpenRadar users.')
		else:
			if not send_update_msg(fgms_id, strip, settings.game_manager.myCallsign(), handover): # 2nd handver message fails
				print('ERROR sending handover. Strip probably seen as yours by OpenRadar users.')
				signals.handoverFailure.emit(strip, 'Could not send handover request.')
	else: # 1st message failed
		if handover == None:
			print('ERROR: Received strip probably seen as pending handover by OpenRadar users.')
		else:
			print('ERROR claiming ownership. Strip probably owned by an OpenRadar user.')
			signals.handoverFailure.emit(strip, 'Could not claim ownership of contact for handover.')














## MAIN CLASS


class StripExchanger:
	def __init__(self, gui, update_SX_callback):
		self.strip_updater = SXupdater(gui, update_SX_callback)
		self.update_ticker = Ticker(self.strip_updater.start, parent=gui)
		self.gui = gui
		self.running = False
		self.strip_updater.finished.connect(env.ATCs.refreshViews)
	
	def start(self):
		self.update_ticker.start(SX_update_interval)
		self.running = True
	
	def stopAndWait(self):
		if self.isRunning(): # CHECK: do we need to wait for any running SXsender threads as well?
			self.update_ticker.stop()
			self.strip_updater.wait()
			self.running = False
	
	def isRunning(self):
		return self.running
	
	def handOver(self, strip, atc_id):
		'''
		returns cleanly if transfer is allowed and properly initiated (contact linked and in handover range),
		otherwise: HandoverError (handover aborted)
		'''
		acft = strip.linkedAircraft()
		if acft == None:
			raise HandoverError('Sending an unlinked strip is not allowed in FlightGear.  :-(')
		else:
			SXsender(self.gui, strip, acft.identifier, handover=atc_id).start()


# ------------------------------------------------------------------------------



class SXsender(QThread):
	def __init__(self, gui, strip, fgms_callsign, handover):
		QThread.__init__(self, parent=gui)
		self.fgms_id = fgms_callsign
		self.strip = strip
		self.handover = handover
	
	def run(self):
		send_update(self.fgms_id, self.strip, handover=self.handover)




class SXupdater(QThread):
	def __init__(self, gui, update_SX_callback):
		QThread.__init__(self, parent=gui)
		self.update_ATCs = update_SX_callback
	
	def run(self):
		## PREPARING QUERY
		pos = env.radarPos()
		qdict = {
			'username': settings.ATC_name,
      'lon': pos.lon,
      'lat': pos.lat,
      'frequency': some(settings.publicised_frequency, '000.00'),
      'xmlVersion': '1.0',
      'contacts': ','.join(acft.identifier for acft in env.radar.contacts()) # should this be all FGMS connections?
		}
		server_response = server_query('getFlightplans', qdict)
		## USING RESPONSE
		if server_response != None:
			try:
				ww_root = ElementTree.fromstring(server_response)
			except ElementTree.ParseError as parse_error:
				print('Parse error in SX server data: %s' % parse_error)
				return
			# ATCs first
			ATCs = {}
			for ww_atc in ww_root.find('atcsInRange').iter('atc'):
				ww_callsign = ww_atc.find('callsign').text
				ww_atcname = ww_atc.find('username').text
				ww_frq = ww_atc.find('frequency').text
				ww_coords = EarthCoords(float(ww_atc.find('lat').text), float(ww_atc.find('lon').text))
				ATCs[ww_callsign] = ww_coords, ww_atcname, (None if ww_frq.startswith('000') else ww_frq)
			self.update_ATCs(ATCs)
			# Then strip data
			for ww_flightplan in ww_root.iter('flightplan'):
				ww_header = ww_flightplan.find('header')
				if ww_header.find('handover').text == settings.game_manager.myCallsign(): # RECEIVE A STRIP!
					ww_data = ww_flightplan.find('data')
					ww_callsign = ww_header.find('callsign').text
					strip = Strip()
					strip.writeDetail(received_from_detail, ww_header.find('owner').text)
					strip.writeDetail(assigned_SQ_detail, ck_int(ww_header.find('squawk').text, base=8))
					strip.writeDetail(assigned_altitude_detail, ww_header.find('assignedAlt').text)
					# ATC-pie hides a token in <pilot>, wake turb. on its left and callsign to its right
					# e.g. <pilot>M__ATC-pie__X-FOO</pilot> for M turb. and X-FOO strip callsign
					# If the token is absent, we know the strip is from OpenRadar
					hidden_tokens = some(ww_data.find('pilot').text, '').split(ATCpie_hidden_string, maxsplit=1)
					if len(hidden_tokens) == 1: # hidden marker NOT present; previous strip editor was OpenRadar
						strip.writeDetail(FPL.CALLSIGN, ww_callsign)
					else: # recognise strip edited with ATC-pie
						strip.writeDetail(FPL.WTC, hidden_tokens[0])
						strip.writeDetail(FPL.CALLSIGN, hidden_tokens[1])
					# Ignored from header above: <flags>, <assignedRunway>, <assignedRoute>, <status>, <flight>
					# ---------------------------------------------
					# Ignored from data below: <fuelTime>
					# Used with ulterior motive: <pilot>
					strip.writeDetail(FPL.FLIGHT_RULES, ww_data.find('type').text)
					strip.writeDetail(FPL.ACFT_TYPE, ww_data.find('aircraft').text)
					strip.writeDetail(FPL.ICAO_DEP, ww_data.find('departure').text)
					strip.writeDetail(FPL.ICAO_ARR, ww_data.find('destination').text)
					strip.writeDetail(FPL.ICAO_ALT, ww_data.find('alternateDest').text)
					strip.writeDetail(FPL.ROUTE, ww_data.find('route').text)
					strip.writeDetail(FPL.CRUISE_ALT, ww_data.find('cruisingAlt').text)
					spd = ck_int(ww_data.find('trueAirspeed').text)
					if spd != None:
						strip.writeDetail(FPL.TAS, Speed(spd))
					dep = ck_int(ww_data.find('departureTime').text)
					if dep != None:
						dep_h = dep // 100
						dep_min = dep % 100
						if 0 <= dep_h < 24 and 0 <= dep_min < 60:
							strip.writeDetail(FPL.TIME_OF_DEP, now().replace(hour=dep_h, minute=dep_min, second=0, microsecond=0))
					eet = ww_data.find('estFlightTime').text
					if eet != None and ':' in eet:
						hours, minutes = eet.split(':', maxsplit=1)
						try:
							strip.writeDetail(FPL.EET, timedelta(hours=int(hours), minutes=int(minutes)))
						except ValueError:
							pass
					strip.writeDetail(FPL.SOULS, ck_int(ww_data.find('soulsOnBoard').text))
					strip.writeDetail(FPL.COMMENTS, ww_data.find('remarks').text)
					signals.receiveStrip.emit(strip)
					send_update(ww_callsign, strip) # Acknowledge strip




def ck_int(spec_string, base=10):
	try:
		return int(spec_string, base)
	except (TypeError, ValueError):
		return None



####################################



def make_simple_element(tag, contents):
	elt = ElementTree.Element(tag)
	if contents != None:
		elt.text = str(contents)
	return elt


def make_WW_XML(fgms_id, strip, owner, handover):
	# Header
	header = ElementTree.Element('header')
	header.append(make_simple_element('callsign', fgms_id))
	header.append(make_simple_element('owner', owner))
	header.append(make_simple_element('handover', handover))
	sq = strip.lookup(assigned_SQ_detail)
	header.append(make_simple_element('squawk', (None if sq == None else int('%o' % sq, base=10))))
	header.append(make_simple_element('assignedAlt', strip.lookup(assigned_altitude_detail)))
	# Ignored header
	header.append(make_simple_element('status', 'ACTIVE'))
	header.append(make_simple_element('fgcom', 'false'))
	header.append(make_simple_element('flight', None)) # WW says: element must not be empty
	header.append(make_simple_element('assignedRunway', None))
	header.append(make_simple_element('assignedRoute', None))
	# Data
	data = ElementTree.Element('data')
	data.append(make_simple_element('type', some(strip.lookup(FPL.FLIGHT_RULES, fpl=True), 'VFR')))
	data.append(make_simple_element('aircraft', strip.lookup(FPL.ACFT_TYPE, fpl=True)))
	spd = strip.lookup(FPL.TAS, fpl=True)
	data.append(make_simple_element('trueAirspeed', (spd.kt if spd != None else None)))
	data.append(make_simple_element('departure', strip.lookup(FPL.ICAO_DEP, fpl=True)))
	dep = strip.lookup(FPL.TIME_OF_DEP, fpl=True)
	data.append(make_simple_element('departureTime', (None if dep == None else '%02d%02d' % (dep.hour, dep.minute))))
	data.append(make_simple_element('cruisingAlt', strip.lookup(FPL.CRUISE_ALT, fpl=True)))
	data.append(make_simple_element('route', strip.lookup(FPL.ROUTE, fpl=True)))
	data.append(make_simple_element('destination', strip.lookup(FPL.ICAO_ARR, fpl=True)))
	data.append(make_simple_element('alternateDest', strip.lookup(FPL.ICAO_ALT, fpl=True)))
	eet = strip.lookup(FPL.EET, fpl=True)
	eet = None if eet == None else int(eet.total_seconds() + .5) // 60
	data.append(make_simple_element('estFlightTime', (None if eet == None else '%d:%02d' % (eet // 60, eet % 60))))
	data.append(make_simple_element('soulsOnBoard', strip.lookup(FPL.SOULS, fpl=True)))
	data.append(make_simple_element('remarks', strip.lookup(FPL.COMMENTS, fpl=True)))
	# Hidden data (ulterior motive: recognise data generated by ATC-pie)
	hidden_wake_turb = some(strip.lookup(FPL.WTC, fpl=True), '')
	hidden_callsign = some(strip.callsign(fpl=True), '')
	data.append(make_simple_element('pilot', '%s%s%s' % (hidden_wake_turb, ATCpie_hidden_string, hidden_callsign)))
	# Ignored data
	data.append(make_simple_element('fuelTime', None))
	# Wrap up
	root = ElementTree.Element('flightplan')
	root.append(header)
	root.append(data)
	return root


